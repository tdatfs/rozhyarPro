package com.example.pourya.sharsna.main.view.profile_login1;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class Pager extends FragmentStatePagerAdapter {

    int tabCount;

    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Tab_user tb1 = new Tab_user();
                return tb1;
            case 1:
                Tab_follower tb2 = new Tab_follower();
                return tb2;
            case 2:
                Tab_friend tb3 = new Tab_friend();
                return tb3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}