package com.example.pourya.sharsna.main.view;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Get_StepTwo;
import com.example.pourya.sharsna.main.api.Get_Verification;

import java.util.Timer;
import java.util.TimerTask;

public class Dialog_Code extends Dialog implements View.OnClickListener {

    Activity c;
    Dialog d;
    EditText code;
    LinearLayout btn_login;
    String enter_mobile;
    TextView txt_t;
    int wait_time = 80;

    public Dialog_Code(Activity a, String mobile) {
        super(a);
        this.c = a;
        enter_mobile = mobile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.pcode);

        code = (EditText) findViewById(R.id.mobile);
        btn_login = (LinearLayout) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(this);

        txt_t =(TextView) findViewById(R.id.txt_t);

        final Timer T = new Timer();
        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                c.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(wait_time>=0) {
                            txt_t.setText(wait_time + " ثانیه");
                            wait_time = wait_time - 1;
                        }else{
                            T.cancel();
                        }
                    }
                });
            }
        }, 1000, 1000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                String _code = faToEn(code.getText().toString());
                if(_code.equals("")){
                    Toast.makeText(c, "کد فعالسازی خود را وارد کنید", Toast.LENGTH_LONG).show();
                }else{
                    dismiss();
                    new Get_StepTwo(true,c,c,enter_mobile,_code,true);
                }
                break;
        }
    }

    public String faToEn(String num) {
        return num
                .replace("۰", "0")
                .replace("۱", "1")
                .replace("۲", "2")
                .replace("۳", "3")
                .replace("۴", "4")
                .replace("۵", "5")
                .replace("۶", "6")
                .replace("۷", "7")
                .replace("۸", "8")
                .replace("۹", "9");
    }
}