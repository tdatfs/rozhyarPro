package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Search;
import com.example.pourya.sharsna.main.model.Search_Model;
import com.example.pourya.sharsna.main.presenter.Search_Presenter;
import com.example.pourya.sharsna.main.view.search.Search;
import dagger.Module;
import dagger.Provides;

@Module
public class Search_Module {

    private Search activity;

    public Search_Module(Search activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Search providesSearchActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Search.ProvidedPresenterOps providedPresenterOps() {
        Search_Presenter presenter = new Search_Presenter( activity );
        Search_Model model = new Search_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
