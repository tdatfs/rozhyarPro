package com.example.pourya.sharsna.main.view.tabs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Product_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Product;
import com.example.pourya.sharsna.main.api.Get_OneProduct_bazar;
import com.example.pourya.sharsna.main.api.Get_OneUser_bazar;
import com.example.pourya.sharsna.main.api.Post_Like;
import com.example.pourya.sharsna.main.api.Post_UnLike;
import com.example.pourya.sharsna.main.presenter.Product_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Login1;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.search.Search;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import org.json.JSONException;
import javax.inject.Inject;

public class Product extends BaseActivity implements MVP_Product.RequiredViewOps, View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Product.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), Product.class.getName());

    @Inject
    public MVP_Product.ProvidedPresenterOps mPresenter;

    Toolbar toolbar;
    LinearLayout navigation_icon;
    DrawerLayout drawerLayout;
    MaterialSpinner spinner;
    SpaceNavigationView spaceNavigationView;
    public static TextView namep, pricep, offp, datep, desp, likp;
    public static int Pid, UID;
    public static ImageView img_like;
    public static ImageView img_add;
    public static int like_back;
    Button go_shop;
    public static int TabIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product1);

        String member_id = get_member_id();
        String user_id = get_user_id();
        new Get_OneProduct_bazar(getApplicationContext(), this, Pid, member_id, user_id);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(-1);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Product.this.finish();
                Intent intent = new Intent(Product.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        Product.this.finish();
                        Intent intent1 = new Intent(Product.this, Main.class);
                        startActivity(intent1);
                        break;

                    case 1:
                        Product.this.finish();
                        Intent intent2 = new Intent(Product.this, List1.class);
                        startActivity(intent2);
                        break;

                    case 2:
                        Product.this.finish();
                        Intent intentshop = new Intent(Product.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        after_setup();
    }

    private void setupViews(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        namep=(TextView)findViewById(R.id.nameProduct);
        pricep=(TextView)findViewById(R.id.priceProduct);
        offp=(TextView)findViewById((R.id.discountProduct));
        datep=(TextView)findViewById(R.id.dateProduct);
        desp=(TextView)findViewById(R.id.description);
        likp=(TextView)findViewById(R.id.like_count);
        img_add=(ImageView) findViewById(R.id.img_add);
        img_like=(ImageView) findViewById((R.id.img_like));
        go_shop = (Button) findViewById(R.id.go_shop);
    }

    private void after_setup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        navigation_icon.setOnClickListener(this);

        // like with 2 thing
        img_like.setOnClickListener(this);
        img_add.setOnClickListener(this);

        go_shop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }

        if (v.getId() == R.id.go_shop) {
            String mId = get_member_id();
            String uId = get_user_id();
            new Get_OneUser_bazar(haveNetworkConnection(), mPresenter, getApplicationContext(), this, UID, mId, uId);
        }

        if (v.getId() == R.id.img_like || v.getId() == R.id.img_add) {
            if(like_back == 0){

                String user_id = get_user_id();
                String member_id = get_member_id();
                // go login page
                if (member_id.equals("") && user_id.equals("")) {
                    Dialog_Login1 cdd = new Dialog_Login1(this);
                    cdd.show();
                }

                // do follow
                else{
                    try {
                        int cl = Integer.parseInt(likp.getText().toString());
                        new Post_Like(haveNetworkConnection(), mPresenter, getApplicationContext(), this, user_id, member_id, Pid, cl).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if(like_back == 1){

                String user_id = get_user_id();
                String member_id = get_member_id();
                // go login page
                if (member_id.equals("") && user_id.equals("")) {
                    Dialog_Login1 cdd = new Dialog_Login1(this);
                    cdd.show();
                }

                // do unfollow
                else{
                    try {
                        int cl = Integer.parseInt(likp.getText().toString());
                        new Post_UnLike(haveNetworkConnection(), mPresenter, getApplicationContext(), this, user_id, member_id, Pid, cl).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Product.this.finish();
                startActivity(new Intent(Product.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Product.this.finish();
                startActivity(new Intent( Product.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Product_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Product_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getProductComponent(new Product_Module(this))
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(-1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        Product.this.finish();
        Tabs_Main.TabIndex = TabIndex;
        startActivity(new Intent(Product.this, Tabs_Main.class));
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}