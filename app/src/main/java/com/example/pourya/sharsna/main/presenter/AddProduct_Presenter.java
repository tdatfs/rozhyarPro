package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_AddProduct;
import java.lang.ref.WeakReference;

public class AddProduct_Presenter implements MVP_AddProduct.ProvidedPresenterOps, MVP_AddProduct.RequiredPresenterOps{

    private WeakReference<MVP_AddProduct.RequiredViewOps> mView;
    private MVP_AddProduct.ProvidedModelOps mModel;

    public AddProduct_Presenter(MVP_AddProduct.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_AddProduct.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_AddProduct.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(MVP_AddProduct.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}