package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Product2;
import com.example.pourya.sharsna.main.viewModel.Cities;
import java.lang.ref.WeakReference;
import java.util.List;

public class Product2_Presenter implements MVP_Product2.ProvidedPresenterOps, MVP_Product2.RequiredPresenterOps{

    private WeakReference<MVP_Product2.RequiredViewOps> mView;
    private MVP_Product2.ProvidedModelOps mModel;

    public Product2_Presenter(MVP_Product2.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Product2.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Product2.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    public void setModel(MVP_Product2.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}