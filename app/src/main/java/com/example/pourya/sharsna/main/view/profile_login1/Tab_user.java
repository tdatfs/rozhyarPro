package com.example.pourya.sharsna.main.view.profile_login1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Get_info1;
import com.example.pourya.sharsna.main.api.Update_Members;
import com.example.pourya.sharsna.main.api.Update_User;

import org.json.JSONException;

public class Tab_user extends Fragment {

    public static TextView name, tel, mail;
    public static ImageView img;
    public static EditText et_name_family, et_email, et_mobile;
    Button update;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_user, container, false);

        name = (TextView) view.findViewById(R.id.name) ;
        tel = (TextView) view.findViewById(R.id.tel) ;
        mail = (TextView) view.findViewById(R.id.mail) ;
        img = (ImageView) view.findViewById(R.id.img) ;
        et_name_family = (EditText) view.findViewById(R.id.et_name_family) ;
        et_email = (EditText) view.findViewById(R.id.et_email) ;
        et_mobile = (EditText) view.findViewById(R.id.et_mobile) ;
        update = (Button) view.findViewById(R.id.update);

        new Get_info1(view.getContext(), getActivity(), Tabs_main1.IdUser);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String a1 = et_name_family.getText().toString();
                String a2 = et_email.getText().toString();
                String a3 = et_mobile.getText().toString();
                try {
                    new Update_Members(getContext(), getActivity(), Tabs_main1.IdUser, a1, a2, a3).post();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }
}