package com.example.pourya.sharsna.main.view.list1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.list2.List2;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.util.List;

public class Adapter_List1 extends RecyclerView.Adapter<Adapter_List1.ViewHolder> {

    Context context;
    Activity activity;
    List<Gulids> list;
    public String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_List1(Context c, Activity a, List<Gulids> l) {
        context = c;
        activity = a;
        list = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;
        LinearLayout main_item;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_name);
            imageView = (ImageView) itemView.findViewById(R.id.img_icon);
            main_item = (LinearLayout) itemView.findViewById(R.id.main_item);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_list1, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // not show
        if(list.get(position).getGuildName().equals("")){
            holder.main_item.setVisibility(View.INVISIBLE);
        }
        // normal
        else {
            holder.textView.setText(list.get(position).getGuildName().toString());
            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list.get(position).getIcon();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(holder.imageView);
            }catch (Exception e){
                new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
            }
            final int id = list.get(position).getId();
            holder.main_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List2.Gid = id;
                    activity.finish();
                    view.getContext().startActivity(new Intent(view.getContext(), List2.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}