package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Search_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.search.Search;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Search_Module.class )
public interface Search_Component {
    Search inject(Search activity);
}