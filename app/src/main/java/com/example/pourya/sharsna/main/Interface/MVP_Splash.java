package com.example.pourya.sharsna.main.Interface;

import android.content.Context;
import java.util.List;

public interface MVP_Splash {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_Splash.RequiredViewOps view);

        // slider
        int Count_Slider();
        List<Integer> List_Slider();
        String MaxRowV_Slider();
        void Insert_Slider(String Q);
        void Delete_Slider();

        // selectedshop
        int Count_SelectedShop();
        List<Integer> List_SelectedShop();
        void Delete_SelectedShop();
        void Insert_SelectedShop(String Q);

        // notification
        int Count_Notification();
        List<Integer> List_Notification();
        String MaxRowV_Notification();
        void Insert_Notification(String Q);
        void Delete_Notification();
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // slider
        int Count_Slider();
        List<Integer> List_Slider();
        String MaxRowV_Slider();
        void Insert_Slider(String Q);
        void Delete_Slider();

        // selectedshop
        int Count_SelectedShop();
        List<Integer> List_SelectedShop();
        void Delete_SelectedShop();
        void Insert_SelectedShop(String Q);

        // notification
        int Count_Notification();
        List<Integer> List_Notification();
        String MaxRowV_Notification();
        void Insert_Notification(String Q);
        void Delete_Notification();
    }
}