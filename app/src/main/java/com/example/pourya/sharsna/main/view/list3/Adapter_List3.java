package com.example.pourya.sharsna.main.view.list3;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.shop.Shop;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.util.List;

public class Adapter_List3 extends RecyclerView.Adapter<Adapter_List3.ViewHolder> {

    Context context;
    Activity activity;
    List<Users> list;
    boolean from_search, off_gps;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";
    boolean toast = true;

    public Adapter_List3(Context c, Activity a, List<Users> l, boolean fs, boolean g) {
        context = c;
        activity = a;
        list = l;
        from_search = fs;
        off_gps = g;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, address, dis, abc;
        ImageView image, img_selector_adp;
        RatingBar rating;
        RelativeLayout main_lst3;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.txt_name_adp);
            address = (TextView) itemView.findViewById(R.id.txt_address_adp);
            image = (ImageView) itemView.findViewById(R.id.img_main_adp);
            img_selector_adp = (ImageView) itemView.findViewById(R.id.img_selector_adp);
            rating = (RatingBar) itemView.findViewById(R.id.ratingbar_adp);
            main_lst3 = (RelativeLayout) itemView.findViewById(R.id.main_lst3);
            dis = (TextView) itemView.findViewById(R.id.dis);
            abc = (TextView) itemView.findViewById(R.id.abc);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_list3, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // not show
        if(list.get(position).getId() == -1 || list.get(position).getNameShop().equals((""))){
           // holder.main_lst3.setVisibility(View.INVISIBLE);
            holder.name.setText("");
            holder.address.setText("");
            holder.image.setImageResource(R.drawable.non);
            holder.dis.setText("");
            holder.abc.setText("");
            holder.rating.setVisibility(View.INVISIBLE);
            holder.main_lst3.setBackgroundResource(R.drawable.non);
            holder.img_selector_adp.setImageResource(R.drawable.non);
        }
        // normal
        else {
            holder.main_lst3.setBackgroundResource(R.color.colorPrimary);
            holder.img_selector_adp.setImageResource(R.drawable.ic_chevron_left_black_24dp);
            holder.rating.setVisibility(View.VISIBLE);
            holder.name.setText(list.get(position).getNameShop().toString());
            holder.address.setText(list.get(position).getAddress().toString());
            float distance = list.get(position).getDistance();
            if(distance == 0 || off_gps){
                holder.dis.setVisibility(View.GONE);
                holder.abc.setVisibility(View.GONE);
            }else{
                if(distance > 99){
                    // gps not valid
                    if(toast){
                        Toast.makeText(context, "اطلاعات مکانی شما در دسترس نیست دریافت اطلاعات از GPS مقداری طول خواهد کشید", Toast.LENGTH_LONG).show();
                        toast = false;
                    }
                    holder.dis.setVisibility(View.GONE);
                    holder.abc.setVisibility(View.GONE);
                }else{
                    holder.dis.setText(distance + "");
                }
            }
            String r = list.get(position).getRating();
            if(r == null || r.equals("null") || r.equals("")){
                holder.rating.setRating((float) 0);
            }else{
                holder.rating.setRating((float) Float.parseFloat(list.get(position).getRating()));
            }
            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list.get(position).getPicProfile();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(holder.image);
            }catch (Exception e){
                new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
            }

            final int id = list.get(position).getId();
            holder.main_lst3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(from_search){
                        Shop.is_search = true;
                        Shop.search_result = list.get(position);
                        Shop.selected = false;
                        Shop.profile = false;
                        Shop.bazar = false;
                        activity.finish();
                        view.getContext().startActivity(new Intent(view.getContext(), Shop.class));
                    }else {
                        Shop.Uid = id;
                        Shop.is_search = false;
                        Shop.selected = false;
                        Shop.profile = false;
                        Shop.bazar = false;
                        activity.finish();
                        view.getContext().startActivity(new Intent(view.getContext(), Shop.class));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}