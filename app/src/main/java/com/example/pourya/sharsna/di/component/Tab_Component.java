package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Tab_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Tab_Module.class )
public interface Tab_Component {
    Tabs_Main inject(Tabs_Main activity);
}