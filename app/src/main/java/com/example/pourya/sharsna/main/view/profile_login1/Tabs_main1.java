package com.example.pourya.sharsna.main.view.profile_login1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Tab1_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Tab1;
import com.example.pourya.sharsna.main.presenter.Tab1_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import javax.inject.Inject;

public class Tabs_main1 extends BaseActivity implements MVP_Tab1.RequiredViewOps,
        View.OnClickListener, TabLayout.OnTabSelectedListener,
        NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Tabs_main1.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer(getFragmentManager(), Tabs_main1.class.getName());

    @Inject
    public MVP_Tab1.ProvidedPresenterOps mPresenter;

    public static int IdUser;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    MaterialSpinner spinner;
    SpaceNavigationView spaceNavigationView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    LinearLayout navigation_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs_main1);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(3);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Tabs_main1.this.finish();
                Intent intent = new Intent(Tabs_main1.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {

                    case 1:
                        Tabs_main1.this.finish();
                        Intent intenthome = new Intent(Tabs_main1.this, List1.class);
                        startActivity(intenthome);
                        break;

                    case 0:
                        Tabs_main1.this.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent = new Intent(Tabs_main1.this, Main.class);
                        startActivity(intent);
                        break;

                    case 2:
                        Tabs_main1.this.finish();
                        Intent intentshop = new Intent(Tabs_main1.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        break;

                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();
        after_setup();
    }

    private void setupViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.pager);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
    }

    private void after_setup() {

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        navigation_icon.setOnClickListener(this);

        tabLayout.addTab(tabLayout.newTab().setText("مشخصات من"));
        tabLayout.addTab(tabLayout.newTab().setText("دنبال شده ها"));
        tabLayout.addTab(tabLayout.newTab().setText("دوستان من"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        tabLayout.setOnTabSelectedListener((TabLayout.OnTabSelectedListener) Tabs_main1.this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Tabs_main1.this.finish();
                startActivity(new Intent(Tabs_main1.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Tabs_main1.this.finish();
                startActivity(new Intent( Tabs_main1.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP() {
        if (mStateMaintainer.firstTimeIn()) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize() {
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Tab1_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Tab1_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if (mPresenter == null)
            setupComponent();
    }

    private void setupComponent() {
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getTab1Component(new Tab1_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //spaceNavigationView.changeCurrentItem(1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back() {
        Tabs_main1.this.finish();
        startActivity(new Intent(this, Main.class));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}
}