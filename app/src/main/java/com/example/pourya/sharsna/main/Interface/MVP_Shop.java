package com.example.pourya.sharsna.main.Interface;

import android.content.Context;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import com.example.pourya.sharsna.main.viewModel.Products;
import com.example.pourya.sharsna.main.viewModel.Users;

import java.util.List;

public interface MVP_Shop {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_Shop.RequiredViewOps view);

        // user
        Users Get_User(int id);

        // get product
        List<Products> List_Products(int id);
        String MaxRowV_Products();
        void Insert_Products(String Q);
        List<Integer> Id_Products();

        // gulid
        Gulids Get_Gulid(int id);

        // get city
        List<Cities> getCity();

        void update_follow(String follow ,int count, int id);
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // user
        Users Get_User(int id);

        // get product
        List<Products> List_Products(int id);
        String MaxRowV_Products();
        void Insert_Products(String Q);
        List<Integer> Id_Products();

        // gulid
        Gulids Get_Gulid(int id);

        // get city
        List<Cities> getCity();

        void update_follow(String follow ,int count, int id);
    }
}