package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Login1_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.login.Login1;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Login1_Module.class )
public interface Login1_Component {
    Login1 inject(Login1 activity);
}