package com.example.pourya.sharsna.main.view.splash;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Splash_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Splash;
import com.example.pourya.sharsna.main.api.Get_SelectedShop;
import com.example.pourya.sharsna.main.presenter.Splash_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Retry;
import com.example.pourya.sharsna.main.view.main.Main;
import javax.inject.Inject;

public class Splash extends BaseActivity implements MVP_Splash.RequiredViewOps  {

    private static final String TAG = Splash.class.getSimpleName();

    @Inject
    public MVP_Splash.ProvidedPresenterOps mPresenter;
    private final StateMaintainer mStateMaintainer = new StateMaintainer(getFragmentManager(), Splash.class.getName());

    GifImageView gifImageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        gifImageView = (GifImageView) findViewById(R.id.GifImageView);
        gifImageView.setGifImageResource(R.drawable.loading);

        // no login 1
        //Login1.sp1 = getSharedPreferences("DB1",0);
        //SharedPreferences.Editor edit1 = Login1.sp1.edit();
        //edit1.putString("uid1","");
        //edit1.commit();
        // no login 2
        //Login2.sp2 = getSharedPreferences("DB2",0);
        //SharedPreferences.Editor edit2 = Login2.sp2.edit();
        //edit2.putString("uid2","");
        //edit2.commit();

        setupViews();
        setupMVP();
        after_setup();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(haveNetworkConnection()){
                    new Get_SelectedShop(haveNetworkConnection(), mPresenter, getAppContext(), Splash.this, 31);
                }else{
                    alert();
                }
            }
        }, 1500);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy(isChangingConfigurations());
    }

    private void setupViews(){}

    private void after_setup(){}

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Splash_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Splash_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getSplashComponent(new Splash_Module(this))
                .inject(this);
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    public void alert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("دسترسی به اینترنت امکان پذیر نیست." + "\n\n" + "آیا مایل به ورود به برنامه با اطلاعات قبلی هستید ؟")
                .setCancelable(false)
                .setPositiveButton("بله.  ادامه میدم    ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Splash.this.finish();
                        dialog.cancel();
                        startActivity(new Intent(Splash.this, Main.class));
                    }
                })
                .setNegativeButton("نه.  خارج میشم", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Splash.this.finish();
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle(" ");
        alert.setIcon(R.drawable.wifi);
        alert.show();
    }
}