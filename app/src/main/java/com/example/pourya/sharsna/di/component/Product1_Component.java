package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Product1_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.shop.Product1;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Product1_Module.class )
public interface Product1_Component {
    Product1 inject(Product1 activity);
}