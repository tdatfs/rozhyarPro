package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_List1;
import com.example.pourya.sharsna.main.cls.Set_List1;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.main.Main;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class Get_List1 extends BaseActivity {

    MVP_List1.ProvidedPresenterOps list1_presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;

    public Get_List1(boolean net, MVP_List1.ProvidedPresenterOps ppo, Context context, Activity activity, RecyclerView r) {
        list1_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        progressDialog = new ProgressDialog(mactivity);
        recyclerView = r;
        get();
    }

    public Get_List1 get() {

        if(Main.list1 == 0){
            if(mnet){
                progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
                progressDialog.show();
                JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                        url+ "DropDwon/DpGulid", null, new Response.Listener<JSONArray>() {

                    //?row="+list1_presenter.MaxRowV_Gulids()

                    @Override
                    public void onResponse(JSONArray response) {

                        boolean insert = false;
                        List<Integer> list_gulids = list1_presenter.Id_Gulids();

                        try {
                            String Q1 = "insert into Gulids (Id,GuildName,ParentId,Icon,RowVersion,GetServer) values ";
                            for (int i=0; i<response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                String id = jsonObject.getString("Id");
                                String name = jsonObject.getString("GuildName");
                                String parent = jsonObject.getString("ParentId");
                                String icon = jsonObject.getString("Icon");
                                String row = "1";
                                String status = "false";

                                int Id = Integer.parseInt(id);

                                int type = whatdo(list_gulids, Id);
                                // insert
                                if(type == 1){
                                    insert = true;
                                    Q1 = Q1.concat("('" + id + "','" + name + "','" + parent + "','" + icon + "','" + row + "','" + status + "')," );
                                }
                                // update
                                if(type == 2){
                                    String Q2="update Gulids set GuildName='"+name+"',ParentId='"+parent+"',Icon='"+icon+"',RowVersion='"+row+"',GetServer='"+status+"' where Id="+Id;
                                    list1_presenter.Insert_Gulids(Q2);
                                }
                            }
                            if(insert) {
                                Q1 = Q1.substring(0, Q1.trim().length() - 1).concat(";");
                                list1_presenter.Insert_Gulids(Q1);
                            }
                            progressDialog.dismiss();
                            Main.list1 = 1;
                            Set_List1 set1 = new Set_List1(list1_presenter,_context,mactivity,recyclerView);
                            set1.load();

                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                            new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        new ErrorVolley(_context).Error(volleyError,"get");
                        if (volleyError.networkResponse == null) {
                            if (volleyError.getClass().equals(TimeoutError.class)) {
                                Set_List1 set1 = new Set_List1(list1_presenter,_context,mactivity,recyclerView);
                                set1.load();
                            }
                        }
                    }
                });

                SampleApp.getInstance().addToRequestQueue(jsonObjReq);

            }else{
                Set_List1 set1 = new Set_List1(list1_presenter,_context,mactivity,recyclerView);
                set1.load();
            }
        }
        // not repeat again
        else{
            Set_List1 set1 = new Set_List1(list1_presenter,_context,mactivity,recyclerView);
            set1.load();
        }
        return null;
    }

    public int whatdo(List<Integer> listFunction, int id){
        int result = 1;
        for(int j=0 ; j < listFunction.size() ; j++) {
            if(listFunction.get(j) == id){
                result = 2;
            }
        }
        return result;
    }
}