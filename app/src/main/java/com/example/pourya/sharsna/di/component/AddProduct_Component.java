package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.AddProduct_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.profile_login2.AddProduct;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = AddProduct_Module.class )
public interface AddProduct_Component {
    AddProduct inject(AddProduct activity);
}