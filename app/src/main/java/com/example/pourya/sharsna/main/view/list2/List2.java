package com.example.pourya.sharsna.main.view.list2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.List2_Module;
import com.example.pourya.sharsna.main.Interface.MVP_List2;
import com.example.pourya.sharsna.main.presenter.List2_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class List2 extends BaseActivity implements MVP_List2.RequiredViewOps, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = List2.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), List2.class.getName());

    @Inject
    public MVP_List2.ProvidedPresenterOps mPresenter;

    public static int Gid;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    RecyclerView recyclerView;
    LinearLayout navigation_icon;
    List<Gulids> list2;
    MaterialSpinner spinner;
    List<Cities> list_City = new ArrayList<>();
    SpaceNavigationView spaceNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list2);


        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(-1);
        spaceNavigationView.bringToFront();
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                List2.this.finish();
                Intent intent = new Intent(List2.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        List2.this.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent = new Intent(List2.this, Main.class);
                        startActivity(intent);
                        break;

                    case 1:
                        List2.this.finish();
                        Intent intentmenu = new Intent(List2.this, List1.class);
                        startActivity(intentmenu);
                        break;

                    case 2:
                        List2.this.finish();
                        Intent intentshop = new Intent(List2.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
            }
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();
        list2 = mPresenter.List_Gulids(Gid);
        Gulids app = new Gulids();
        app.setGuildName("");
        app.setId(-1);
        list2.add(app);

        after_setup();
    }

    private void setupViews(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        recyclerView = (RecyclerView) findViewById(R.id.rv_list_tow);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
    }

    private void after_setup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        Adapter_List2 adapter = new Adapter_List2(getActivityContext(),this, list2);
        adapter.notifyItemInserted(list2.size() - 1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        navigation_icon.setOnClickListener(this);

        /*fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new DecelerateInterpolator());
        fadeOut.setDuration(300);
        fadein = new AlphaAnimation(0, 1);
        fadein.setInterpolator(new DecelerateInterpolator());
        fadein.setDuration(300);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    spaceNavigationView.startAnimation(fadein);
                    spaceNavigationView.setVisibility(View.INVISIBLE);
                    spaceNavigationView.startAnimation(fadeOut);
                } else {
                    spaceNavigationView.startAnimation(fadein);
                    spaceNavigationView.setVisibility(View.VISIBLE);
                    spaceNavigationView.startAnimation(fadeOut);
                }
            }
        });*/
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                List2.this.finish();
                startActivity(new Intent(List2.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                List2.this.finish();
                startActivity(new Intent( List2.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(List2_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(List2_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getList2Component(new List2_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(-1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        List2.this.finish();
        startActivity(new Intent(this, List1.class));
    }
}