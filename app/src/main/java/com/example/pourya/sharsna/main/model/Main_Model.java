package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Notifications;
import com.example.pourya.sharsna.main.viewModel.SelectedShops;
import com.example.pourya.sharsna.main.viewModel.Sliders;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.util.ArrayList;
import java.util.List;

public class Main_Model implements MVP_Main.ProvidedModelOps {

    private MVP_Main.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Users> UList;
    private List<Cities> CList;
    private List<Sliders> SList;
    private List<SelectedShops> SSList;
    private List<Notifications> NList;
    private List<Integer> id_user;
    private List<Integer> id_selectedshop;

    public Main_Model(MVP_Main.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        UList = new ArrayList<>();
        CList = new ArrayList<>();
        SList = new ArrayList<>();
        SSList = new ArrayList<>();
        NList = new ArrayList<>();
        id_user = new ArrayList<>();
        id_selectedshop = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public List<Cities> getCity() {
        try{
            String q = "SELECT [Id],[Title],[Selected] FROM [Cities]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Cities app = new Cities();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setTitle(cursor.getString(1));
                app.setSelected(cursor.getString(2));
                CList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return CList;
    }

    @Override
    public List<Sliders> getSlider(int cid) {
        SList = new ArrayList<>();
        try{
            String q = "SELECT [Id],[Pic],[CityId],[RowVersion] FROM [Sliders] where CityId="+cid+" OR CityId='null'";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Sliders app = new Sliders();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setPic(cursor.getString(1));
                app.setCityId(cursor.getString(2));
                app.setRowVersion(cursor.getString(3));
                SList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return SList;
    }

    @Override
    public List<SelectedShops> getSelectedShop(int cid) {
        SSList = new ArrayList<>();
        try{
            String q = "SELECT [UserId],[UserNameShop],[UserPicProfile] FROM [SelectedShops] where CityId = "+cid;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                SelectedShops app = new SelectedShops();
                app.setUserId(Integer.parseInt(cursor.getString(0)));
                app.setUserNameShop(cursor.getString(1));
                app.setUserPicProfile(cursor.getString(2));
                SSList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return SSList;
    }

    @Override
    public int Count_SelectedShop() {
        int count = 0;
        try {
            String q =  "SELECT [UserId] FROM [SelectedShops]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            count = cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context, ex.getMessage(), Utility.getMethodName()).postError();
        }
        return count;
    }

    @Override
    public List<Integer> List_SelectedShop() {
        try{
            String q = "SELECT [UserId] FROM [SelectedShops]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_selectedshop.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_selectedshop;
    }

    @Override
    public void Delete_SelectedShop() {
        String q = "Delete from [SelectedShops]";
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int count = cursor.getCount();
        cursor.moveToFirst();
    }

    @Override
    public void Insert_SelectedShop(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public List<Notifications> getNotification() {
        try{
            String q = "SELECT [Id],[Text],[RowVersion] FROM [Notifications]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Notifications app = new Notifications();
                app.set_id(Integer.parseInt(cursor.getString(0)));
                app.setText(cursor.getString(1));
                app.setRowVersion(cursor.getString(2));
                NList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return NList;
    }

    @Override
    public List<Users> getShop() {
        try{
            String q = "SELECT [Id],[GulidId],[NameShop],[Address],[Operator],[Phone],[PicProfile],[CellPhone],[RowVersion] FROM [Users]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Users app = new Users();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setGulidId(Integer.parseInt(cursor.getString(1)));
                app.setNameShop(cursor.getString(2));
                app.setAddress(cursor.getString(3));
                app.setOperator(cursor.getString(4));
                app.setPhone(cursor.getString(5));
                app.setPicProfile(cursor.getString(6));
                app.setCellPhone(cursor.getString(7));
                app.setRowVersion(cursor.getString(8));
                UList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return UList;
    }

    @Override
    public String MaxRowV_Users() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Users";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public void Insert_Users(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public List<Integer> Id_Users() {
        try{
            String q = "SELECT [Id] FROM [Users]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_user.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_user;
    }

    @Override
    public String Status_Selected(int id) {
        String result = "";
        try{
            String q = "SELECT [GetServer] FROM [SelectedShops] where [UserId] = "+id;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count = cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                result = cursor.getString(0);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return result;
    }

    @Override
    public void Chanege_Status_Selected(int id) {
        String q = "update [SelectedShops] set [GetServer]='true' where [UserId] ="+id;
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int count = cursor.getCount();
        cursor.moveToFirst();
    }
}