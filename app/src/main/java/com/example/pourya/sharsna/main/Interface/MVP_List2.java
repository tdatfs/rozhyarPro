package com.example.pourya.sharsna.main.Interface;

import android.content.Context;

import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.util.List;

public interface MVP_List2 {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_List2.RequiredViewOps view);

        // gulids
        List<Gulids> List_Gulids(int id);

        // get city
        List<Cities> getCity();
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // gulids
        List<Gulids> List_Gulids(int id);

        // get city
        List<Cities> getCity();
    }
}