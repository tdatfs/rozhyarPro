package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Product;
import com.example.pourya.sharsna.main.model.Product_Model;
import com.example.pourya.sharsna.main.presenter.Product_Presenter;
import com.example.pourya.sharsna.main.view.tabs.Product;
import dagger.Module;
import dagger.Provides;

@Module
public class Product_Module {

    private Product activity;

    public Product_Module(Product activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Product  providesProductActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Product.ProvidedPresenterOps providedPresenterOps() {
        Product_Presenter presenter = new Product_Presenter( activity );
        Product_Model model = new Product_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
