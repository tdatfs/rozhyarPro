package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_Shop;
import com.example.pourya.sharsna.main.cls.Set_Shop;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.main.Main;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class Get_Product extends BaseActivity {

    MVP_Shop.ProvidedPresenterOps shop_presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    int id;
    String mid, uid;

    public Get_Product(boolean net, MVP_Shop.ProvidedPresenterOps ppo, Context context, Activity activity, RecyclerView r, int i, String m, String u) {
        shop_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        progressDialog = new ProgressDialog(mactivity);
        recyclerView = r;
        id = i;
        mid = m;
        uid = u;
        get();
    }

    public Get_Product get() {
        if(mnet){
            if (mid.equals("")) {
                mid = "0";
            }
            if (uid.equals("")) {
                uid = "0";
            }
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    url+ "ApiProduct/ListForUser?id="+id+"&memberId="+mid+"&userid="+uid, null, new Response.Listener<JSONArray>() {

                //?row="+list1_presenter.MaxRowV_Gulids()

                @Override
                public void onResponse(JSONArray response) {

                    boolean insert = false;
                    List<Integer> list_products = shop_presenter.Id_Products();

                    try {
                        String Q1 = "insert into Products (Id,UserId,NameProduct,PicProduct,Price,InsertDate,Des,OffProduct,DateExpire,DateExpireshamsi,IsActive,CountView,CountLike,IsConfirm,IsLike,RowVersion) values ";
                        for (int i=0; i<response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            String id = jsonObject.getString("Id");
                            String uid = jsonObject.getString("UserId");
                            String name = jsonObject.getString("NameProduct");
                            String pic = jsonObject.getString("PicProduct");
                            String price = jsonObject.getString("Price");
                            String insert_date = jsonObject.getString("InsertDate");
                            String des = jsonObject.getString("Des");
                            String off = jsonObject.getString("OffProduct");
                            String expire = jsonObject.getString("DateExpire");
                            String expire_SH = jsonObject.getString("DateExpireshamsi");
                            String active = jsonObject.getString("IsActive");
                            String c_view = jsonObject.getString("CountView");
                            String c_like = jsonObject.getString("CountLike");
                            String confirm = jsonObject.getString("IsConfirm");
                            String islike = jsonObject.getString("IsLike");
                            String row = "1";

                            int Id = Integer.parseInt(id);
                            int Uid = Integer.parseInt(uid);

                            int type = whatdo(list_products, Id);
                            // insert
                            if(type == 1){
                                insert = true;
                                Q1 = Q1.concat("('" + Id + "','" + Uid + "','" + name + "','" + pic + "','" + price + "','" + insert_date + "','" + des + "','" + off + "','" + expire + "','" + expire_SH + "','" + active + "','" + c_view + "','" + c_like + "','" + confirm + "','" + islike + "','" + row + "')," );
                            }
                            // update
                            if(type == 2){
                                String Q2="update Products set UserId='"+Uid+"',NameProduct='"+name+"',PicProduct='"+pic+"',Price='"+price+"',InsertDate='"+insert_date+"',Des='"+des+"',OffProduct='"+off+"',DateExpire='"+expire+"',DateExpireshamsi='"+expire_SH+"',IsActive='"+active+"',CountView='"+c_view+"',CountLike='"+c_like+"',IsConfirm='"+confirm+"',IsLike='"+islike+"',RowVersion='"+row+"' where Id="+Id;
                                shop_presenter.Insert_Products(Q2);
                            }
                        }
                        if(insert) {
                            Q1 = Q1.substring(0, Q1.trim().length() - 1).concat(";");
                            shop_presenter.Insert_Products(Q1);
                        }
                        progressDialog.dismiss();
                        Main.shop_product = 1;
                        Set_Shop set = new Set_Shop(shop_presenter,_context,mactivity,recyclerView,id);
                        set.load();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {
                            Set_Shop set = new Set_Shop(shop_presenter,_context,mactivity,recyclerView,id);
                            set.load();
                        }
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        }else{
            Set_Shop set = new Set_Shop(shop_presenter,_context,mactivity,recyclerView,id);
            set.load();
        }
        return null;
    }

    public int whatdo(List<Integer> listFunction, int id){
        int result = 1;
        for(int j=0 ; j < listFunction.size() ; j++) {
            if(listFunction.get(j) == id){
                result = 2;
            }
        }
        return result;
    }
}