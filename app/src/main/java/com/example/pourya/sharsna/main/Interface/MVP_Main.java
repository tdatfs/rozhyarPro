package com.example.pourya.sharsna.main.Interface;

import android.content.Context;

import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Notifications;
import com.example.pourya.sharsna.main.viewModel.SelectedShops;
import com.example.pourya.sharsna.main.viewModel.Sliders;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.util.List;

public interface MVP_Main {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_Main.RequiredViewOps view);

        // get city
        List<Cities> getCity();

        // get slider
        List<Sliders> getSlider(int cid);

        // get selectedshop
        List<SelectedShops> getSelectedShop(int cid);
        int Count_SelectedShop();
        List<Integer> List_SelectedShop();
        void Delete_SelectedShop();
        void Insert_SelectedShop(String Q);

        // get notification
        List<Notifications> getNotification();

        // get user
        String MaxRowV_Users();
        void Insert_Users(String Q);
        List<Integer> Id_Users();

        // get one shop
        List<Users> getShop();

        String Status_Selected(int id);
        void Chanege_Status_Selected(int id);
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // get city
        List<Cities> getCity();

        // get slider
        List<Sliders> getSlider(int cid);

        // get selectedshop
        List<SelectedShops> getSelectedShop(int cid);
        int Count_SelectedShop();
        List<Integer> List_SelectedShop();
        void Delete_SelectedShop();
        void Insert_SelectedShop(String Q);

        // get notification
        List<Notifications> getNotification();

        // get user
        String MaxRowV_Users();
        void Insert_Users(String Q);
        List<Integer> Id_Users();

        // get one shop
        List<Users> getShop();

        String Status_Selected(int id);
        void Chanege_Status_Selected(int id);
    }
}