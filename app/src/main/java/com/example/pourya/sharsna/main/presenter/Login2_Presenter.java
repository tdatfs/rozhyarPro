package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Login2;
import java.lang.ref.WeakReference;

public class Login2_Presenter implements MVP_Login2.ProvidedPresenterOps, MVP_Login2.RequiredPresenterOps {

    private WeakReference<MVP_Login2.RequiredViewOps> mView;
    private MVP_Login2.ProvidedModelOps mModel;

    public Login2_Presenter(MVP_Login2.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Login2.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Login2.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(MVP_Login2.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}