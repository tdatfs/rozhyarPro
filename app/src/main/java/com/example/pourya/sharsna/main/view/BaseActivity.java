package com.example.pourya.sharsna.main.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;
import com.example.pourya.sharsna.main.view.login.Login;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.login.Login2;
import com.example.pourya.sharsna.main.view.profile_login1.Tabs_main1;
import com.example.pourya.sharsna.main.view.profile_login2.Tabs_main2;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;

public class BaseActivity extends AppCompatActivity {

    // policy
    int socketTimeout = 30000;
    public RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    public String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";
    public String url = "http://rozhyar.tdaapp.ir/api/";

    public Context getActivityContext() {
        return this;
    }

    public Context getAppContext() {  return getApplicationContext(); }

    public String get_member_id(){
        Login1.sp1 = getApplicationContext().getSharedPreferences("DB1",0);
        String member_id = Login1.sp1.getString("uid1","");
        return member_id;
    }

    public String get_user_id(){
        Login2.sp2 = getApplicationContext().getSharedPreferences("DB2",0);
        String user_id = Login2.sp2.getString("uid2","");
        return user_id;
    }

    public void profile_status(){
        Login1.sp1 = getApplicationContext().getSharedPreferences("DB1",0);
        String uid_shp_1 = Login1.sp1.getString("uid1","");
        String username_shp_1 = Login1.sp1.getString("user1","");

        Login2.sp2 = getApplicationContext().getSharedPreferences("DB2",0);
        String uid_shp_2 = Login2.sp2.getString("uid2","");
        String username_shp_2 = Login1.sp1.getString("user2","");

        if(uid_shp_1.equals("") && uid_shp_2.equals("")){
            if(username_shp_1.equals("") && username_shp_2.equals("")){
                // no status then login
                this.finish();
                Tabs_Main.city_id = 0;
                startActivity(new Intent(getApplicationContext(), Login.class));
            }
            if(username_shp_1.equals("")){
            }else{
                // status : login1 have data
                this.finish();
                Tabs_Main.city_id = 0;
                startActivity(new Intent(getApplicationContext(), Login1.class));
            }
            if(username_shp_2.equals("")){
            }else{
                // status : login2 have data
                this.finish();
                Tabs_Main.city_id = 0;
                startActivity(new Intent(getApplicationContext(), Login1.class));
            }
        }else{
            if(uid_shp_1.equals("")){
            }else{
                // status : login1 is active
                this.finish();
                Tabs_Main.city_id = 0;
                Tabs_main1.IdUser = Integer.parseInt(uid_shp_1);
                startActivity(new Intent(getApplicationContext(), Tabs_main1.class));
            }
            if(uid_shp_2.equals("")){
            }else{
                // status : login2 is active
                this.finish();
                Tabs_Main.city_id = 0;
                Tabs_main2.IdUser = Integer.parseInt(uid_shp_2);
                startActivity(new Intent(getApplicationContext(), Tabs_main2.class));
            }
        }
    }

    public void clear_login1(){
        Login1.sp1 = getSharedPreferences("DB1",0);
        SharedPreferences.Editor edit1 = Login1.sp1.edit();
        edit1.putString("uid1","");
        edit1.putString("user1","");
        edit1.commit();
    }

    public void clear_login2(){
        Login2.sp2 = getSharedPreferences("DB2",0);
        SharedPreferences.Editor edit2 = Login2.sp2.edit();
        edit2.putString("uid2","");
        edit2.putString("user2","");
        edit2.putString("pass2","");
        edit2.commit();
    }

    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static String faToEn(String num) {
        return num
                .replace("۰", "0")
                .replace("۱", "1")
                .replace("۲", "2")
                .replace("۳", "3")
                .replace("۴", "4")
                .replace("۵", "5")
                .replace("۶", "6")
                .replace("۷", "7")
                .replace("۸", "8")
                .replace("۹", "9");
    }

    public boolean cheak_date(String now, String select){
        boolean date = false;
        String a1[] = now.split("/");
        String a2[] = select.split("/");
        // year <
        if ( Integer.parseInt(a1[0]) > Integer.parseInt(a2[0]) ){
            date = false;
        }
        // year >
        if ( Integer.parseInt(a1[0]) < Integer.parseInt(a2[0]) ){
            date = true;
        }
        // year =
        if ( Integer.parseInt(a1[0]) == Integer.parseInt(a2[0]) ){
            // month <
            if( Integer.parseInt(a1[1]) > Integer.parseInt(a2[1]) ){
                date = false;
            }
            // month >
            if( Integer.parseInt(a1[1]) < Integer.parseInt(a2[1]) ){
                date = true;
            }
            // month =
            if( Integer.parseInt(a1[1]) == Integer.parseInt(a2[1]) ){
                // day <
                if ( Integer.parseInt(a1[2]) > Integer.parseInt(a2[2]) ){
                    date = false;
                }
                // day >
                if ( Integer.parseInt(a1[2]) < Integer.parseInt(a2[2]) ){
                    date = true;
                }
                // day =
                if ( Integer.parseInt(a1[2]) == Integer.parseInt(a2[2]) ){
                    date = true;
                }
            }
        }
        return date;
    }
}