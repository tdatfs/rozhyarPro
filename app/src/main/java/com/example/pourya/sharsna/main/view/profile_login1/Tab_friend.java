package com.example.pourya.sharsna.main.view.profile_login1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Get_Friends;

public class Tab_friend extends Fragment {

    RecyclerView recyclerView;
    Button add;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_friends, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_list3);
        new Get_Friends(getContext(), getActivity(), recyclerView, Tabs_main1.IdUser);

        add = (Button) view.findViewById(R.id.add_friend);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Friend.class));
            }
        });

        return view;
    }
}