package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Tab;
import com.example.pourya.sharsna.main.model.Tab_Model;
import com.example.pourya.sharsna.main.presenter.Tab_Presenter;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import dagger.Module;
import dagger.Provides;

@Module
public class Tab_Module {

    private Tabs_Main activity;

    public Tab_Module(Tabs_Main activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Tabs_Main providesTabActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Tab.ProvidedPresenterOps providedPresenterOps() {
        Tab_Presenter presenter = new Tab_Presenter( activity );
        Tab_Model model = new Tab_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
