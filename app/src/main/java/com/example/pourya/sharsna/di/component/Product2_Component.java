package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Product2_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.profile_login2.Product2;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Product2_Module.class )
public interface Product2_Component {
    Product2 inject(Product2 activity);
}