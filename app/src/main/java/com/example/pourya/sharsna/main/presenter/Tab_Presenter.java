package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Tab;
import com.example.pourya.sharsna.main.viewModel.Cities;
import java.lang.ref.WeakReference;
import java.util.List;

public class Tab_Presenter implements MVP_Tab.ProvidedPresenterOps, MVP_Tab.RequiredPresenterOps{

    private WeakReference<MVP_Tab.RequiredViewOps> mView;
    private MVP_Tab.ProvidedModelOps mModel;

    public Tab_Presenter(MVP_Tab.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Tab.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Tab.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    public void setModel(MVP_Tab.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}