package com.example.pourya.sharsna.main.viewModel;

public class Products {
    private int Id;
    private int UserId;
    private String NameProduct;
    private String PicProduct;
    private String Price;
    private String InsertDate;
    private String Des;
    private String OffProduct;
    private String DateExpire;
    private String DateExpireshamsi;
    private String IsActive;
    private String CountView;
    private String CountLike;
    private String IsConfirm;
    private int CityId;
    private String IsLike;
    private String RowVersion;

    public Products(){}

    public String getIsLike() {
        return IsLike;
    }

    public void setIsLike(String isLike) {
        IsLike = isLike;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getDateExpireshamsi() {
        return DateExpireshamsi;
    }

    public void setDateExpireshamsi(String dateExpireshamsi) {
        DateExpireshamsi = dateExpireshamsi;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getNameProduct() {
        return NameProduct;
    }

    public void setNameProduct(String nameProduct) {
        NameProduct = nameProduct;
    }

    public String getPicProduct() {
        return PicProduct;
    }

    public void setPicProduct(String picProduct) {
        PicProduct = picProduct;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getInsertDate() {
        return InsertDate;
    }

    public void setInsertDate(String insertDate) {
        InsertDate = insertDate;
    }

    public String getDes() {
        return Des;
    }

    public void setDes(String des) {
        Des = des;
    }

    public String getOffProduct() {
        return OffProduct;
    }

    public void setOffProduct(String offProduct) {
        OffProduct = offProduct;
    }

    public String getDateExpire() {
        return DateExpire;
    }

    public void setDateExpire(String dateExpire) {
        DateExpire = dateExpire;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCountView() {
        return CountView;
    }

    public void setCountView(String countView) {
        CountView = countView;
    }

    public String getCountLike() {
        return CountLike;
    }

    public void setCountLike(String countLike) {
        CountLike = countLike;
    }

    public String getIsConfirm() {
        return IsConfirm;
    }

    public void setIsConfirm(String isConfirm) {
        IsConfirm = isConfirm;
    }

    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String rowVersion) {
        RowVersion = rowVersion;
    }
}
