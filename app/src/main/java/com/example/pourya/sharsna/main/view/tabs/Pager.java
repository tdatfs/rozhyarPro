package com.example.pourya.sharsna.main.view.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class Pager extends FragmentStatePagerAdapter {

    int tabCount;

    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Tabs_New tb1 = new Tabs_New();
                return tb1;
            case 1:
                Tabs_Popular tb2 = new Tabs_Popular();
                return tb2;
            case 2:
                Tabs_Discount tb3 = new Tabs_Discount();
                return tb3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
