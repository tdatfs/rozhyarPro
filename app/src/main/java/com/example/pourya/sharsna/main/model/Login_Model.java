package com.example.pourya.sharsna.main.model;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Login;
import com.example.pourya.sharsna.main.db.DBAdapter;

public class Login_Model implements MVP_Login.ProvidedModelOps {

    private MVP_Login.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;

    public Login_Model(MVP_Login.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }
}