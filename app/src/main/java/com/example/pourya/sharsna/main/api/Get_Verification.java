package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.BaseSetingApi;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Code;
import com.example.pourya.sharsna.main.view.login.Code;

public class Get_Verification extends BaseActivity {
    Context _context;
    Activity mactivity;
    boolean mnet;
    String mobile;
    ProgressDialog progressDialog;
    boolean pop;

    public Get_Verification(boolean net,Context context, Activity activity, String m, boolean p) {
        mactivity = activity;
        _context = context;
        mnet = net;
        mobile = m;
        pop = p;
        progressDialog = new ProgressDialog(activity);
        get();
    }

    public Get_Verification get() {
        if(mnet){
            progressDialog.setMessage("لطفا صبر کنید ...");
            progressDialog.show();

            StringRequest sr = new StringRequest(Request.Method.GET, url+ "ApiUser/VerificationCode?id="+mobile,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try{
                                if(response.equals("true")){
                                    progressDialog.dismiss();
                                    if(pop){
                                        Dialog_Code cdd = new Dialog_Code(mactivity,mobile);
                                        cdd.show();
                                    }else{
                                        mactivity.finish();
                                        Code.enter_mobile = mobile;
                                        mactivity.startActivity(new Intent(mactivity, Code.class));
                                    }
                                }else{
                                    progressDialog.dismiss();
                                    Toast.makeText(_context, "اطلاعات وارد شده صحیح نیست", Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(sr);

        }else{
            Toast.makeText(_context, "دسترسی به اینترنت امکان پذیر نیست", Toast.LENGTH_LONG).show();
        }
        return null;
    }
}