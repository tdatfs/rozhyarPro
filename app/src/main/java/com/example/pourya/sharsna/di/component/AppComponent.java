package com.example.pourya.sharsna.di.component;

import android.app.Application;
import com.example.pourya.sharsna.di.module.AddProduct_Module;
import com.example.pourya.sharsna.di.module.AppModule;
import com.example.pourya.sharsna.di.module.Code_Module;
import com.example.pourya.sharsna.di.module.Comment_Module;
import com.example.pourya.sharsna.di.module.List1_Module;
import com.example.pourya.sharsna.di.module.List2_Module;
import com.example.pourya.sharsna.di.module.List3_Module;
import com.example.pourya.sharsna.di.module.Login1_Module;
import com.example.pourya.sharsna.di.module.Login2_Module;
import com.example.pourya.sharsna.di.module.Login_Module;
import com.example.pourya.sharsna.di.module.Main_Module;
import com.example.pourya.sharsna.di.module.Product1_Module;
import com.example.pourya.sharsna.di.module.Product2_Module;
import com.example.pourya.sharsna.di.module.Product_Module;
import com.example.pourya.sharsna.di.module.Search_Module;
import com.example.pourya.sharsna.di.module.Shop_Module;
import com.example.pourya.sharsna.di.module.Splash_Module;
import com.example.pourya.sharsna.di.module.Tab1_Module;
import com.example.pourya.sharsna.di.module.Tab2_Module;
import com.example.pourya.sharsna.di.module.Tab_Module;
import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    Application application();

    Splash_Component getSplashComponent(Splash_Module module);
    Main_Component getMainComponent(Main_Module module);
    List1_Component getList1Component(List1_Module module);
    List2_Component getList2Component(List2_Module module);
    List3_Component getList3Component(List3_Module module);
    Shop_Component getShopComponent(Shop_Module module);
    Search_Component getSearchComponent(Search_Module module);
    Tab_Component getTabComponent(Tab_Module module);
    Login_Component getLoginComponent(Login_Module module);
    Login1_Component getLogin1Component(Login1_Module module);
    Code_Component getCodeComponent(Code_Module module);
    Login2_Component getLogin2Component(Login2_Module module);
    Tab1_Component getTab1Component(Tab1_Module module);
    Tab2_Component getTab2Component(Tab2_Module module);
    AddProduct_Component getAddProductComponent(AddProduct_Module module);
    Product_Component getProductComponent(Product_Module module);
    Product1_Component getProduct1Component(Product1_Module module);
    Product2_Component getProduct2Component(Product2_Module module);
    Comment_Component getCommentComponent(Comment_Module module);
}