package com.example.pourya.sharsna.main.cls;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.main.Adapter_Main;
import com.example.pourya.sharsna.main.viewModel.SelectedShops;
import java.util.List;

public class Set_SelectedShop extends BaseActivity {

    MVP_Main.ProvidedPresenterOps main_presenter;
    Context _context;
    Activity mactivity;
    RecyclerView mRecyclerView;
    List<SelectedShops> list_SelectedShop;
    boolean net;
    int Cid;

    public Set_SelectedShop(boolean n, MVP_Main.ProvidedPresenterOps ppo, Context c, Activity a, RecyclerView r, int ci) {
        _context = c;
        mactivity = a;
        net = n;
        main_presenter = ppo;
        mRecyclerView = r;
        Cid = ci;
    }

    public void load() {
        list_SelectedShop = main_presenter.getSelectedShop(Cid);
        Adapter_Main adapter = new Adapter_Main(net, main_presenter, _context ,mactivity, list_SelectedShop);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mactivity, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setAdapter(adapter);
    }
}