package com.example.pourya.sharsna.main.Interface;

import android.content.Context;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.util.List;

public interface MVP_Product1 {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_Product1.RequiredViewOps view);

        // product
        Products Get_Products(int id);

        // get city
        List<Cities> getCity();

        void update_like(String like, int count, int id);
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // product
        Products Get_Products(int id);

        // get city
        List<Cities> getCity();

        void update_like(String like, int count, int id);
    }
}