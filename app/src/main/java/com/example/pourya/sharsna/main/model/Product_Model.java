package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_Product;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;

import java.util.ArrayList;
import java.util.List;

public class Product_Model implements MVP_Product.ProvidedModelOps {

    private MVP_Product.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Integer> id_user;

    public Product_Model(MVP_Product.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        id_user = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public String MaxRowV_Users() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Users";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public void Insert_Users(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public List<Integer> Id_Users() {
        try{
            String q = "SELECT [Id] FROM [Users]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_user.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return id_user;
    }

    @Override
    public void update_like(String like, int count, int id) {
        String q = "update [Products] set [IsLike] = '"+ like +"' , [CountLike] = "+ count +" where [Id] = "+ id ;
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int c = cursor.getCount();
        cursor.moveToFirst();
    }
}