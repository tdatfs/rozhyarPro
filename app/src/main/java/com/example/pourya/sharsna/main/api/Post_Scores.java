package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.shop.CommentPage;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;

public class Post_Scores extends BaseActivity {

    Context _context;
    Activity _activity;
    boolean mnet;
    int userid;
    String memberid, comment, point, suserid;
    ProgressDialog progressDialog;

    public Post_Scores(boolean net, Context context, Activity activity, int uid, String mid, String suid, String com, String po) {

        progressDialog = new ProgressDialog(activity);
        mnet = net;
        _context = context;
        _activity = activity;
        userid = uid;
        suserid = suid;
        memberid = mid;
        comment = com;
        point = po;
    }

    public void post() throws JSONException {
        if(memberid.equals("") || memberid.equals("null")){
            memberid = "0";
        }
        if(suserid.equals("") || suserid.equals("null")){
            suserid = "0";
        }

        progressDialog.setMessage("لطفا صبر کنید ...");
        progressDialog.show();

        JSONObject _jsonBody=new JSONObject();
        _jsonBody.put("UserId", userid);
        _jsonBody.put("MemberId", memberid);
        _jsonBody.put("SecondUserId", suserid);
        _jsonBody.put("Comment", comment);
        _jsonBody.put("Point", point);

        RequestQueue requestQueue = Volley.newRequestQueue(_context);
        final String requestBody = _jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,url+"ApiScore/Score",
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(response.equals("204")){
                        progressDialog.dismiss();
                        Toast.makeText(_context,"نظر شما با موفقیت ثبت شد",Toast.LENGTH_LONG).show();
                        CommentPage.et_commetnts.setText("");
                        //CommentPage.rate.setIsIndicator(false);
                        _activity.finish();
                        Intent intent = new Intent(_activity, CommentPage.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        _activity.startActivity(intent);
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(_context,"خطا در ارسال اطلاعات به سرور",Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        new ErrorVolley(_context).Error(error, "post");
                    }
                })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}