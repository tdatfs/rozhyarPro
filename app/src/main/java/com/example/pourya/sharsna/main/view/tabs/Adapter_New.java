package com.example.pourya.sharsna.main.view.tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.util.List;

public class Adapter_New extends RecyclerView.Adapter<Adapter_New.ViewHolder> {

    Context context;
    Activity activity;
    List<Products> list;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_New(Context c, Activity a, List<Products> l) {
        context = c;
        activity = a;
        list = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private RatingBar rating;
        private ImageView img;
        private LinearLayout all;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            rating = (RatingBar) itemView.findViewById(R.id.ratinbar) ;
            img = (ImageView) itemView.findViewById(R.id.img_main);
            all = (LinearLayout) itemView.findViewById(R.id.all);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_new, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // not show
        if(list.get(position).getNameProduct().equals("")){
            holder.name.setText("");
            holder.rating.setVisibility(View.INVISIBLE);
            holder.img.setImageResource(R.drawable.non);
            holder.all.setBackgroundResource(R.drawable.non);
        }
        // normal
        else {
            holder.all.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.rating.setVisibility(View.VISIBLE);
            holder.name.setText(list.get(position).getNameProduct());
            if(list.get(position).getCountView().toString().equals("null") || list.get(position).getCountView().toString().equals("")){
                holder.rating.setRating((float) 0);
            }else{
                holder.rating.setRating((float) Float.parseFloat(list.get(position).getCountView()));
            }

            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list.get(position).getPicProduct();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(holder.img);
            }catch (Exception e){
                new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
            }

            final int id = list.get(position).getId();
            final int uid = list.get(position).getUserId();
            holder.all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product.Pid = id;
                    Product.UID = uid;
                    activity.finish();
                    Product.TabIndex = 0;
                    view.getContext().startActivity(new Intent(view.getContext(), Product.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}