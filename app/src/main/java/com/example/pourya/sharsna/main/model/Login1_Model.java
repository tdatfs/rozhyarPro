package com.example.pourya.sharsna.main.model;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Login1;
import com.example.pourya.sharsna.main.db.DBAdapter;

public class Login1_Model implements MVP_Login1.ProvidedModelOps {

    private MVP_Login1.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;

    public Login1_Model(MVP_Login1.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }
}