package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_Search;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list3.List3;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.viewModel.Users;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class Get_Search extends BaseActivity {

    MVP_Search.ProvidedPresenterOps search_presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;
    ProgressDialog progressDialog;
    String G_lat, G_long, Text;
    int city_id;
    String mid, uid;

    public Get_Search(boolean net, MVP_Search.ProvidedPresenterOps ppo, Context context, Activity activity, String lat, String lng, String txt, int c_id, String u, String m) {
        search_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        progressDialog = new ProgressDialog(mactivity);
        G_lat = lat;
        G_long = lng;
        Text = txt;
        city_id = c_id;
        uid = u;
        mid = m;
        get();
    }

    public Get_Search get() {

        if(mid.equals("")){
            mid = "0";
        }
        if(uid.equals("")){
            uid = "0";
        }

        final List<Users> UList = new ArrayList<>();
        if(mnet){
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();
            String encodedString = null;
            try {
                encodedString = URLEncoder.encode(Text, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    url+ "ApiShop/Search?Lat="+G_lat+"&Long="+G_long+"&Text="+encodedString+"&CityId="+city_id+"&UserId="+uid+"&MemberId="+mid , null, new Response.Listener<JSONArray>() {

            //?row="+list1_presenter.MaxRowV_Gulids()

                @Override
                public void onResponse(JSONArray response) {

                    int z = response.length();
                    try {
                        for (int i=0; i<response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            Users app = new Users();
                            app.setId(Integer.parseInt(jsonObject.getString("Id")));
                            app.setGulidId(Integer.parseInt(jsonObject.getString("GulidId")));
                            app.setGuildName(jsonObject.getString("GuildName"));
                            app.setUserTypeId(Integer.parseInt(jsonObject.getString("UserTypeId")));
                            app.setNameShop(jsonObject.getString("NameShop"));
                            app.setAddress(jsonObject.getString("Address"));
                            app.setOperator(jsonObject.getString("Operator"));
                            app.setPhone(jsonObject.getString("Phone"));
                            app.setPicProfile(jsonObject.getString("PicProfile"));
                            app.setCellPhone(jsonObject.getString("CellPhone"));
                            app.setEmail(jsonObject.getString("Email"));
                            app.setWebSilte(jsonObject.getString("WebSilte"));
                            app.setTelegram(jsonObject.getString("Telegram"));
                            app.setInstagram(jsonObject.getString("Instagram"));
                            app.setCountView(jsonObject.getString("CountView"));
                            app.setCountFollowe(jsonObject.getString("CountFollowe"));
                            app.setRating(jsonObject.getString("Rating"));
                            app.setFollow(jsonObject.getString("IsFollow"));
                            app.setGeneral(jsonObject.getString("IsGeneral"));
                            String d = jsonObject.getString("Distance");
                            if(d.equals("") || d.equals("null")){
                                app.setDistance(0);
                            }else{
                                double d1 = Double.parseDouble(d);
                                int d2 = (int) d1;
                                float d3 = (float) d2/1000;
                                float d4 = (float) (Math.round(d3*10.0)/10.0);
                                app.setDistance(d4);
                            }
                            UList.add(app);
                        }
                        progressDialog.dismiss();

                        if(UList.size() > 0){
                            // add 1 item null
                            Users app = new Users();
                            app.setId(-1);
                            app.setNameShop("");
                            UList.add(app);

                            // go list 3
                            mactivity.finish();
                            List3.is_search = true;
                            List3.search_result = UList;
                            Intent intent = new Intent(mactivity, List3.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.startActivity(intent);

                        }else{
                            // no result
                            Search.result.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        }else{}

        return null;
    }
}