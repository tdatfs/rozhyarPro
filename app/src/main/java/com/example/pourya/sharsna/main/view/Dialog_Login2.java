package com.example.pourya.sharsna.main.view;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Cheak_PLogin2;

public class Dialog_Login2 extends Dialog implements View.OnClickListener {

    Activity c;
    Dialog d;
    EditText mobile, password;
    TextView other_login;
    LinearLayout btn_login;

    public Dialog_Login2(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.plogin2);

        other_login = (TextView) findViewById(R.id.other_login);
        btn_login = (LinearLayout) findViewById(R.id.btn_login);
        mobile = (EditText) findViewById(R.id.mobile);
        password = (EditText) findViewById(R.id.password);

        other_login.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                String user = faToEn(mobile.getText().toString());
                String pass = faToEn(password.getText().toString());
                if(user.equals("") || pass.equals("")){
                    Toast.makeText(c, "شماره موبایل و رمز عبور خود را وارد کنید", Toast.LENGTH_LONG).show();
                }else{
                    dismiss();
                    new Cheak_PLogin2(true, c, c , user, pass);
                }
                break;

            case R.id.other_login:
                dismiss();
                Dialog_Login1 cdd=new Dialog_Login1(c);
                cdd.show();
                break;
        }
        dismiss();
    }

    public String faToEn(String num) {
        return num
                .replace("۰", "0")
                .replace("۱", "1")
                .replace("۲", "2")
                .replace("۳", "3")
                .replace("۴", "4")
                .replace("۵", "5")
                .replace("۶", "6")
                .replace("۷", "7")
                .replace("۸", "8")
                .replace("۹", "9");
    }
}