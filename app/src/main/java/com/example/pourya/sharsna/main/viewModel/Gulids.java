package com.example.pourya.sharsna.main.viewModel;

public class Gulids {

    private int Id;
    private String GuildName;
    private String ParentId;
    private String Icon;
    private String RowVersion;
    private String GetServer;

    public Gulids(){}

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getGuildName() {
        return GuildName;
    }

    public void setGuildName(String guildName) {
        GuildName = guildName;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String parentId) {
        ParentId = parentId;
    }

    public String getIcon() { return Icon; }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String rowVersion) {
        RowVersion = rowVersion;
    }

    public String isGetServer() {
        return GetServer;
    }

    public void setGetServer(String getServer) {
        GetServer = getServer;
    }
}