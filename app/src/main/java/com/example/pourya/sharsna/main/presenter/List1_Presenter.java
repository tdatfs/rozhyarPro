package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_List1;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.lang.ref.WeakReference;
import java.util.List;

public class List1_Presenter implements MVP_List1.ProvidedPresenterOps, MVP_List1.RequiredPresenterOps{

    private WeakReference<MVP_List1.RequiredViewOps> mView;
    private MVP_List1.ProvidedModelOps mModel;

    public List1_Presenter(MVP_List1.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_List1.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_List1.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Gulids> List_Gulids() {
        return mModel.List_Gulids();
    }

    @Override
    public String MaxRowV_Gulids() {
        return mModel.MaxRowV_Gulids();
    }

    @Override
    public void Insert_Gulids(String Q) {
        mModel.Insert_Gulids(Q);
    }

    @Override
    public List<Integer> Id_Gulids() {
        return mModel.Id_Gulids();
    }

    @Override
    public int Count_Gulids() {
        return mModel.Count_Gulids();
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    public void setModel(MVP_List1.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}