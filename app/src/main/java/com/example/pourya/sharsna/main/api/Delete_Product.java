package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.login.Login2;
import com.example.pourya.sharsna.main.view.profile_login2.Tabs_main2;

public class Delete_Product extends BaseActivity {

    Activity _activity;
    int Pid;
    ProgressDialog progressDialog;

    public Delete_Product(Activity activity, int id) {
        _activity = activity;
        Pid = id;
        progressDialog = new ProgressDialog(activity);
        post();
    }

    public Delete_Product post(){

        progressDialog.setMessage("لطفا صبر کنید ...");
        progressDialog.show();

        StringRequest sr = new StringRequest(Request.Method.DELETE, url+ "ApiProduct/Delete?id="+Pid,
        new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(response.equals("")){
                        progressDialog.dismiss();
                        Toast.makeText(_activity, "محصول حذف شد",Toast.LENGTH_LONG).show();
                        _activity.startActivity(new Intent(_activity, Tabs_main2.class));
                        _activity.finish();
                    }
                }catch (Exception e){
                    new PostError(_activity, e.getMessage(), Utility.getMethodName()).postError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                new ErrorVolley(_activity).Error(volleyError,"get");
                if (volleyError.networkResponse == null) {
                    if (volleyError.getClass().equals(TimeoutError.class)) {}
                }
            }
        });
        SampleApp.getInstance().addToRequestQueue(sr);
        return null;
    }
}