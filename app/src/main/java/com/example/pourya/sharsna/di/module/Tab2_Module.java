package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Tab2;
import com.example.pourya.sharsna.main.model.Tab2_Model;
import com.example.pourya.sharsna.main.presenter.Tab2_Presenter;
import com.example.pourya.sharsna.main.view.profile_login2.Tabs_main2;
import dagger.Module;
import dagger.Provides;

@Module
public class Tab2_Module {

    private Tabs_main2 activity;

    public Tab2_Module(Tabs_main2 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Tabs_main2 providesTab2Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Tab2.ProvidedPresenterOps providedPresenterOps() {
        Tab2_Presenter presenter = new Tab2_Presenter( activity );
        Tab2_Model model = new Tab2_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}