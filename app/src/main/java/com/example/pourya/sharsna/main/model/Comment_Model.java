package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_Comment;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Scores;
import java.util.ArrayList;
import java.util.List;

public class Comment_Model implements MVP_Comment.ProvidedModelOps {

    private MVP_Comment.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Integer> id_comments;
    private List<Scores> SList;
    private List<Scores> My;

    public Comment_Model(MVP_Comment.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        id_comments = new ArrayList<>();
        SList = new ArrayList<>();
        My = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public List<Integer> Id_Comments() {
        try{
            String q = "SELECT [Id] FROM [Scores]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_comments.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return id_comments;
    }

    @Override
    public void Insert_Comments(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public String MaxRowV_Comments() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Scores";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public List<Scores> List_Comments(int uid) {
        try{
            String q = "SELECT [Id],[UserId],[MemberId],[SecondUserId],[MemberFullName],[User1NameShop],[Point],[Comment],[RowVersion] FROM [Scores] where [UserId] = "+ uid;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Scores app = new Scores();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setUserId(Integer.parseInt(cursor.getString(1)));
                app.setMemberId(Integer.parseInt(cursor.getString(2)));
                app.setSecondUserId(Integer.parseInt(cursor.getString(3)));
                app.setMemberFullName(cursor.getString(4));
                app.setUser1NameShop(cursor.getString(5));
                app.setPoint(cursor.getString(6));
                app.setComment(cursor.getString(7));
                app.setRowVersion(cursor.getString(8));
                SList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return SList;
    }

    @Override
    public Scores My_Comment_M(int mid) {
        int have = 0;
        try{
            String q = "SELECT [Id],[UserId],[MemberId],[MemberFullName],[Point],[Comment],[RowVersion] FROM [Scores] where [MemberId] = "+ mid;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Scores app = new Scores();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setUserId(Integer.parseInt(cursor.getString(1)));
                app.setMemberId(Integer.parseInt(cursor.getString(2)));
                app.setMemberFullName(cursor.getString(3));
                app.setPoint(cursor.getString(4));
                app.setComment(cursor.getString(5));
                app.setRowVersion(cursor.getString(6));
                My.add(app);
                cursor.moveToNext();
                have = 1;
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        if(have == 1){
            return My.get(0);
        }else{
            return null;
        }
    }

    @Override
    public Scores My_Comment_U(int uid) {
        int have = 0;
        try{
            String q = "SELECT [Id],[UserId],[SecondUserId],[MemberFullName],[Point],[Comment],[RowVersion] FROM [Scores] where [SecondUserId] = "+ uid;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Scores app = new Scores();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setUserId(Integer.parseInt(cursor.getString(1)));
                app.setSecondUserId(Integer.parseInt(cursor.getString(2)));
                app.setMemberFullName(cursor.getString(3));
                app.setPoint(cursor.getString(4));
                app.setComment(cursor.getString(5));
                app.setRowVersion(cursor.getString(6));
                My.add(app);
                cursor.moveToNext();
                have = 1;
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        if(have == 1){
            return My.get(0);
        }else{
            return null;
        }
    }
}