package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Product_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.tabs.Product;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Product_Module.class )
public interface Product_Component {
    Product inject(Product activity);
}