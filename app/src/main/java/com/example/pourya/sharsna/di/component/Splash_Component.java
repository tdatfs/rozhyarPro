package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Splash_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.splash.Splash;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Splash_Module.class )
public interface Splash_Component {
    Splash inject(Splash activity);
}