package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Login;
import com.example.pourya.sharsna.main.model.Login_Model;
import com.example.pourya.sharsna.main.presenter.Login_Presenter;
import com.example.pourya.sharsna.main.view.login.Login;
import dagger.Module;
import dagger.Provides;

@Module
public class Login_Module {

    private Login activity;

    public Login_Module(Login activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Login providesLoginActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Login.ProvidedPresenterOps providedPresenterOps() {
        Login_Presenter presenter = new Login_Presenter( activity );
        Login_Model model = new Login_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}