package com.example.pourya.sharsna.main.view.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Login2_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Login2;
import com.example.pourya.sharsna.main.api.Cheak_Login2;
import com.example.pourya.sharsna.main.presenter.Login2_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.profile_login2.Tabs_main2;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import javax.inject.Inject;

public class Login2 extends BaseActivity implements MVP_Login2.RequiredViewOps, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = Login2.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), Login2.class.getName());

    @Inject
    public MVP_Login2.ProvidedPresenterOps mPresenter;

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    LinearLayout navigation_icon;
    SpaceNavigationView spaceNavigationView;
    MaterialSpinner spinner;
    TextView other_login;
    EditText mobile, password;
    LinearLayout btn_login;
    public static SharedPreferences sp2;
    String username_shp, password_shp, uid_shp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login2);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(3);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Login2.this.finish();
                Intent intent = new Intent(Login2.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        Login2.this.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent = new Intent(Login2.this, Main.class);
                        startActivity(intent);
                        break;

                    case 1:
                        Login2.this.finish();
                        Intent intentmenu = new Intent(Login2.this, List1.class);
                        startActivity(intentmenu);
                        break;

                    case 2:
                        Login2.this.finish();
                        Intent intentshop = new Intent(Login2.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        // nothing
                        break;
                }
            }
            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();

        sp2 = getApplicationContext().getSharedPreferences("DB2",0);

        username_shp = sp2.getString("user2","");
        password_shp = sp2.getString("pass2","");
        uid_shp = sp2.getString("uid2","");

        // no login
        if(uid_shp.equals("")){
            if(username_shp.equals("") && password_shp.equals("")){
                // nothing
            }else{
                // show it and login
                mobile.setText(username_shp);
                password.setText(password_shp);
                String user = faToEn(mobile.getText().toString());
                String pass = faToEn(password.getText().toString());
                if(user.equals("") || pass.equals("")){
                    Toast.makeText(getApplicationContext(), "شماره موبایل و رمز عبور خود را وارد کنید", Toast.LENGTH_LONG).show();
                }else{
                    new Cheak_Login2(haveNetworkConnection(), getApplicationContext(), this, user, pass, true);
                }
            }
        }
        // is login
        else{
            Tabs_main2.IdUser = Integer.parseInt(uid_shp);
            this.finish();
            startActivity(new Intent(this, Tabs_main2.class));
        }

        setupMVP();
        afterSetup();
    }

    public void setupViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        other_login = (TextView) findViewById(R.id.other_login);
        password =(EditText) findViewById(R.id.password);
        mobile =(EditText) findViewById(R.id.mobile);
        btn_login = (LinearLayout) findViewById(R.id.btn_login);
    }

    private void afterSetup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        navigation_icon.setOnClickListener(this);
        other_login.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.navigation_icon:
                drawerLayout.openDrawer(GravityCompat.END);
                break;

            case R.id.other_login :
                Login2.this.finish();
                startActivity(new Intent(Login2.this, Login1.class));
                break;

            case R.id.btn_login:
                //AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
                //v.startAnimation(buttonClick);
                String user = faToEn(mobile.getText().toString());
                String pass = faToEn(password.getText().toString());
                if(user.equals("") || pass.equals("")){
                    Toast.makeText(getApplicationContext(), "شماره موبایل و رمز عبور خود را وارد کنید", Toast.LENGTH_LONG).show();
                }else{
                    new Cheak_Login2(haveNetworkConnection(), getApplicationContext(), this, user, pass, true);
                }
                break;
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Login2.this.finish();
                startActivity(new Intent(Login2.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Login2.this.finish();
                startActivity(new Intent( Login2.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Login2_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Login2_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getLogin2Component(new Login2_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(3);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        Login2.this.finish();
        startActivity(new Intent(this, Main.class));
    }
}