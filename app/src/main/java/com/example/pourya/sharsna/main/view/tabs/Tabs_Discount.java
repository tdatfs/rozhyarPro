package com.example.pourya.sharsna.main.view.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Get_Discount;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.login.Login2;

public class Tabs_Discount extends Fragment {

    public static TextView no_product;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tabs_discount, container, false);
        no_product = (TextView) view.findViewById(R.id.no_product);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_discount);
        Login1.sp1 = getContext().getSharedPreferences("DB1",0);
        String member_id = Login1.sp1.getString("uid1","");
        Login2.sp2 = getContext().getSharedPreferences("DB2",0);
        String user_id = Login2.sp2.getString("uid2","");
        new Get_Discount(getContext(), getActivity(), recyclerView, member_id, Tabs_Main.city_id, user_id).get();
        return view;
    }
}