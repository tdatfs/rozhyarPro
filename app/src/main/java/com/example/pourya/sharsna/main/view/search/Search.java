package com.example.pourya.sharsna.main.view.search;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Search_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Search;
import com.example.pourya.sharsna.main.api.Get_Search;
import com.example.pourya.sharsna.main.location.GPSTracker;
import com.example.pourya.sharsna.main.permission.PermissionHandler;
import com.example.pourya.sharsna.main.permission.Permissions;
import com.example.pourya.sharsna.main.presenter.Search_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class Search extends BaseActivity implements MVP_Search.RequiredViewOps, View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Search.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer(getFragmentManager(), Search.class.getName());

    @Inject
    public MVP_Search.ProvidedPresenterOps mPresenter;

    public static TextView result;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    RecyclerView recyclerView;
    LinearLayout navigation_icon;
    MaterialSpinner spinner;
    List<Cities> list_City = new ArrayList<>();
    SpaceNavigationView spaceNavigationView;
    EditText edt;
    int city_id;
    double latitude, longitude;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(-1);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                // location
                Permissions.check(getApplicationContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        "location", new Permissions.Options().setRationaleDialogTitle("Info"),

                        new PermissionHandler() {
                            @Override
                            public void onGranted() {

                                GPSTracker tracker = new GPSTracker(Search.this);
                                latitude = tracker.getLatitude();
                                longitude = tracker.getLongitude();

                                String text_search = edt.getText().toString();
                                if(text_search.equals("")){
                                    Toast.makeText(getApplicationContext(), "متن جستجو را وارد کنید", Toast.LENGTH_LONG).show();
                                }else{
                                    String mid = get_member_id();
                                    String uid = get_user_id();
                                    new Get_Search(haveNetworkConnection(), mPresenter, getAppContext(), Search.this, latitude+"", longitude+"", text_search, city_id, uid, mid);
                                }
                            }

                            @Override
                            public void onDenied(Context context, ArrayList<String> deniedPermissions) {}

                            @Override
                            public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                                return false;
                            }

                            @Override
                            public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {}
                        });
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        Search.this.finish();
                        Intent intent = new Intent(Search.this, Main.class);
                        startActivity(intent);
                        break;

                    case 1:
                        Search.this.finish();
                        Intent intentmenu = new Intent(Search.this, List1.class);
                        startActivity(intentmenu);
                        break;

                    case 2:
                        Search.this.finish();
                        Intent intentshop = new Intent(Search.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();
        after_setup();

        // location
        Permissions.check(getApplicationContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                "location", new Permissions.Options().setRationaleDialogTitle("Info"),

                new PermissionHandler() {
                    @Override
                    public void onGranted() {}

                    @Override
                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {}

                    @Override
                    public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                        return false;
                    }

                    @Override
                    public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {}
                });

        // GPS
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("متاسفانه GPS شما خاموش است"+"\n\n" + "لطفا GPS گوشی خود را روشن کنید")
                    .setCancelable(false)
                    .setPositiveButton("  OK  ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.setTitle(" ");
            alert.setIcon(R.drawable.ic_place_black_24dp);
            alert.show();
        }
    }

    private void setupViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        recyclerView = (RecyclerView) findViewById(R.id.rv_list3);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        edt = (EditText) findViewById(R.id.et_search_box);
        result = (TextView) findViewById(R.id.result);
    }

    private void after_setup() {

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        List<String> city_name = new ArrayList<>();
        int id_selected = 0;
        for (int i = 0; i < list_City.size(); i++) {
            city_name.add(list_City.get(i).getTitle());
            if(list_City.get(i).getSelected().equals("1")){
                id_selected = i;
                city_id = list_City.get(i).getId();
            }
        }
        spinner.setItems(city_name);
        spinner.setSelectedIndex(id_selected);
        // set text size
        spinner.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
        spinner.setTextDirection(View.TEXT_DIRECTION_RTL);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                city_id = list_City.get(position).getId();
            }
        });

        navigation_icon.setOnClickListener(this);
        edt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_RIGHT = 2;
                if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if(motionEvent.getRawX() >= (edt.getRight() - edt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        // location
                        Permissions.check(getApplicationContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                "location", new Permissions.Options().setRationaleDialogTitle("Info"),

                                new PermissionHandler() {
                                    @Override
                                    public void onGranted() {

                                        GPSTracker tracker=new GPSTracker(Search.this);
                                        latitude = tracker.getLatitude();
                                        longitude = tracker.getLongitude();

                                        String text_search = edt.getText().toString();
                                        if(text_search.equals("")){
                                            Toast.makeText(getApplicationContext(), "متن جستجو را وارد کنید", Toast.LENGTH_LONG).show();
                                        }else{
                                            String mid = get_member_id();
                                            String uid = get_user_id();
                                            new Get_Search(haveNetworkConnection(), mPresenter, getAppContext(), Search.this, latitude+"", longitude+"", text_search, city_id, uid, mid);
                                        }
                                    }

                                    @Override
                                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {}

                                    @Override
                                    public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                                        return false;
                                    }

                                    @Override
                                    public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {}
                                });

                        return true;
                    }
                }
                return false;
            }
        });

        edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    edt.clearFocus();
                    InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(edt.getWindowToken(), 0);

                    GPSTracker tracker = new GPSTracker(Search.this);
                    latitude = tracker.getLatitude();
                    longitude = tracker.getLongitude();
                    String text_search = edt.getText().toString();
                    if(text_search.equals("")){
                        Toast.makeText(getApplicationContext(), "متن جستجو را وارد کنید", Toast.LENGTH_LONG).show();
                    }else{
                        String mid = get_member_id();
                        String uid = get_user_id();
                        new Get_Search(haveNetworkConnection(), mPresenter, getAppContext(), Search.this, latitude+"", longitude+"", text_search, city_id, uid, mid);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Search.this.finish();
                startActivity(new Intent(Search.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Search.this.finish();
                startActivity(new Intent( Search.this, Main.class));
               break;
        }
        return true;
    }

    private void setupMVP() {
        if (mStateMaintainer.firstTimeIn()) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize() {
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Search_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Search_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if (mPresenter == null)
            setupComponent();
    }

    private void setupComponent() {
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getSearchComponent(new Search_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back() {
        Search.this.finish();
        startActivity(new Intent(this, Main.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(-1);
    }
}