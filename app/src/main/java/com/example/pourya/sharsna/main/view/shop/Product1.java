package com.example.pourya.sharsna.main.view.shop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Product1_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Product1;
import com.example.pourya.sharsna.main.api.Post_Like1;
import com.example.pourya.sharsna.main.api.Post_UnLike1;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.presenter.Product1_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Login1;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Products;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class Product1 extends BaseActivity implements MVP_Product1.RequiredViewOps, View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Product1.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), Product1.class.getName());

    @Inject
    public MVP_Product1.ProvidedPresenterOps mPresenter;

    Toolbar toolbar;
    LinearLayout navigation_icon;
    DrawerLayout drawerLayout;
    MaterialSpinner spinner;
    List<Cities> list_City = new ArrayList<>();
    SpaceNavigationView spaceNavigationView;
    public static TextView namep, pricep,offp,datep,desp,likp;
    public static int Pid;
    public static ImageView img_like;
    public static ImageView img_add;
    public static int like_back;
    Products p;
    Button go_shop;
    public static Activity shop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product1);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(-1);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Product1.this.finish();
                Intent intent = new Intent(Product1.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        Product1.this.finish();
                        shop.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent1 = new Intent(Product1.this, Main.class);
                        startActivity(intent1);
                        break;

                    case 1:
                        Product1.this.finish();
                        Intent intent2 = new Intent(Product1.this, List1.class);
                        startActivity(intent2);
                        break;

                    case 2:
                        Product1.this.finish();
                        Intent intentshop = new Intent(Product1.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();
        p = mPresenter.Get_Products(Pid);

        after_setup();
    }

    private void setupViews(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        namep=(TextView)findViewById(R.id.nameProduct);
        pricep=(TextView)findViewById(R.id.priceProduct);
        offp=(TextView)findViewById((R.id.discountProduct));
        datep=(TextView)findViewById(R.id.dateProduct);
        desp=(TextView)findViewById(R.id.description);
        likp=(TextView)findViewById(R.id.like_count);
        img_add=(ImageView) findViewById(R.id.img_add);
        img_like=(ImageView) findViewById((R.id.img_like));
        go_shop = (Button) findViewById(R.id.go_shop);
    }

    private void after_setup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        navigation_icon.setOnClickListener(this);

        namep.setText(p.getNameProduct());
        pricep.setText(p.getPrice());
        // off
        if(p.getOffProduct().equals("") || p.getOffProduct().equals("null")){
            offp.setText("");
        }else{
            offp.setText(p.getOffProduct());
        }
        // date
        if(p.getDateExpireshamsi().toString().equals("") || p.getDateExpireshamsi().toString().equals("null")){
            datep.setText("");
        }else{
            datep.setText(p.getDateExpireshamsi().toString());
        }
        // description
        if(p.getDes().equals("") || p.getDes().equals("null")){
            desp.setText("");
        }else{
            desp.setText(p.getDes());
        }
        // like_count
        if(p.getCountLike().equals("") || p.getCountLike().equals("null")){
            likp.setText("0");
        }else{
            likp.setText(p.getCountLike());
        }
        // like_image
        if(p.getIsLike().equals("true")){
            like_back = 1;
            img_like.setBackgroundResource(R.drawable.ic_like);
        }else if(p.getIsLike().equals("false")){
            like_back = 0;
            img_like.setBackgroundResource(R.drawable.ic_like_2);
        }

        try{
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.place_holder);
            requestOptions.error(R.drawable.e);
            String url = url_download + p.getPicProduct();
            Glide.with(getApplicationContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load(url)
                    .into(img_add);
        }catch (Exception e){
            new PostError(getApplicationContext(), e.getMessage(), Utility.getMethodName()).postError();
        }

        // like with 2 thing
        img_like.setOnClickListener(this);
        img_add.setOnClickListener(this);

        go_shop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
        if (v.getId() == R.id.go_shop) {
            Product1.this.finish();
        }
        if (v.getId() == R.id.img_like || v.getId() == R.id.img_add) {

            if(like_back == 0){

                String user_id = get_user_id();
                String member_id = get_member_id();
                // go login page
                if (member_id.equals("") && user_id.equals("")) {
                    Dialog_Login1 cdd = new Dialog_Login1(this);
                    cdd.show();
                }

                // do follow
                else{
                    try {
                        int cl = Integer.parseInt(likp.getText().toString());
                        new Post_Like1(haveNetworkConnection(), mPresenter, getApplicationContext(), this, user_id, member_id, Pid, cl).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if(like_back == 1){

                String user_id = get_user_id();
                String member_id = get_member_id();
                // go login page
                if (member_id.equals("") && user_id.equals("")) {
                    Dialog_Login1 cdd = new Dialog_Login1(this);
                    cdd.show();
                }

                // do unfollow
                else{
                    try {
                        int cl = Integer.parseInt(likp.getText().toString());
                        new Post_UnLike1(haveNetworkConnection(), mPresenter, getApplicationContext(), this, user_id, member_id, Pid, cl).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Product1.this.finish();
                startActivity(new Intent(Product1.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Product1.this.finish();
                startActivity(new Intent( Product1.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Product1_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Product1_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getProduct1Component(new Product1_Module(this))
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(-1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        Product1.this.finish();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}