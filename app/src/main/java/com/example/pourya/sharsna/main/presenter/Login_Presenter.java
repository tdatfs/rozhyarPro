package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Login;
import java.lang.ref.WeakReference;

public class Login_Presenter implements MVP_Login.ProvidedPresenterOps, MVP_Login.RequiredPresenterOps {

    private WeakReference<MVP_Login.RequiredViewOps> mView;
    private MVP_Login.ProvidedModelOps mModel;

    public Login_Presenter(MVP_Login.RequiredViewOps view)
    {
        mView = new WeakReference<>(view);
    }

    private MVP_Login.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Login.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(MVP_Login.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}