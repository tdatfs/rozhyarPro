package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_Splash;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Retry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class Get_SelectedShop extends BaseActivity {

    MVP_Splash.ProvidedPresenterOps splash_presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;
    boolean insert;
    int cid;

    public Get_SelectedShop(boolean net, MVP_Splash.ProvidedPresenterOps ppo, Context context, Activity activity, int c) {
        splash_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        cid = c;
        get();
    }

    public Get_SelectedShop get() {

        splash_presenter.Delete_SelectedShop();
        insert = false;

        if(mnet){
            int count = splash_presenter.Count_SelectedShop();

            if(count == 0){

                JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                        url+ "ApiShop/Shop?cityid="+cid, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            String Q1 = "insert into SelectedShops (UserId,UserNameShop,UserPicProfile,CityId,GetServer) values ";
                            for (int i=0; i<response.length(); i++) {
                                insert = true;
                                JSONObject jsonObject = response.getJSONObject(i);
                                String id = jsonObject.getString("UserId");
                                String name = jsonObject.getString("UserNameShop");
                                String pic = jsonObject.getString("UserPicProfile");
                                String status = "false";

                                int Id = Integer.parseInt(id);

                                Q1 = Q1.concat("('" + Id + "','" + name + "','" + pic + "','" + cid + "','" + status + "')," );
                            }
                            if(insert) {
                                Q1 = Q1.substring(0, Q1.trim().length() - 1).concat(";");
                                splash_presenter.Insert_SelectedShop(Q1);
                            }

                            // get slider
                            new Get_Slider(mnet, splash_presenter, _context, mactivity);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                            Toast.makeText(_context, "خطایی پیش امده است مجددا تلاش کنید.", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        new ErrorVolley(_context).Error(volleyError,"get");
                        if (volleyError.networkResponse == null) {
                            if (volleyError.getClass().equals(TimeoutError.class)) {
                                Dialog_Retry dr = new Dialog_Retry(mactivity);
                                dr.show();
                            }
                        }
                    }
                });
                SampleApp.getInstance().addToRequestQueue(jsonObjReq);
            }

        }else{}
        return null;
    }

    public int whatdo(List<Integer> listFunction, int id){
        int result = 1;
        for(int j=0 ; j < listFunction.size() ; j++) {
            if(listFunction.get(j) == id){
                result = 2;
            }
        }
        return result;
    }
}