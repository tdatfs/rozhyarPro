package com.example.pourya.sharsna.main.view.profile_login1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.shop.Shop;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.util.List;

public class Adapter_friend extends RecyclerView.Adapter<Adapter_friend.ViewHolder> {

    Context context;
    Activity activity;
    List<Users> list;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_friend(Context c, Activity a, List<Users> l) {
        context = c;
        activity = a;
        list = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, address, dis, abc;
        ImageView image;
        RatingBar rating;
        RelativeLayout main_lst3;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.txt_name_adp);
            address = (TextView) itemView.findViewById(R.id.txt_address_adp);
            image = (ImageView) itemView.findViewById(R.id.img_main_adp);
            rating = (RatingBar) itemView.findViewById(R.id.ratingbar_adp);
            main_lst3 = (RelativeLayout) itemView.findViewById(R.id.main_lst3);
            dis = (TextView) itemView.findViewById(R.id.dis);
            abc = (TextView) itemView.findViewById(R.id.abc);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_list3, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getNameShop().toString());
        holder.address.setText(list.get(position).getAddress().toString());
        float distance = list.get(position).getDistance();
        if(distance == 0){
            holder.dis.setVisibility(View.GONE);
            holder.abc.setVisibility(View.GONE);
        }else{
            holder.dis.setText(distance + "");
        }
        String r = list.get(position).getRating();
        if(r == null || r.equals("null")){
            holder.rating.setRating((float) 0);
        }else{
            holder.rating.setRating((float) Float.parseFloat(list.get(position).getRating()));
        }

        try{
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.place_holder);
            requestOptions.error(R.drawable.e);
            String url = url_download + list.get(position).getPicProfile();
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(url)
                    .into(holder.image);
        }catch (Exception e){
            new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
        }

        final int id = list.get(position).getId();
        holder.main_lst3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Shop.search_result = list.get(position);
                Shop.is_search = false;
                Shop.selected = false;
                Shop.bazar = false;
                Shop.profile = true;
                activity.finish();
                view.getContext().startActivity(new Intent(view.getContext(), Shop.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}