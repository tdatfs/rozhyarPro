package com.example.pourya.sharsna.main.view.shop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Shop_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Shop;
import com.example.pourya.sharsna.main.api.Get_Product;
import com.example.pourya.sharsna.main.api.Post_Follow;
import com.example.pourya.sharsna.main.api.Post_UnFollow;
import com.example.pourya.sharsna.main.cls.Set_Shop;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.presenter.Shop_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Login1;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.list3.List3;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.profile_login1.Tabs_main1;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Product;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Products;
import com.example.pourya.sharsna.main.viewModel.Users;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import de.hdodenhof.circleimageview.CircleImageView;

public class Shop extends BaseActivity implements MVP_Shop.RequiredViewOps, View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Shop.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), Shop.class.getName());

    @Inject
    public MVP_Shop.ProvidedPresenterOps mPresenter;

    public static int Uid;
    public static boolean selected, is_search, bazar, profile;
    //search
    public static Users search_result;
    Toolbar toolbar;
    LinearLayout navigation_icon, go_comment;
    DrawerLayout drawerLayout;
    RecyclerView recyclerView;
    Users one_user;
    MaterialSpinner spinner;
    List<Cities> list_City = new ArrayList<>();
    SpaceNavigationView spaceNavigationView;
    ImageView _instagram, _telegram, _web;
    ImageView img;
    String instagram_link, telegram_link, web_link;
    public static TextView product, no_product;
    TextView name, follower, operator, phone, gulid, address, comment, a, b;
    RatingBar rate;
    public static int background_follower;
    public static TextView f;
    public static ImageView backgrond;
    List<Products> list_product = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(-1);
        spaceNavigationView.bringToFront();
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Shop.this.finish();
                Intent intent = new Intent(Shop.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        Shop.this.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent1 = new Intent(Shop.this, Main.class);
                        startActivity(intent1);
                        break;

                    case 1:
                        Shop.this.finish();
                        Intent intent2 = new Intent(Shop.this, List1.class);
                        startActivity(intent2);
                        break;

                    case 2:
                        Shop.this.finish();
                        Intent intentshop = new Intent(Shop.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();

        if(is_search || profile){
            // have search result
            one_user = search_result;
            Uid = one_user.getId();
        }else{
            one_user = mPresenter.Get_User(Uid);
        }

        String mid = get_member_id();
        String uid = get_user_id();
        list_product = mPresenter.List_Products(Uid);
        // get product
        if(list_product.size() == 0){
            new Get_Product(haveNetworkConnection(), mPresenter, getApplicationContext(), this, recyclerView, Uid, mid, uid);
        }
        // no repeate again
        else{
            Set_Shop set = new Set_Shop(mPresenter,getApplicationContext(),this,recyclerView,Uid);
            set.load();
        }

        after_setup();
    }

    private void setupViews(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        go_comment = (LinearLayout) findViewById(R.id.go_comment);
        recyclerView = (RecyclerView) findViewById(R.id.rv_profile_information);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        img = (CircleImageView) findViewById(R.id.img);
        _instagram = (ImageView) findViewById(R.id._instagram);
        _telegram = (ImageView) findViewById(R.id._telegram);
        _web = (ImageView) findViewById(R.id._web);
        backgrond = (ImageView) findViewById(R.id.backgrond);
        name = (TextView) findViewById(R.id.name);
        product = (TextView) findViewById(R.id.product);
        no_product = (TextView) findViewById(R.id.no_product);
        follower = (TextView) findViewById(R.id.follower);
        operator = (TextView) findViewById(R.id.operator);
        phone = (TextView) findViewById(R.id.phone);
        gulid = (TextView) findViewById(R.id.gulid);
        address = (TextView) findViewById(R.id.address);
        rate = (RatingBar) findViewById(R.id.rate);
        f = (TextView) findViewById(R.id.f);
        comment = (TextView) findViewById(R.id.comment);
        a = (TextView) findViewById(R.id.a);
        b = (TextView) findViewById(R.id.b);
    }

    private void after_setup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        try{
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.place_holder);
            requestOptions.error(R.drawable.e);
            String url = url_download + one_user.getPicProfile();
            Glide.with(getApplicationContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load(url)
                    .into(img);

        }catch (Exception e){
            new PostError(getApplicationContext(), e.getMessage(), Utility.getMethodName()).postError();
        }

        name.setText(one_user.getNameShop());
        operator.setText(one_user.getOperator());
        address.setText(one_user.getAddress());
        gulid.setText(one_user.getGuildName());

        if(one_user.getCellPhone().toString().equals("null")){
            // nothing
        }else{
            if(one_user.getPhone().toString().equals("") || one_user.getPhone().toString().equals("null")){
                // only cell phone
                phone.setText(one_user.getCellPhone());
            }else{
                // cell phone & phone
                phone.setText(one_user.getCellPhone() + "\n" + one_user.getPhone());
            }
        }

        if(one_user.getCountFollowe().toString().equals("null")){
            follower.setText(0+"");
        }else{
            follower.setText(one_user.getCountFollowe());
        }

        if(one_user.isFollow().equals("false")){
            f.setText("دنبال نشده");
            backgrond.setBackgroundResource(R.drawable.ic_button_unfollow);
            background_follower = 0;
        }else if(one_user.isFollow().equals("true")){
            f.setText("دنبال شده");
            backgrond.setBackgroundResource(R.drawable.ic_button_follow);
            background_follower = 1;
        }

        if(one_user.getRating().toString().equals("null")){
            rate.setRating((float) 0);
        }else{
            rate.setRating((float) Float.parseFloat(one_user.getRating()));
        }

        if(one_user.getInstagram().equals("") || one_user.getInstagram().equals("null")){
            _instagram.setVisibility(View.GONE);
        }else{
            _instagram.setVisibility(View.VISIBLE);
            instagram_link = one_user.getInstagram();
        }
        if(one_user.getTelegram().equals("") || one_user.getTelegram().equals("null")){
            _telegram.setVisibility(View.GONE);
        }else{
            _telegram.setVisibility(View.VISIBLE);
            telegram_link = one_user.getTelegram();
        }
        if(one_user.getWebSilte().equals("") || one_user.getWebSilte().equals("null")){
            _web.setVisibility(View.GONE);
        }else{
            _web.setVisibility(View.VISIBLE);
            web_link = one_user.getWebSilte();
        }
        if(one_user.isGeneral().equals("true")){
            // hide phone & operator
            a.setVisibility(View.GONE);
            b.setVisibility(View.GONE);
            phone.setVisibility(View.GONE);
            operator.setVisibility(View.GONE);
        }else{
            // show phone & operator
        }

        navigation_icon.setOnClickListener(this);
        backgrond.setOnClickListener(this);
        go_comment.setOnClickListener(this);
        _instagram.setOnClickListener(this);
        _telegram.setOnClickListener(this);
        _web.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
        if (v.getId() == R.id._instagram) {
            // go to instagram
            Intent instagram = new Intent(Intent.ACTION_VIEW , Uri.parse("https://www.instagram.com/tdateam/"));
            startActivity(instagram);
        }
        if (v.getId() == R.id._telegram) {
            // go to telegram
            Intent telegram = new Intent(Intent.ACTION_VIEW , Uri.parse("http://telegram.me/tdateam"));
            startActivity(telegram);
        }
        if (v.getId() == R.id._web) {
            // go to web page
            Intent web = new Intent(Intent.ACTION_VIEW , Uri.parse("http://tdaapp.ir/"));
            startActivity(web);
        }
        if (v.getId() == R.id.backgrond) {
            if(background_follower == 0){
                // shop
                int user_id = Uid;
                // mid
                String mid = get_member_id();
                //uid
                String uid = get_user_id();
                // go login page
                if (mid.equals("") && uid.equals("")) {
                    Dialog_Login1 cdd = new Dialog_Login1(this);
                    cdd.show();
                }
                // do follow
                else{
                    try {
                        int cf = Integer.parseInt(follower.getText().toString());
                        new Post_Follow(haveNetworkConnection(),mPresenter, getApplicationContext(), this, user_id, mid, uid, Uid, cf).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if(background_follower == 1){
                // shop
                int user_id = Uid;
                // mid
                String mid = get_member_id();
                //uid
                String uid = get_user_id();
                // go login page
                if (mid.equals("") && uid.equals("")) {
                    Dialog_Login1 cdd = new Dialog_Login1(this);
                    cdd.show();
                }
                // do unfollow
                else{
                    try {
                        int cf = Integer.parseInt(follower.getText().toString());
                        new Post_UnFollow(haveNetworkConnection(), mPresenter, getApplicationContext(), this, user_id, mid, uid, Uid, cf).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (v.getId() == R.id.go_comment) {
            CommentPage.Uid = Uid;
            String member_id = get_member_id();
            String user_id = get_user_id();
            CommentPage.Mid = member_id;
            CommentPage.SUid = user_id;
            CommentPage._name = one_user.getNameShop().toString();
            CommentPage._gulid = one_user.getGuildName().toString();
            CommentPage._img = one_user.getPicProfile().toString();
            startActivity(new Intent(this, CommentPage.class));
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Shop.this.finish();
                startActivity(new Intent(Shop.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Shop.this.finish();
                startActivity(new Intent( Shop.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Shop_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Shop_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getShopComponent(new Shop_Module(this))
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(-1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        int go = 0;
        Shop.this.finish();
        if(selected){
            go = 1;
            startActivity(new Intent(this, Main.class));
        }
        if(is_search){
            go = 1;
            startActivity(new Intent(this, List3.class));
        }
        if(bazar){
            go = 1;
            Tabs_Main.TabIndex = Product.TabIndex;
            startActivity(new Intent(this, Tabs_Main.class));
        }
        if(profile){
            go = 1;
            startActivity(new Intent(this, Tabs_main1.class));
        }
        if(go == 0){
            startActivity(new Intent(this, List3.class));
        }
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}