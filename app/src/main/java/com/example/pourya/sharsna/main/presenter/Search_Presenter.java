package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Search;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.lang.ref.WeakReference;
import java.util.List;

public class Search_Presenter implements MVP_Search.ProvidedPresenterOps, MVP_Search.RequiredPresenterOps{

    private WeakReference<MVP_Search.RequiredViewOps> mView;
    private MVP_Search.ProvidedModelOps mModel;

    public Search_Presenter(MVP_Search.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Search.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Search.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    public void setModel(MVP_Search.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}