package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.profile_login1.Adapter_follower;
import com.example.pourya.sharsna.main.viewModel.Users;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class Get_Followers extends BaseActivity {

    Context _context;
    Activity mactivity;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    int id;

    public Get_Followers(Context context, Activity activity, RecyclerView r, int i) {
        mactivity = activity;
        _context = context;
        progressDialog = new ProgressDialog(mactivity);
        recyclerView = r;
        id = i;
        get();
    }

    public Get_Followers get() {

        final List<Users> UList = new ArrayList<>();

        if(true){
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    url+ "ApiMember/Following?id="+id, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    try {
                        for (int i=0; i<response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);

                            Users app = new Users();
                            app.setId(Integer.parseInt(jsonObject.getString("Id")));
                            app.setGulidId(Integer.parseInt(jsonObject.getString("GulidId")));
                            app.setUserTypeId(Integer.parseInt(jsonObject.getString("UserTypeId")));
                            app.setGuildName(jsonObject.getString("GuildName"));
                            app.setNameShop(jsonObject.getString("NameShop"));
                            app.setAddress(jsonObject.getString("Address"));
                            app.setOperator(jsonObject.getString("Operator"));
                            app.setPhone(jsonObject.getString("Phone"));
                            app.setPicProfile(jsonObject.getString("PicProfile"));
                            app.setCellPhone(jsonObject.getString("CellPhone"));
                            app.setEmail(jsonObject.getString("Email"));
                            app.setWebSilte(jsonObject.getString("WebSilte"));
                            app.setTelegram(jsonObject.getString("Telegram"));
                            app.setInstagram(jsonObject.getString("Instagram"));
                            app.setCountView(jsonObject.getString("CountView"));
                            app.setCountFollowe(jsonObject.getString("CountFollowe"));
                            app.setRating(jsonObject.getString("Rating"));
                            app.setFollow(jsonObject.getString("IsFollow"));
                            app.setGeneral(jsonObject.getString("IsGeneral"));
                            app.setRowVersion("1");

                            UList.add(app);
                        }

                        // set data to recycler view
                        Adapter_follower adapter = new Adapter_follower(_context, mactivity, UList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(_context, LinearLayoutManager.VERTICAL,false));
                        recyclerView.setAdapter(adapter);

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        }else{}

        return null;
    }
}