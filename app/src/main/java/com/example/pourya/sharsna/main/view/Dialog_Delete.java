package com.example.pourya.sharsna.main.view;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Delete_Product;

public class Dialog_Delete extends Dialog implements View.OnClickListener {

    Activity c;
    Button btn_ok,btn_cancle;
    int Pid;

    public Dialog_Delete(Activity a, int i) {
        super(a);
        this.c = a;
        Pid = i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.delete);

        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_cancle = (Button) findViewById(R.id.btn_cancle);
        btn_ok.setOnClickListener(this);
        btn_cancle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_ok:
                dismiss();
                new Delete_Product(c, Pid);
                break;

            case R.id.btn_cancle:
                dismiss();
                break;
        }
    }
}