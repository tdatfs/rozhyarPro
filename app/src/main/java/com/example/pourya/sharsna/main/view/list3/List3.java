package com.example.pourya.sharsna.main.view.list3;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.List3_Module;
import com.example.pourya.sharsna.main.Interface.MVP_List3;
import com.example.pourya.sharsna.main.api.Get_List3;
import com.example.pourya.sharsna.main.cls.Set_List3;
import com.example.pourya.sharsna.main.presenter.List3_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.list2.List2;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Users;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class List3 extends BaseActivity implements MVP_List3.RequiredViewOps, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = List3.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), List3.class.getName());

    @Inject
    public MVP_List3.ProvidedPresenterOps mPresenter;

    public static int Gid;
    //search
    public static boolean is_search;
    public static List<Users> search_result;
    public static Toolbar toolbar;
    int city_id = 31;
    DrawerLayout drawerLayout;
    RecyclerView recyclerView;
    LinearLayout navigation_icon;
    MaterialSpinner spinner;
    List<Cities> list_City = new ArrayList<>();
    SpaceNavigationView spaceNavigationView;
    public static TextView no_gulid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list3);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(-1);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                List3.this.finish();
                Intent intent = new Intent(List3.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        List3.this.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent = new Intent(List3.this, Main.class);
                        startActivity(intent);
                        break;

                    case 1:
                        List3.this.finish();
                        Intent intentmenu = new Intent(List3.this, List1.class);
                        startActivity(intentmenu);
                        break;

                    case 2:
                        List3.this.finish();
                        Intent intentshop = new Intent(List3.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();
        if(is_search){
           // have search result
       }else{

            String member_id = get_member_id();
            String uid = get_user_id();

            String result = mPresenter.Status_Gulid(Gid);
            if(result.equals("false")){
                new Get_List3(haveNetworkConnection(), mPresenter, getAppContext(), List3.this, recyclerView, Gid, "", member_id, city_id, uid);
            }
            // no repeate again
            else{
                Set_List3 set1 = new Set_List3(mPresenter,getApplicationContext(),this,recyclerView,Gid,city_id);
                set1.load();
            }
       }

        after_setup();
    }

    private void setupViews(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        recyclerView = (RecyclerView) findViewById(R.id.rv_list3);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        no_gulid = (TextView) findViewById(R.id.no_gulid);
    }

    private void after_setup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        if(is_search){
            spinner.setVisibility(View.INVISIBLE);
        }else{
            List<String> city_name = new ArrayList<>();
            int id_selected = 0;
            for(int i=0 ; i<list_City.size() ; i++){
                city_name.add(list_City.get(i).getTitle());
                if(list_City.get(i).getSelected().equals("1")){
                    id_selected = i;
                    city_id = list_City.get(i).getId();
                }
            }
            spinner.setItems(city_name);
            spinner.setSelectedIndex(id_selected);
            // set text size
            spinner.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
            spinner.setTextDirection(View.TEXT_DIRECTION_RTL);
            spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    city_id = list_City.get(position).getId();

                    String member_id = get_member_id();
                    String uid = get_user_id();
                    new Get_List3(haveNetworkConnection(), mPresenter, getAppContext(), List3.this, recyclerView, Gid, "", member_id, city_id, uid);
                }
            });
        }

        if(is_search){
            boolean off_gps;
            // GPS
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                off_gps = false;
            }else{
                off_gps = true;
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(" اطلاعات GPS در دسترس نیست"+"\n\n" + "مسافت نامشخص است")
                        .setCancelable(false)
                        .setPositiveButton("  متوجه شدم  ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {dialog.cancel();}
                        });
                AlertDialog alert = builder.create();
                alert.setTitle(" ");
                alert.setIcon(R.drawable.ic_place_black_24dp);
                alert.show();
            }
            Adapter_List3 adapter = new Adapter_List3(getApplicationContext(), this, search_result, true, off_gps);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(adapter);
        }

        navigation_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                List3.this.finish();
                startActivity(new Intent(List3.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                List3.this.finish();
                startActivity(new Intent( List3.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(List3_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(List3_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getList3Component(new List3_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(-1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        List3.this.finish();
        if(is_search){
            startActivity(new Intent(this, Search.class));
        }else{
            startActivity(new Intent(this, List2.class));
        }
    }
}
