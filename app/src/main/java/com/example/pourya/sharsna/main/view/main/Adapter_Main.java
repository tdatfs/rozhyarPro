package com.example.pourya.sharsna.main.view.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.api.Get_OneUser;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.login.Login2;
import com.example.pourya.sharsna.main.view.shop.Shop;
import com.example.pourya.sharsna.main.viewModel.SelectedShops;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_Main extends RecyclerView.Adapter<Adapter_Main.ViewHolder> {

    MVP_Main.ProvidedPresenterOps main_presenter;
    Context context;
    Activity activity;
    List<SelectedShops> list;
    boolean net;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_Main(boolean n, MVP_Main.ProvidedPresenterOps ppo, Context c, Activity a, List<SelectedShops> l) {
        main_presenter = ppo;
        net = n;
        context = c;
        activity = a;
        list = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.rv_tv);
            imageView = (CircleImageView) itemView.findViewById(R.id.rv_img);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selectedshop, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(list.get(position).getUserNameShop().toString());

        try{
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.place_holder);
            requestOptions.error(R.drawable.e);
            String url = url_download + list.get(position).getUserPicProfile();
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(url)
                    .into(holder.imageView);
        }catch (Exception e){
            new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
        }

        final int uid = list.get(position).getUserId();
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Login1.sp1 = context.getSharedPreferences("DB1",0);
                String mid = Login1.sp1.getString("uid1","");
                Login2.sp2 = context.getSharedPreferences("DB2",0);
                String usid = Login2.sp2.getString("uid2","");

                int id = list.get(position).getUserId();
                String result = main_presenter.Status_Selected(id);
                if(result.equals("false")){
                    // get data
                    new Get_OneUser(net, main_presenter, context, activity, uid, mid, usid);
                }else{
                    // go shop
                    Shop.Uid = id;
                    Shop.is_search = false;
                    Shop.selected = true;
                    Shop.bazar = false;
                    Shop.profile = false;
                    Intent intent = new Intent(activity, Shop.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.finish();
                    activity.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}