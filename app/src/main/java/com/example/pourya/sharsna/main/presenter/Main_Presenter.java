package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Notifications;
import com.example.pourya.sharsna.main.viewModel.Sliders;
import com.example.pourya.sharsna.main.viewModel.SelectedShops;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.lang.ref.WeakReference;
import java.util.List;

public class Main_Presenter implements MVP_Main.ProvidedPresenterOps, MVP_Main.RequiredPresenterOps{

    private WeakReference<MVP_Main.RequiredViewOps> mView;
    private MVP_Main.ProvidedModelOps mModel;

    public Main_Presenter(MVP_Main.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Main.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Main.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    @Override
    public List<Sliders> getSlider(int cid) {
        return mModel.getSlider(cid);
    }

    @Override
    public List<SelectedShops> getSelectedShop(int cid) {
        return mModel.getSelectedShop(cid);
    }

    @Override
    public int Count_SelectedShop() {
        return mModel.Count_SelectedShop();
    }

    @Override
    public List<Integer> List_SelectedShop() {
        return mModel.List_SelectedShop();
    }

    @Override
    public void Delete_SelectedShop() {
        mModel.Delete_SelectedShop();
    }

    @Override
    public void Insert_SelectedShop(String Q) {
        mModel.Insert_SelectedShop(Q);
    }

    @Override
    public List<Notifications> getNotification() {
        return mModel.getNotification();
    }

    @Override
    public String MaxRowV_Users() {
        return mModel.MaxRowV_Users();
    }

    @Override
    public void Insert_Users(String Q) {
        mModel.Insert_Users(Q);
    }

    @Override
    public List<Integer> Id_Users() {
        return mModel.Id_Users();
    }

    @Override
    public List<Users> getShop() {
        return mModel.getShop();
    }

    @Override
    public String Status_Selected(int id) {
        return mModel.Status_Selected(id);
    }

    @Override
    public void Chanege_Status_Selected(int id) {
        mModel.Chanege_Status_Selected(id);
    }

    public void setModel(MVP_Main.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}