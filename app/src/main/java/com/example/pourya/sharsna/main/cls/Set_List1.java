package com.example.pourya.sharsna.main.cls;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pourya.sharsna.main.Interface.MVP_List1;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.Adapter_List1;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.util.List;

public class Set_List1 extends BaseActivity {

    MVP_List1.ProvidedPresenterOps list1_presenter;
    Context _context;
    Activity mactivity;
    RecyclerView mRecyclerView;
    List<Gulids> list_Gulid;

    public Set_List1(MVP_List1.ProvidedPresenterOps ppo, Context c, Activity a, RecyclerView r) {
        _context = c;
        mactivity = a;
        list1_presenter = ppo;
        mRecyclerView = r;
    }

    public void load() {
        list_Gulid = list1_presenter.List_Gulids();

        // 3 item null
        Gulids app = new Gulids();
        app.setGuildName("");
        list_Gulid.add(app); list_Gulid.add(app); list_Gulid.add(app);

        Adapter_List1 adapter = new Adapter_List1(_context, mactivity, list_Gulid);
        mRecyclerView.setLayoutManager(new GridLayoutManager(_context,3));
        mRecyclerView.setAdapter(adapter);
    }
}
