package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_List2;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.util.ArrayList;
import java.util.List;

public class List2_Model implements MVP_List2.ProvidedModelOps {

    private MVP_List2.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Gulids> GList;
    private List<Cities> CList;

    public List2_Model(MVP_List2.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        GList = new ArrayList<>();
        CList = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public List<Gulids> List_Gulids(int id) {
        try{
            String q = "SELECT [Id],[GuildName],[ParentId],[Icon],[RowVersion] FROM [Gulids] where [ParentId] = "+id;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Gulids app = new Gulids();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setGuildName(cursor.getString(1));
                app.setParentId(cursor.getString(2));
                app.setIcon(cursor.getString(3));
                app.setRowVersion(cursor.getString(4));
                GList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return GList;
    }

    @Override
    public List<Cities> getCity() {
        try{
            String q = "SELECT [Id],[Title] FROM [Cities]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Cities app = new Cities();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setTitle(cursor.getString(1));
                CList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return CList;
    }
}