package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Login_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.login.Login;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Login_Module.class )
public interface Login_Component {
    Login inject(Login activity);
}