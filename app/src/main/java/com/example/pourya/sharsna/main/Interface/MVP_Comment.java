package com.example.pourya.sharsna.main.Interface;

import android.content.Context;
import com.example.pourya.sharsna.main.viewModel.Scores;
import java.util.List;

public interface MVP_Comment {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_Comment.RequiredViewOps view);

        List<Integer> Id_Comments();
        void Insert_Comments(String Q);
        String MaxRowV_Comments();
        List<Scores> List_Comments(int uid);
        Scores My_Comment_M(int mid);
        Scores My_Comment_U(int uid);
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        List<Integer> Id_Comments();
        void Insert_Comments(String Q);
        String MaxRowV_Comments();
        List<Scores> List_Comments(int uid);
        Scores My_Comment_M(int mid);
        Scores My_Comment_U(int uid);
    }
}