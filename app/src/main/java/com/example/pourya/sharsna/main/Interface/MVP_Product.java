package com.example.pourya.sharsna.main.Interface;

import android.content.Context;

import java.util.List;

public interface MVP_Product {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_Product.RequiredViewOps view);

        // get user
        String MaxRowV_Users();
        void Insert_Users(String Q);
        List<Integer> Id_Users();

        void update_like(String like, int count, int id);
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // get user
        String MaxRowV_Users();
        void Insert_Users(String Q);
        List<Integer> Id_Users();

        void update_like(String like, int count, int id);
    }
}