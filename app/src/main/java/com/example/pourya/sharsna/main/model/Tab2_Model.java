package com.example.pourya.sharsna.main.model;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Tab2;
import com.example.pourya.sharsna.main.db.DBAdapter;
import java.util.ArrayList;

public class Tab2_Model implements MVP_Tab2.ProvidedModelOps {

    private MVP_Tab2.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;

    public Tab2_Model(MVP_Tab2.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }
}