package com.example.pourya.sharsna.main.view.profile_login1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Post_Friend;
import com.example.pourya.sharsna.main.view.BaseActivity;

import org.json.JSONException;

public class Friend extends BaseActivity {

    EditText edt;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_friend);

        edt = (EditText) findViewById(R.id.edt);
        btn = (Button) findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // code
                String code = edt.getText().toString();
                // mid
                String mid = get_member_id();
                if(code.equals("")){
                    Toast.makeText(getApplicationContext(), "کد را وارد کنید", Toast.LENGTH_LONG).show();
                }else{
                    try{
                        new Post_Friend(haveNetworkConnection(), getApplicationContext(), Friend.this, mid, code).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
