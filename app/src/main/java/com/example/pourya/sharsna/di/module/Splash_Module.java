package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Splash;
import com.example.pourya.sharsna.main.model.Splash_Model;
import com.example.pourya.sharsna.main.presenter.Splash_Presenter;
import com.example.pourya.sharsna.main.view.splash.Splash;
import dagger.Module;
import dagger.Provides;

@Module
public class Splash_Module {

    private Splash activity;

    public Splash_Module(Splash  activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Splash providesSplashActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Splash.ProvidedPresenterOps providedPresenterOps() {
        Splash_Presenter presenter = new Splash_Presenter( activity );
        Splash_Model model = new Splash_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
