package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Splash;
import java.lang.ref.WeakReference;
import java.util.List;

public class Splash_Presenter implements MVP_Splash.ProvidedPresenterOps, MVP_Splash.RequiredPresenterOps{

    private WeakReference<MVP_Splash.RequiredViewOps> mView;
    private MVP_Splash.ProvidedModelOps mModel;

    public Splash_Presenter(MVP_Splash.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Splash.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Splash.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public int Count_Slider() {
        return mModel.Count_Slider();
    }

    @Override
    public List<Integer> List_Slider() {
        return mModel.List_Slider();
    }

    @Override
    public String MaxRowV_Slider() {
        return mModel.MaxRowV_Slider();
    }

    @Override
    public void Insert_Slider(String Q) {
        mModel.Insert_Slider(Q);
    }

    @Override
    public void Delete_Slider() {
        mModel.Delete_Slider();
    }

    @Override
    public int Count_SelectedShop() {
        return mModel.Count_SelectedShop();
    }

    @Override
    public List<Integer> List_SelectedShop() {
        return mModel.List_SelectedShop();
    }

    @Override
    public void Delete_SelectedShop() {
        mModel.Delete_SelectedShop();
    }

    @Override
    public void Insert_SelectedShop(String Q) {
        mModel.Insert_SelectedShop(Q);
    }

    @Override
    public int Count_Notification() {
        return mModel.Count_Notification();
    }

    @Override
    public List<Integer> List_Notification() {
        return mModel.List_Notification();
    }

    @Override
    public String MaxRowV_Notification() {
        return mModel.MaxRowV_Notification();
    }

    @Override
    public void Insert_Notification(String Q) {
        mModel.Insert_Notification(Q);
    }

    @Override
    public void Delete_Notification() {
        mModel.Delete_Notification();
    }

    public void setModel(MVP_Splash.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}