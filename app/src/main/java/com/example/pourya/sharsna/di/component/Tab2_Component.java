package com.example.pourya.sharsna.di.component;


import com.example.pourya.sharsna.di.module.Tab2_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.profile_login2.Tabs_main2;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Tab2_Module.class )
public interface Tab2_Component {
    Tabs_main2 inject(Tabs_main2 activity);
}