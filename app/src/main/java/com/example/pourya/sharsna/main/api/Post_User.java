package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;

public class Post_User extends BaseActivity {

    Context _context;
    Activity _activity;
    boolean mnet;
    int vid, selected;
    String image, name, address, admin, tel, mobile, email, web, instagram, telegram, width, height, city, type;
    ProgressDialog progressDialog;

    public Post_User(boolean net, Context context, Activity activity,
                     int id, int select, String img,
                     String na, String adres, String ad, String t,
                     String m, String mail, String w, String insta,
                     String tele, String cit, String tp, String wi, String h) {

        progressDialog = new ProgressDialog(activity);
        mnet = net;
        _context = context;
        _activity = activity;
        vid = id;
        selected = select;
        name = na;
        address = adres;
        admin = ad;
        tel = t;
        mobile = m;
        email = mail;
        web = w;
        instagram = insta;
        telegram = tele;
        width = wi;
        height = h;
        image = img;
        city = cit;
        type = tp;
    }

    public void post() throws JSONException {
        progressDialog.setMessage("لطفا صبر کنید ...");
        progressDialog.show();

        JSONObject _jsonBody=new JSONObject();
        _jsonBody.put("GulidId", selected);
        _jsonBody.put("NameShop", name);
        _jsonBody.put("Address", address);
        _jsonBody.put("Phone", tel);
        _jsonBody.put("PicProfile", image);
        _jsonBody.put("CellPhone", mobile);
        _jsonBody.put("Email", email);
        _jsonBody.put("WebSilte", web);
        _jsonBody.put("Telegram", telegram);
        _jsonBody.put("Instagram", instagram);
        _jsonBody.put("CityId", city);
        _jsonBody.put("Lat", height);
        _jsonBody.put("Long", width);
        _jsonBody.put("MarketerId", vid);
        _jsonBody.put("UserTypeId", type);

        RequestQueue requestQueue = Volley.newRequestQueue(_context);
        final String requestBody = _jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,url+"ApiUser/NewUser",
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{
                    if(response.equals("200")){
                        //Tab_signup.wh = "";
                        progressDialog.dismiss();
                        Toast.makeText(_context,"اطلاعات با موفقیت ثبت شد",Toast.LENGTH_LONG).show();
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(_context,"خطا در ارسال اطلاعات به سرور",Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        new ErrorVolley(_context).Error(error, "post");
                    }
                })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}