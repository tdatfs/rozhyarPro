package com.example.pourya.sharsna.main.model;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_AddProduct;
import com.example.pourya.sharsna.main.db.DBAdapter;

public class AddProduct_Model implements MVP_AddProduct.ProvidedModelOps {

    private MVP_AddProduct.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;

    public AddProduct_Model(MVP_AddProduct.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }
}