package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_Comment;
import com.example.pourya.sharsna.main.cls.Set_Comments;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class Get_Comments extends BaseActivity {

    MVP_Comment.ProvidedPresenterOps presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    int id;

    public Get_Comments(boolean net, MVP_Comment.ProvidedPresenterOps ppo, Context context, Activity activity, RecyclerView r, int i) {
        presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        progressDialog = new ProgressDialog(mactivity);
        recyclerView = r;
        id = i;
        get();
    }

    public Get_Comments get() {
        if(mnet){
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    url+ "ApiScore/AllComment?id="+id, null, new Response.Listener<JSONArray>() {

            //?row="+list1_presenter.MaxRowV_Gulids()

                @Override
                public void onResponse(JSONArray response) {

                    boolean insert = false;
                    List<Integer> list_comments = presenter.Id_Comments();

                    try {
                        String Q1 = "insert into Scores (Id,UserId,MemberId,SecondUserId,MemberFullName,User1NameShop,Point,Comment,RowVersion) values ";
                        for (int i=0; i<response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            String id = jsonObject.getString("Id");
                            String uid = jsonObject.getString("UserId");
                            String mid = jsonObject.getString("MemberId");
                            String suid = jsonObject.getString("SecondUserId");
                            String mname = jsonObject.getString("MemberFullName");
                            String uname = jsonObject.getString("User1NameShop");
                            String point = jsonObject.getString("Point");
                            String comment = jsonObject.getString("Comment");
                            String row = jsonObject.getString("RowVersion");

                            int Id = Integer.parseInt(id);
                            int Uid = Integer.parseInt(uid);
                            int Mid;
                            if (mid.equals("") || mid.equals("null")) {
                                Mid = 0;
                            }else{
                                Mid = Integer.parseInt(mid);
                            }
                            int SUid;
                            if(suid.equals("") || suid.equals("null")){
                                SUid = 0;
                            }else{
                                SUid = Integer.parseInt(suid);
                            }

                            int type = whatdo(list_comments, Id);
                            // insert
                            if(type == 1){
                                insert = true;
                                Q1 = Q1.concat("('" + Id + "','" + Uid + "','" + Mid + "','" + SUid + "','" + mname + "','" + uname + "','" + point + "','" + comment + "','" + row + "')," );
                            }
                            // update
                            if(type == 2){
                                String Q2="update Scores set UserId='"+Uid+"',MemberId='"+Mid+"',SecondUserId='"+SUid+"',MemberFullName='"+mname+"',User1NameShop='"+uname+"',Point='"+point+"',Comment='"+comment+"',RowVersion='"+row+"' where Id="+Id;
                                presenter.Insert_Comments(Q2);
                            }
                        }
                        if(insert) {
                            Q1 = Q1.substring(0, Q1.trim().length() - 1).concat(";");
                            presenter.Insert_Comments(Q1);
                        }
                        progressDialog.dismiss();
                        Set_Comments set = new Set_Comments(presenter,_context,mactivity,recyclerView,id);
                        set.load();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {
                            Set_Comments set = new Set_Comments(presenter,_context,mactivity,recyclerView,id);
                            set.load();
                        }
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        }else{
            Set_Comments set = new Set_Comments(presenter,_context,mactivity,recyclerView,id);
            set.load();
        }
        return null;
    }

    public int whatdo(List<Integer> listFunction, int id){
        int result = 1;
        for(int j=0 ; j < listFunction.size() ; j++) {
            if(listFunction.get(j) == id){
                result = 2;
            }
        }
        return result;
    }
}