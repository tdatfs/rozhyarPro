package com.example.pourya.sharsna.main.view.shop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.util.List;

public class Adapter_Shop extends RecyclerView.Adapter<Adapter_Shop.ViewHolder> {

    Context context;
    Activity activity;
    List<Products> list;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_Shop(Context c, Activity a, List<Products> l) {
        context = c;
        activity = a;
        list = l;

        Shop.product.setText(list.size()+"");
        if(list.size() == 0){
            Shop.no_product.setVisibility(View.VISIBLE);
        }else{
            Shop.no_product.setVisibility(View.GONE);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img_saler);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_shop, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // not show
        if(list.get(position).getPicProduct().equals("")){
            holder.imageView.setVisibility(View.INVISIBLE);
        }
        // normal
        else {
            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list.get(position).getPicProduct();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(holder.imageView);
            }catch (Exception e){
                new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
            }
            final int id = list.get(position).getId();
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product1.Pid = id;
                    Product1.shop = activity;
                    view.getContext().startActivity(new Intent(view.getContext(), Product1.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}