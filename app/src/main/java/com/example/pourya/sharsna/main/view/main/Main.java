package com.example.pourya.sharsna.main.view.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Main_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.api.Get_SelectedShop2;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.presenter.Main_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Sliders;
import com.flurry.android.FlurryAgent;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import android.view.View.OnClickListener;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class Main extends BaseActivity implements MVP_Main.RequiredViewOps, OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Main.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer(getFragmentManager(), Main.class.getName());

    @Inject
    public MVP_Main.ProvidedPresenterOps mPresenter;

    // only one (Api)
    public static int list1=0, p_discount=0, p_new=0, p_popular=0, select=0, shop_product=0;
    public static int city_id=0, id_selected=0;
    ViewFlipper flip;
    TextView note;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    MaterialSpinner spinner;
    float initialX;
    RecyclerView recyclerView;
    List<Cities> list_City = new ArrayList<>();
    List<Sliders> list_Slider = new ArrayList<>();
    LinearLayout navigation_icon;
    SpaceNavigationView spaceNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // flurry
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, "ZM3JFFPDP698Q9YY3QK7");

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(0);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                try {
                    Main.this.finish();
                    Intent intent = new Intent(Main.this, Search.class);
                    startActivity(intent);
                }catch (Exception e){}
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        // nothing
                        break;

                    case 1:
                        Main.this.finish();
                        Intent intent = new Intent(Main.this, List1.class);
                        startActivity(intent);
                        break;

                    case 2:
                        Main.this.finish();
                        Intent intentshop = new Intent(Main.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }
            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();
        list_Slider = mPresenter.getSlider(city_id);

        new Get_SelectedShop2(haveNetworkConnection(), mPresenter, getAppContext(), Main.this, recyclerView, city_id);

        after_setup();
    }

    private void setupViews(){

        flip = (ViewFlipper) findViewById(R.id.viewFlipper_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        note = (TextView) findViewById(R.id.note);
    }

    private void after_setup(){

        flip.setInAnimation(this, R.anim.right);
        flip.setOutAnimation(this, R.anim.left);

        for(int i=0 ; i<list_Slider.size() ; i++){
            ImageView image = new ImageView(getApplicationContext());
            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list_Slider.get(i).getPic();
                Glide.with(getApplicationContext())
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(image);
            }catch (Exception e){
                new PostError(getApplicationContext(), e.getMessage(), Utility.getMethodName()).postError();
            }
            image.setScaleType(ImageView.ScaleType.FIT_XY);
            flip.addView(image);
        }

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        // first time
        List<String> city_name = new ArrayList<>();
        if(city_id == 0){
            for(int i=0 ; i<list_City.size() ; i++){
                city_name.add(list_City.get(i).getTitle());
                if(list_City.get(i).getSelected().equals("1")){
                    id_selected = i;
                    city_id = list_City.get(i).getId();
                }
            }
        }
        // change city
        else{
            for(int i=0 ; i<list_City.size() ; i++){
                city_name.add(list_City.get(i).getTitle());
                if(list_City.get(i).getId() == city_id){
                    id_selected = i;
                }
            }
        }
        // other setting
        spinner.setItems(city_name);
        spinner.setSelectedIndex(id_selected);
        // set text size
        spinner.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
        spinner.setTextDirection(View.TEXT_DIRECTION_RTL);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                city_id = list_City.get(position).getId();
                new Get_SelectedShop2(haveNetworkConnection(), mPresenter, getAppContext(), Main.this, recyclerView, city_id);
                // slider
                list_Slider = mPresenter.getSlider(city_id);
                flip.removeAllViews();
                for(int i=0 ; i<list_Slider.size() ; i++){
                    ImageView image = new ImageView(getApplicationContext());
                    try{
                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.placeholder(R.drawable.place_holder);
                        requestOptions.error(R.drawable.e);
                        String url = url_download + list_Slider.get(i).getPic();
                        Glide.with(getApplicationContext())
                                .setDefaultRequestOptions(requestOptions)
                                .load(url)
                                .into(image);
                    }catch (Exception e){
                        new PostError(getApplicationContext(), e.getMessage(), Utility.getMethodName()).postError();
                    }
                    image.setScaleType(ImageView.ScaleType.FIT_XY);
                    flip.addView(image);
                }
            }
        });

        note.setText(mPresenter.getNotification().get(0).getText());
        navigation_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

   @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getY() < flip.getBottom() && ev.getY() > flip.getTop()) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    initialX = ev.getX();
                    break;

                case MotionEvent.ACTION_UP:
                    float finalX = ev.getX();
                    if (initialX > finalX) {
                        flip.showNext();
                    } else {
                        flip.setInAnimation(this, R.anim.left2);
                        flip.setOutAnimation(this, R.anim.right2);
                        flip.showPrevious();
                        flip.setInAnimation(this, R.anim.right);
                        flip.setOutAnimation(this, R.anim.left);
                    }
                    break;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Main_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Main_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getMainComponent(new Main_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        Main.this.finish();
        System.exit(0);
    }
}