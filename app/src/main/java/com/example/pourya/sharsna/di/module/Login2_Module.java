package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Login2;
import com.example.pourya.sharsna.main.model.Login2_Model;
import com.example.pourya.sharsna.main.presenter.Login2_Presenter;
import com.example.pourya.sharsna.main.view.login.Login2;
import dagger.Module;
import dagger.Provides;

@Module
public class Login2_Module {

    private Login2 activity;

    public Login2_Module(Login2 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Login2 providesLogin2Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Login2.ProvidedPresenterOps providedPresenterOps() {
        Login2_Presenter presenter = new Login2_Presenter( activity );
        Login2_Model model = new Login2_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}