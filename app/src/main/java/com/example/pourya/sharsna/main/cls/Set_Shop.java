package com.example.pourya.sharsna.main.cls;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.pourya.sharsna.main.Interface.MVP_Shop;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list3.List3;
import com.example.pourya.sharsna.main.view.shop.Adapter_Shop;
import com.example.pourya.sharsna.main.view.shop.Shop;
import com.example.pourya.sharsna.main.viewModel.Products;
import com.example.pourya.sharsna.main.viewModel.Users;

import java.util.List;

public class Set_Shop extends BaseActivity {

    MVP_Shop.ProvidedPresenterOps shop_presenter;
    Context _context;
    Activity mactivity;
    RecyclerView mRecyclerView;
    List<Products> list_Products;
    int Uid;

    public Set_Shop(MVP_Shop.ProvidedPresenterOps ppo, Context c, Activity a, RecyclerView r, int id) {
        _context = c;
        mactivity = a;
        shop_presenter = ppo;
        mRecyclerView = r;
        Uid = id;
    }

    public void load() {
        list_Products = shop_presenter.List_Products(Uid);

        if(list_Products.size() == 0) {
            Shop.no_product.setVisibility(View.VISIBLE);
        }else{
            Shop.no_product.setVisibility(View.GONE);
            // 3 item null
            Products app = new Products();
            app.setPicProduct("");
            list_Products.add(app); list_Products.add(app); list_Products.add(app);
        }

        Adapter_Shop adapter = new Adapter_Shop(_context, mactivity, list_Products);
        mRecyclerView.setLayoutManager(new GridLayoutManager(_context, 3));
        mRecyclerView.setAdapter(adapter);
    }
}
