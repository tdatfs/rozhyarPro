package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_Search;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Cities;
import java.util.ArrayList;
import java.util.List;

public class Search_Model implements MVP_Search.ProvidedModelOps {

    private MVP_Search.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Cities> CList;

    public Search_Model(MVP_Search.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        CList = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public List<Cities> getCity() {
        try{
            String q = "SELECT [Id],[Title],[Selected] FROM [Cities]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Cities app = new Cities();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setTitle(cursor.getString(1));
                app.setSelected(cursor.getString(2));
                CList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return CList;
    }
}