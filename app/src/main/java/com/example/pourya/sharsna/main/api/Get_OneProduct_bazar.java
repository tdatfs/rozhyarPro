package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.tabs.Product;
import com.example.pourya.sharsna.main.viewModel.Products;
import org.json.JSONException;
import org.json.JSONObject;

public class Get_OneProduct_bazar extends BaseActivity {

    Context _context;
    Activity mactivity;
    int id;
    String mid, uid;
    ProgressDialog progressDialog;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Get_OneProduct_bazar(Context context, Activity activity, int i, String m, String u) {
        mactivity = activity;
        _context = context;
        id = i;
        mid = m;
        uid = u;
        progressDialog = new ProgressDialog(mactivity);
        get();
    }

    public Get_OneProduct_bazar get() {

        if (true) {
            if (mid.equals("")) {
                mid = "0";
            }
            if(uid.equals("")){
                uid = "0";
            }
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url + "ApiProduct/Product?id=" + id + "&memberId="+mid+"&userid="+uid, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        Products app = new Products();
                        app.setId(Integer.parseInt(response.getString("Id")));
                        app.setUserId(Integer.parseInt(response.getString("UserId")));
                        app.setNameProduct(response.getString("NameProduct"));
                        app.setPicProduct(response.getString("PicProduct"));
                        app.setPrice(response.getString("Price"));
                        app.setInsertDate(response.getString("InsertDate"));
                        app.setDes(response.getString("Des"));
                        app.setOffProduct(response.getString("OffProduct"));
                        app.setDateExpire(response.getString("DateExpire"));
                        app.setDateExpireshamsi(response.getString("DateExpireshamsi"));
                        app.setIsActive(response.getString("IsActive"));
                        app.setCountView(response.getString("CountView"));
                        app.setCountLike(response.getString("CountLike"));
                        app.setIsConfirm(response.getString("IsConfirm"));
                        app.setIsLike(response.getString("IsLike"));
                        app.setRowVersion("1");

                        // show data
                        // name
                        if(app.getNameProduct().toString().equals("") || app.getNameProduct().toString().equals("null")){
                        }else{
                            Product.namep.setText(app.getNameProduct());
                        }
                        // off
                        if(app.getOffProduct().toString().equals("") || app.getOffProduct().toString().equals("null")){
                        }else{
                            Product.offp.setText(app.getOffProduct().toString());
                        }
                        // date
                        if(app.getDateExpireshamsi().toString().equals("") || app.getDateExpireshamsi().toString().equals("null")){
                        }else{
                            Product.datep.setText(app.getDateExpireshamsi().toString());
                        }
                        // price
                        if(app.getPrice().toString().equals("") || app.getPrice().toString().equals("null")){
                        }else{
                            Product.pricep.setText(app.getPrice());
                        }
                        // des
                        if(app.getDes().toString().equals("") || app.getDes().toString().equals("null")){
                        }else{
                            Product.desp.setText(app.getDes());
                        }
                        // like_count
                        if(app.getCountLike().equals("") || app.getCountLike().equals("null")){
                            Product.likp.setText("0");
                        }else{
                            Product.likp.setText(app.getCountLike());
                        }
                        // like_image
                        if(app.getIsLike().equals("true")){
                            Product.like_back = 1;
                            Product.img_like.setBackgroundResource(R.drawable.ic_like);
                        }else if(app.getIsLike().equals("false")){
                            Product.like_back = 0;
                            Product.img_like.setBackgroundResource(R.drawable.ic_like_2);
                        }
                        // image
                        if(app.getPicProduct().toString().equals("") || app.getPicProduct().toString().equals("null")){
                        }else{
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.drawable.place_holder);
                            requestOptions.error(R.drawable.e);
                            String url = url_download + app.getPicProduct();
                            Glide.with(_context)
                                    .setDefaultRequestOptions(requestOptions)
                                    .load(url)
                                    .into(Product.img_add);
                        }

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError, "get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        } else {}
        return null;
    }
}