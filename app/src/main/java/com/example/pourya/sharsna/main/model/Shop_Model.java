package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_Shop;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import com.example.pourya.sharsna.main.viewModel.Products;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.util.ArrayList;
import java.util.List;

public class Shop_Model implements MVP_Shop.ProvidedModelOps {

    private MVP_Shop.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Users> UList;
    private List<Gulids> GList;
    private List<Cities> CList;
    private List<Products> PList;
    private List<Integer> id_product;
    private List<Integer> id_user;

    public Shop_Model(MVP_Shop.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        UList = new ArrayList<>();
        GList = new ArrayList<>();
        CList = new ArrayList<>();
        PList = new ArrayList<>();
        id_product = new ArrayList<>();
        id_user = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public Users Get_User(int id) {
        try{
            String q = "SELECT [Id],[GulidId],[GuildName],[NameShop],[Address],[Operator],[Phone],[PicProfile],[CellPhone],[Email],[WebSilte],[Telegram],[Instagram],[CountView],[CountFollowe],[Rating],[IsFollow],[IsGeneral] FROM [Users] where [Id] = "+id;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Users app = new Users();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setGulidId(Integer.parseInt(cursor.getString(1)));
                app.setGuildName(cursor.getString(2));
                app.setNameShop(cursor.getString(3));
                app.setAddress(cursor.getString(4));
                app.setOperator(cursor.getString(5));
                app.setPhone(cursor.getString(6));
                app.setPicProfile(cursor.getString(7));
                app.setCellPhone(cursor.getString(8));
                app.setEmail(cursor.getString(9));
                app.setWebSilte(cursor.getString(10));
                app.setTelegram(cursor.getString(11));
                app.setInstagram(cursor.getString(12));
                app.setCountView(cursor.getString(13));
                app.setCountFollowe(cursor.getString(14));
                app.setRating(cursor.getString(15));
                app.setFollow(cursor.getString(16));
                app.setGeneral(cursor.getString(17));
                UList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return UList.get(0);
    }

    @Override
    public List<Products> List_Products(int id) {
        PList = new ArrayList<>();
        try{
            String q = "SELECT [Id],[UserId],[PicProduct] FROM [Products] where [UserId] = "+id;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Products app = new Products();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setUserId(Integer.parseInt(cursor.getString(1)));
                app.setPicProduct(cursor.getString(2));
                PList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return PList;
    }

    @Override
    public String MaxRowV_Products() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Products";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public void Insert_Products(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public List<Integer> Id_Products() {
        id_product = new ArrayList<>();
        try{
            String q = "SELECT [Id] FROM [Products]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_product.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_product;
    }

    @Override
    public Gulids Get_Gulid(int id) {
        try{
            String q = "SELECT [Id],[GuildName],[ParentId],[Icon],[RowVersion] FROM [Gulids] where [Id] = "+id;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Gulids app = new Gulids();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setGuildName(cursor.getString(1));
                app.setParentId(cursor.getString(2));
                app.setIcon(cursor.getString(3));
                app.setRowVersion(cursor.getString(4));
                GList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return GList.get(0);
    }

    @Override
    public List<Cities> getCity() {
        try{
            String q = "SELECT [Id],[Title] FROM [Cities]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Cities app = new Cities();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setTitle(cursor.getString(1));
                CList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return CList;
    }

    @Override
    public void update_follow(String follow ,int count, int id) {
        String q = "update [Users] set [IsFollow] = '" + follow + "' , [CountFollowe] = "+ count +" where [Id] = "+ id;
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int c = cursor.getCount();
        cursor.moveToFirst();
    }

}