package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.example.pourya.sharsna.main.Interface.MVP_List1;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.util.ArrayList;
import java.util.List;

public class List1_Model implements MVP_List1.ProvidedModelOps {

    private MVP_List1.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Gulids> GList;
    private List<Integer> id_gulid;
    private List<Cities> CList;

    public List1_Model(MVP_List1.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        GList = new ArrayList<>();
        id_gulid = new ArrayList<>();
        CList = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public int Count_Gulids() {
        String q="SELECT Count([Id]) as x FROM Gulids";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        int id=0;
        for (int i = 0; i < count; i++) {
            id=cursor.getInt(0);
        }
        return id;
    }

    @Override
    public List<Gulids> List_Gulids() {
        try{
            String q = "SELECT [Id],[GuildName],[ParentId],[Icon] FROM [Gulids] where [ParentId] = 'null'";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Gulids app = new Gulids();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setGuildName(cursor.getString(1));
                app.setParentId(cursor.getString(2));
                app.setIcon(cursor.getString(3));
                GList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return GList;
    }

    @Override
    public void Insert_Gulids(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public String MaxRowV_Gulids() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Gulids";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public List<Integer> Id_Gulids() {
        try{
            String q = "SELECT [Id] FROM [Gulids]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_gulid.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_gulid;
    }

    @Override
    public List<Cities> getCity() {
        try{
            String q = "SELECT [Id],[Title] FROM [Cities]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Cities app = new Cities();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setTitle(cursor.getString(1));
                CList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return CList;
    }
}