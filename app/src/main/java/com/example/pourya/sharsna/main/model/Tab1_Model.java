package com.example.pourya.sharsna.main.model;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Tab1;
import com.example.pourya.sharsna.main.db.DBAdapter;

public class Tab1_Model implements MVP_Tab1.ProvidedModelOps {

    private MVP_Tab1.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;

    public Tab1_Model(MVP_Tab1.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }
}