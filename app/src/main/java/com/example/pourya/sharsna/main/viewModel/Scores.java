package com.example.pourya.sharsna.main.viewModel;

public class Scores {

    private int Id;
    private int UserId;
    private int MemberId;
    private int SecondUserId;
    private String MemberFullName;
    private String User1NameShop;
    private String Point;
    private String Comment;
    private String RowVersion;

    public Scores() {}

    public String getUser1NameShop() {
        return User1NameShop;
    }

    public void setUser1NameShop(String user1NameShop) {
        User1NameShop = user1NameShop;
    }

    public int getSecondUserId() {
        return SecondUserId;
    }

    public void setSecondUserId(int secondUserId) {
        SecondUserId = secondUserId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getMemberId() {
        return MemberId;
    }

    public void setMemberId(int memberId) {
        MemberId = memberId;
    }

    public String getMemberFullName() {
        return MemberFullName;
    }

    public void setMemberFullName(String memberFullName) {
        MemberFullName = memberFullName;
    }

    public String getPoint() {
        return Point;
    }

    public void setPoint(String point) {
        Point = point;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String rowVersion) {
        RowVersion = rowVersion;
    }
}