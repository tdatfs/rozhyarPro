package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Shop;
import com.example.pourya.sharsna.main.model.Shop_Model;
import com.example.pourya.sharsna.main.presenter.Shop_Presenter;
import com.example.pourya.sharsna.main.view.shop.Shop;

import dagger.Module;
import dagger.Provides;

@Module
public class Shop_Module {

    private Shop activity;

    public Shop_Module(Shop activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Shop providesShopActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Shop.ProvidedPresenterOps providedPresenterOps() {
        Shop_Presenter presenter = new Shop_Presenter( activity );
        Shop_Model model = new Shop_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
