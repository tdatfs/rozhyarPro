package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.tabs.Adapter_New;
import com.example.pourya.sharsna.main.view.tabs.Tabs_New;
import com.example.pourya.sharsna.main.viewModel.Products;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class Get_New extends BaseActivity {

    Context _context;
    Activity mactivity;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    String mid, uid;
    int cid;

    public Get_New(Context context, Activity activity, RecyclerView r, String id, int c, String u) {
        mactivity = activity;
        _context = context;
        progressDialog = new ProgressDialog(mactivity);
        recyclerView = r;
        mid = id;
        cid = c;
        uid = u;
        get();
    }

    public Get_Discount get() {
        if(mid.equals("")){
            mid = "0";
        }
        if(uid.equals("")){
            uid = "0";
        }
        final List<Products> PList = new ArrayList<>();

        if(Main.p_new == 0){
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    url+ "ApiProduct/News?memberId="+mid+"&CityId="+cid+"&userid="+uid, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    try {
                        for (int i=0; i<response.length(); i++) {

                            JSONObject jsonObject = response.getJSONObject(i);

                            Products app = new Products();
                            app.setId(Integer.parseInt(jsonObject.getString("Id")));
                            app.setUserId(Integer.parseInt(jsonObject.getString("UserId")));
                            app.setNameProduct(jsonObject.getString("NameProduct"));
                            app.setPicProduct( jsonObject.getString("PicProduct"));
                            app.setPrice(jsonObject.getString("Price"));
                            app.setInsertDate(jsonObject.getString("InsertDate"));
                            app.setDateExpireshamsi(jsonObject.getString("DateExpireshamsi"));
                            app.setDes(jsonObject.getString("Des"));
                            app.setOffProduct(jsonObject.getString("OffProduct"));
                            app.setDateExpire(jsonObject.getString("DateExpire"));
                            app.setIsActive(jsonObject.getString("IsActive"));
                            app.setCountView(jsonObject.getString("CountView"));
                            app.setCountLike(jsonObject.getString("CountLike"));
                            app.setIsConfirm(jsonObject.getString("IsConfirm"));
                            app.setRowVersion("1");

                            PList.add(app);
                        }

                        progressDialog.dismiss();

                        if(PList.size() == 0){
                            Tabs_New.no_product.setVisibility(View.VISIBLE);
                        }else{
                            Tabs_New.no_product.setVisibility(View.GONE);
                        }

                        // 2 item null
                        Products app = new Products();
                        app.setNameProduct("");
                        PList.add(app); PList.add(app);

                        // set data to recycler view
                        Adapter_New adapter = new Adapter_New(_context, mactivity, PList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(_context, LinearLayoutManager.VERTICAL,false));
                        recyclerView.setAdapter(adapter);

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);
        }
        return null;
    }
}