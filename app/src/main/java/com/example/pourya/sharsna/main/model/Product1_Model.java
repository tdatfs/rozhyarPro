package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_Product1;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.util.ArrayList;
import java.util.List;

public class Product1_Model implements MVP_Product1.ProvidedModelOps {

    private MVP_Product1.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Cities> CList;
    private List<Products> PList;

    public Product1_Model(MVP_Product1.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        CList = new ArrayList<>();
        PList = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public Products Get_Products(int id) {
        try{
            String q = "SELECT [Id],[UserId],[NameProduct],[PicProduct],[Price],[InsertDate],[Des],[OffProduct],[DateExpire],[DateExpireshamsi],[IsActive],[CountView],[CountLike],[IsConfirm],[IsLike],[RowVersion] FROM [Products] where [Id] = "+id;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Products app = new Products();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setUserId(Integer.parseInt(cursor.getString(1)));
                app.setNameProduct(cursor.getString(2));
                app.setPicProduct(cursor.getString(3));
                app.setPrice(cursor.getString(4));
                app.setInsertDate(cursor.getString(5));
                app.setDes(cursor.getString(6));
                app.setOffProduct(cursor.getString(7));
                app.setDateExpire(cursor.getString(8));
                app.setDateExpireshamsi(cursor.getString(9));
                app.setIsActive(cursor.getString(10));
                app.setCountView(cursor.getString(11));
                app.setCountLike(cursor.getString(12));
                app.setIsConfirm(cursor.getString(13));
                app.setIsLike(cursor.getString(14));
                app.setRowVersion(cursor.getString(15));
                PList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return PList.get(0);
    }

    @Override
    public List<Cities> getCity() {
        try{
            String q = "SELECT [Id],[Title],[Selected] FROM [Cities]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Cities app = new Cities();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setTitle(cursor.getString(1));
                app.setSelected(cursor.getString(2));
                CList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return CList;
    }

    @Override
    public void update_like(String like, int count, int id) {
        String q = "update [Products] set [IsLike] = '"+ like +"' , [CountLike] = "+ count +" where [Id] = "+ id ;
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int c = cursor.getCount();
        cursor.moveToFirst();
    }
}