package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_ChangePass;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.login.Login2;

public class Cheak_PLogin2 extends BaseActivity {

    Context _context;
    Activity _activity;
    String user_name, _password;
    boolean mnet;
    ProgressDialog progressDialog;

    public Cheak_PLogin2(boolean net, Context context, Activity activity, String username, String password) {
        mnet = net;
        _context = context;
        _activity = activity;
        user_name = username;
        _password = password;
        progressDialog = new ProgressDialog(activity);
        post();
    }

    public Cheak_PLogin2 post(){

        if(mnet){
            progressDialog.setMessage("لطفا صبر کنید ...");
            progressDialog.show();

            StringRequest sr = new StringRequest(Request.Method.GET, url+ "ApiLogin/CheckLogin?UserName="+user_name+"&Pass="+_password+"&kind=true",
            new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try{
                    if(response.equals("0")){
                        progressDialog.dismiss();
                        Toast.makeText(_context, "اطلاعات وارد شده صحیح نیست", Toast.LENGTH_LONG).show();
                        Login2.sp2 = _context.getSharedPreferences("DB2",0);
                        SharedPreferences.Editor edit = Login2.sp2.edit();
                        edit.putString("user2","");
                        edit.putString("pass2","");
                        edit.putString("uid2","");
                        edit.commit();
                    }else{
                        Toast.makeText(_context,"ورود با موفقیت انجام شد",Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        Login2.sp2 = _context.getSharedPreferences("DB2",0);
                        SharedPreferences.Editor edit = Login2.sp2.edit();
                        edit.putString("user2",user_name);
                        edit.putString("pass2",_password);
                        edit.putString("uid2",Math.abs(Integer.parseInt(response))+"");
                        edit.commit();
                        // ----------------------- clean other-------------------------
                        Login1.sp1 = _context.getSharedPreferences("DB1",0);
                        SharedPreferences.Editor edit1 = Login1.sp1.edit();
                        edit1.putString("user1","");
                        edit1.putString("uid1","");
                        edit1.commit();
                        // force
                        if(Integer.parseInt(response) < 0){
                            Dialog_ChangePass cc = new Dialog_ChangePass(_activity);
                            cc.show();
                        }else{}
                    }
                }catch (Exception e){
                    new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                new ErrorVolley(_context).Error(volleyError,"get");
                if (volleyError.networkResponse == null) {
                    if (volleyError.getClass().equals(TimeoutError.class)) {}
                }
            }
        });

        SampleApp.getInstance().addToRequestQueue(sr);

        }else{
            Toast.makeText(_context, "دسترسی به اینترنت امکان پذیر نیست", Toast.LENGTH_LONG).show();
        }

        return null;
    }
}