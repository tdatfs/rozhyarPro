package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Code_Module;
import com.example.pourya.sharsna.di.module.Login1_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.login.Code;
import com.example.pourya.sharsna.main.view.login.Login1;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Code_Module.class )
public interface Code_Component {
    Code inject(Code activity);
}