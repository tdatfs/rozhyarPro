package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Code;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.profile_login1.Tabs_main1;

public class Get_StepTwo extends BaseActivity {
    Context _context;
    Activity mactivity;
    boolean mnet;
    String mobile, code;
    ProgressDialog progressDialog;
    boolean pop;

    public Get_StepTwo(boolean net, Context context, Activity activity, String m, String c, boolean p) {
        mactivity = activity;
        _context = context;
        mnet = net;
        mobile = m;
        code = c;
        pop = p;
        progressDialog = new ProgressDialog(activity);
        get();
    }

    public Get_StepTwo get() {

        if(mnet){
            progressDialog.setMessage("لطفا صبر کنید ...");
            progressDialog.show();

            StringRequest sr = new StringRequest(Request.Method.GET, url+ "ApiUser/SetpTwo?id="+mobile+"&code="+code,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try{
                                if(response.equals("0")){
                                    progressDialog.dismiss();
                                    Toast.makeText(_context, "اطلاعات وارد شده صحیح نیست", Toast.LENGTH_LONG).show();
                                }else{
                                    progressDialog.dismiss();
                                    // save sharedPreferences
                                    Login1.sp1 = _context.getSharedPreferences("DB1",0);
                                    SharedPreferences.Editor edit = Login1.sp1.edit();
                                    edit.putString("user1",mobile);
                                    edit.putString("uid1",response);
                                    edit.commit();
                                    // what do
                                    if(pop){
                                        Toast.makeText(_context, "ورود با موفقیت انجام شد", Toast.LENGTH_LONG).show();
                                    }else{
                                        // profile page
                                        Tabs_main1.IdUser = Integer.parseInt(response);
                                        mactivity.finish();
                                        mactivity.startActivity(new Intent(mactivity, Tabs_main1.class));
                                    }
                                }
                            }catch (Exception e){
                                new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(sr);

        }else{
            Toast.makeText(_context, "دسترسی به اینترنت امکان پذیر نیست", Toast.LENGTH_LONG).show();
        }

        return null;
    }
}