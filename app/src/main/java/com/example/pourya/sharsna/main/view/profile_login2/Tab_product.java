package com.example.pourya.sharsna.main.view.profile_login2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Get_Product2;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.login.Login2;

public class Tab_product extends Fragment {

    public static TextView pnumber;
    public static String is_confirm;
    ImageView img;
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_product, container, false);

        pnumber = (TextView) view.findViewById(R.id.pnumber);
        img = (ImageView) view.findViewById(R.id.add_product);
        recyclerView =(RecyclerView) view.findViewById(R.id.rv_myprofile_product);

        Login1.sp1 = getContext().getSharedPreferences("DB1",0);
        String mid = Login1.sp1.getString("uid1","");
        Login2.sp2 = getContext().getSharedPreferences("DB2",0);
        String uid = Login2.sp2.getString("uid2","");
        new Get_Product2(getContext(), getActivity(), recyclerView, Tabs_main2.IdUser, mid, uid);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(is_confirm.equals("true")){
                    Intent intent = new Intent(getContext(), AddProduct.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                    getActivity().finish();
                }else{
                    Toast.makeText(getContext(), "مغازه دار گرامی اکانت شما هنوز تایید نشده است", Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;
    }
}