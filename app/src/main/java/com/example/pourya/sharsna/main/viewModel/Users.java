package com.example.pourya.sharsna.main.viewModel;

import java.util.Date;

public class Users {

    private int Id;
    private int GulidId;
    private String GuildName;
    private String Code;
    private int UserTypeId;
    private String NameShop;
    private String Address;
    private String Operator;
    private String Phone;
    private String PicProfile;
    private String CellPhone;
    private String Email;
    private String WebSilte;
    private String Telegram;
    private String Instagram;
    private String Coordination;
    private String UserName;
    private String Pass;
    private String IsConfirm;
    private boolean IsForceChangePass;
    private int CityId;
    private String CountView;
    private String CountFollowe;
    private String Rating;
    private String PicCover;
    private Date DateInsert;
    private boolean IsRepet;
    private int RepeatId;
    private Date ExeDate;
    private int MarketerId;
    private String IsFollow;
    private String IsGeneral;
    private float Distance;
    private int Remainingdays;
    private String RowVersion;

    public Users() {}

    public int getRemainingdays() {
        return Remainingdays;
    }

    public void setRemainingdays(int remainingdays) {
        Remainingdays = remainingdays;
    }

    public float getDistance() {
        return Distance;
    }

    public void setDistance(float distance) {
        Distance = distance;
    }

    public String isGeneral() {
        return IsGeneral;
    }

    public void setGeneral(String general) {
        IsGeneral = general;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getGulidId() {
        return GulidId;
    }

    public void setGulidId(int gulidId) {
        GulidId = gulidId;
    }

    public String getGuildName() {
        return GuildName;
    }

    public void setGuildName(String guildName) {
        GuildName = guildName;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public int getUserTypeId() {
        return UserTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        UserTypeId = userTypeId;
    }

    public String getNameShop() {
        return NameShop;
    }

    public void setNameShop(String nameShop) {
        NameShop = nameShop;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPicProfile() {
        return PicProfile;
    }

    public void setPicProfile(String picProfile) {
        PicProfile = picProfile;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String cellPhone) {
        CellPhone = cellPhone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getWebSilte() {
        return WebSilte;
    }

    public void setWebSilte(String webSilte) {
        WebSilte = webSilte;
    }

    public String getTelegram() {
        return Telegram;
    }

    public void setTelegram(String telegram) {
        Telegram = telegram;
    }

    public String getInstagram() {
        return Instagram;
    }

    public void setInstagram(String instagram) {
        Instagram = instagram;
    }

    public String getCoordination() {
        return Coordination;
    }

    public void setCoordination(String coordination) {
        Coordination = coordination;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String pass) {
        Pass = pass;
    }

    public String getPicCover() {
        return PicCover;
    }

    public void setPicCover(String picCover) {
        PicCover = picCover;
    }

    public String isConfirm() {
        return IsConfirm;
    }

    public void setConfirm(String confirm) {
        IsConfirm = confirm;
    }

    public boolean isForceChangePass() {
        return IsForceChangePass;
    }

    public void setForceChangePass(boolean forceChangePass) {
        IsForceChangePass = forceChangePass;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getCountView() {
        return CountView;
    }

    public void setCountView(String countView) {
        CountView = countView;
    }

    public String getCountFollowe() {
        return CountFollowe;
    }

    public void setCountFollowe(String countFollowe) {
        CountFollowe = countFollowe;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public Date getDateInsert() {
        return DateInsert;
    }

    public void setDateInsert(Date dateInsert) {
        DateInsert = dateInsert;
    }

    public boolean isRepet() {
        return IsRepet;
    }

    public void setRepet(boolean repet) {
        IsRepet = repet;
    }

    public int getRepeatId() {
        return RepeatId;
    }

    public void setRepeatId(int repeatId) {
        RepeatId = repeatId;
    }

    public Date getExeDate() {
        return ExeDate;
    }

    public void setExeDate(Date exeDate) {
        ExeDate = exeDate;
    }

    public int getMarketerId() {
        return MarketerId;
    }

    public void setMarketerId(int marketerId) {
        MarketerId = marketerId;
    }

    public String isFollow() {
        return IsFollow;
    }

    public void setFollow(String follow) {
        IsFollow = follow;
    }

    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String rowVersion) {
        RowVersion = rowVersion;
    }
}
