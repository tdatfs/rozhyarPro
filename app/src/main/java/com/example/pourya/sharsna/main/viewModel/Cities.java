package com.example.pourya.sharsna.main.viewModel;

public class Cities {

    private int Id;
    private String Title;
    private String Selected;

    public Cities(){}

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSelected() {
        return Selected;
    }

    public void setSelected(String selected) {
        Selected = selected;
    }
}
