package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_List3;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.util.ArrayList;
import java.util.List;

public class List3_Model implements MVP_List3.ProvidedModelOps {

    private MVP_List3.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Users> UList;
    private List<Integer> id_user;
    private List<Cities> CList;

    public List3_Model(MVP_List3.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        UList = new ArrayList<>();
        id_user = new ArrayList<>();
        CList = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public List<Users> List_Users(int id, int cid) {
        UList = new ArrayList<>();
        try{
            String q = "SELECT [Id],[GulidId],[GuildName],[NameShop],[Address],[Operator],[Phone],[PicProfile],[CellPhone],[Email],[WebSilte],[Telegram],[Instagram],[CityId],[Rating],[IsGeneral],[RowVersion] FROM [Users] where GulidId = "+id+ " and CityId="+ cid;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Users app = new Users();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setGulidId(Integer.parseInt(cursor.getString(1)));
                app.setGuildName(cursor.getString(2));
                app.setNameShop(cursor.getString(3));
                app.setAddress(cursor.getString(4));
                app.setOperator(cursor.getString(5));
                app.setPhone(cursor.getString(6));
                app.setPicProfile(cursor.getString(7));
                app.setCellPhone(cursor.getString(8));
                app.setEmail(cursor.getString(9));
                app.setWebSilte(cursor.getString(10));
                app.setTelegram(cursor.getString(11));
                app.setInstagram(cursor.getString(12));
                app.setCityId(Integer.parseInt(cursor.getString(13)));
                app.setRating(cursor.getString(14));
                app.setGeneral(cursor.getString(15));
                app.setRowVersion(cursor.getString(16));
                UList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(), Utility.getMethodName()).postError();
        }
        return UList;
    }

    @Override
    public String MaxRowV_Users() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Users";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public void Insert_Users(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public List<Integer> Id_Users() {
        try{
            String q = "SELECT [Id] FROM [Users]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_user.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_user;
    }

    @Override
    public List<Cities> getCity() {
        try{
            String q = "SELECT [Id],[Title],[Selected] FROM [Cities]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Cities app = new Cities();
                app.setId(Integer.parseInt(cursor.getString(0)));
                app.setTitle(cursor.getString(1));
                app.setSelected(cursor.getString(2));
                CList.add(app);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return CList;
    }

    @Override
    public String Status_Gulid(int gid) {
        String result = "";
        try{
            String q = "SELECT [GetServer] FROM [Gulids] where [Id] = "+gid;
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count = cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                result = cursor.getString(0);
                cursor.moveToNext();
            }
        }
        catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return result;
    }

    @Override
    public void Chanege_Status_Gulid(int gid) {
        String q = "update [Gulids] set [GetServer]='true' where [Id] ="+gid;
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int count = cursor.getCount();
        cursor.moveToFirst();
    }
}