package com.example.pourya.sharsna.main.view.shop;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.viewModel.Scores;
import java.util.List;

public class Adapter_Comment extends RecyclerView.Adapter<Adapter_Comment.ViewHolder> {

    Context context;
    Activity activity;
    List<Scores> Slist;

    public Adapter_Comment(Context c, Activity a, List<Scores> l) {
        context = c;
        activity = a;
        Slist = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView comment;
        private TextView user_name;
        private RatingBar rating;

        public ViewHolder(View itemView) {
            super(itemView);
            comment = (TextView) itemView.findViewById(R.id.comments_);
            user_name = (TextView) itemView.findViewById(R.id.name_);
            rating = (RatingBar) itemView.findViewById(R.id.ratingbar1);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.comment.setText(Slist.get(position).getComment().toString());
        if(Slist.get(position).getUser1NameShop().equals("") || Slist.get(position).getUser1NameShop().equals("null")){
            if(Slist.get(position).getMemberFullName().equals("") || Slist.get(position).getMemberFullName().equals("null")){
                holder.user_name.setText("کاربر مهمان");
            }else{
                holder.user_name.setText(Slist.get(position).getMemberFullName().toString());
            }
        }else{
            holder.user_name.setText(Slist.get(position).getUser1NameShop().toString());
        }
        String r = Slist.get(position).getPoint();
        if(r == null || r.equals("null")){
            holder.rating.setRating((float) 5);
        }else{
            holder.rating.setRating((float) Float.parseFloat(Slist.get(position).getPoint()));
        }
    }

    @Override
    public int getItemCount() {
        return Slist.size();
    }
}