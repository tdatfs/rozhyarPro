package com.example.pourya.sharsna.main.model;

import android.content.Context;
import android.database.Cursor;
import com.example.pourya.sharsna.main.Interface.MVP_Splash;
import com.example.pourya.sharsna.main.db.DBAdapter;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import java.util.ArrayList;
import java.util.List;

public class Splash_Model implements MVP_Splash.ProvidedModelOps {

    private MVP_Splash.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;
    private List<Integer> id_slider;
    private List<Integer> id_selectedshop;
    private List<Integer> id_notification;

    public Splash_Model(MVP_Splash.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
        id_slider = new ArrayList<>();
        id_selectedshop = new ArrayList<>();
        id_notification = new ArrayList<>();
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }

    @Override
    public int Count_Slider() {
        int count = 0;
        try {
            String q = "SELECT [Id] FROM [Sliders]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            count = cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context, ex.getMessage(), Utility.getMethodName()).postError();
        }
        return count;
    }

    @Override
    public List<Integer> List_Slider() {
        try{
            String q = "SELECT [Id] FROM [Sliders]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_slider.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_slider;
    }

    @Override
    public String MaxRowV_Slider() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Sliders";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public void Insert_Slider(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public void Delete_Slider() {
        String q = "Delete from [Sliders]";
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int count = cursor.getCount();
        cursor.moveToFirst();
    }

    @Override
    public int Count_SelectedShop() {
        int count = 0;
        try {
            String q =  "SELECT [UserId] FROM [SelectedShops]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            count = cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context, ex.getMessage(), Utility.getMethodName()).postError();
        }
        return count;
    }

    @Override
    public List<Integer> List_SelectedShop() {
        try{
            String q = "SELECT [UserId] FROM [SelectedShops]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_selectedshop.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_selectedshop;
    }

    @Override
    public void Delete_SelectedShop() {
        String q = "Delete from [SelectedShops]";
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int count = cursor.getCount();
        cursor.moveToFirst();
    }

    @Override
    public void Insert_SelectedShop(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public int Count_Notification() {
        int count = 0;
        try {
            String q = "SELECT [Id] FROM [Notifications]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            count = cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context, ex.getMessage(), Utility.getMethodName()).postError();
        }
        return count;
    }

    @Override
    public List<Integer> List_Notification() {
        try{
            String q = "SELECT [Id] FROM [Notifications]";
            Cursor cursor = dbAdapter.ExecuteQ(q);
            int count=cursor.getCount();
            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                id_notification.add(Integer.parseInt(cursor.getString(0)));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            new PostError(context,ex.getMessage(),Utility.getMethodName()).postError();
        }
        return id_notification;
    }

    @Override
    public String MaxRowV_Notification() {
        String q="SELECT MAX(RowVersion) as RowVersion FROM Notifications";
        Cursor cursor=dbAdapter.ExecuteQ(q);
        int count=cursor.getCount();
        cursor.moveToFirst();
        String id="0x0";
        for (int i = 0; i < count; i++) {
            id=cursor.getString(0);
        }
        if(null==id)
            id="0x0";
        return id;
    }

    @Override
    public void Insert_Notification(String Q) {
        dbAdapter.ExecuteQ(Q);
    }

    @Override
    public void Delete_Notification() {
        String q = "Delete from [Notifications]";
        Cursor cursor = dbAdapter.ExecuteQ(q);
        int count = cursor.getCount();
        cursor.moveToFirst();
    }
}