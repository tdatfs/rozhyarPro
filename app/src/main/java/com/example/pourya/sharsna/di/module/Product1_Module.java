package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Product1;
import com.example.pourya.sharsna.main.model.Product1_Model;
import com.example.pourya.sharsna.main.presenter.Product1_Presenter;
import com.example.pourya.sharsna.main.view.shop.Product1;
import dagger.Module;
import dagger.Provides;

@Module
public class Product1_Module {

    private Product1 activity;

    public Product1_Module(Product1 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Product1 providesProduct1Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Product1.ProvidedPresenterOps providedPresenterOps() {
        Product1_Presenter presenter = new Product1_Presenter( activity );
        Product1_Model model = new Product1_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
