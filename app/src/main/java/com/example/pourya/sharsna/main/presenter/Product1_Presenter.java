package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Product1;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.lang.ref.WeakReference;
import java.util.List;

public class Product1_Presenter implements MVP_Product1.ProvidedPresenterOps, MVP_Product1.RequiredPresenterOps{

    private WeakReference<MVP_Product1.RequiredViewOps> mView;
    private MVP_Product1.ProvidedModelOps mModel;

    public Product1_Presenter(MVP_Product1.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Product1.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Product1.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public Products Get_Products(int id) {
        return mModel.Get_Products(id);
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    @Override
    public void update_like(String like, int count, int id) {
        mModel.update_like(like,count,id);
    }

    public void setModel(MVP_Product1.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}