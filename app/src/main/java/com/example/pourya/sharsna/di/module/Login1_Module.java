package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Login1;
import com.example.pourya.sharsna.main.model.Login1_Model;
import com.example.pourya.sharsna.main.presenter.Login1_Presenter;
import com.example.pourya.sharsna.main.view.login.Login1;
import dagger.Module;
import dagger.Provides;

@Module
public class Login1_Module {

    private Login1 activity;

    public Login1_Module(Login1 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Login1 providesLogin1Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Login1.ProvidedPresenterOps providedPresenterOps() {
        Login1_Presenter presenter = new Login1_Presenter( activity );
        Login1_Model model = new Login1_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}