package com.example.pourya.sharsna.main.view.tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.util.List;

public class Adapter_Discount extends RecyclerView.Adapter<Adapter_Discount.ViewHolder> {

    Context context;
    Activity activity;
    List<Products> list;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_Discount(Context c, Activity a, List<Products> l) {
        context = c;
        activity = a;
        list = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, discount, d2;
        private ImageView img, d1;
        private LinearLayout all, one;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            discount = (TextView) itemView.findViewById(R.id.discount_percent) ;
            img = (ImageView) itemView.findViewById(R.id.img_discount_main);
            d2 = (TextView) itemView.findViewById(R.id.d2) ;
            d1 = (ImageView) itemView.findViewById(R.id.d1);
            all = (LinearLayout) itemView.findViewById(R.id.all);
            one = (LinearLayout) itemView.findViewById(R.id.one);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_discount, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // not show
        if(list.get(position).getNameProduct().equals("")){
            holder.name.setText("");
            holder.discount.setText("");
            holder.d2.setText("");
            holder.d1.setImageResource(R.drawable.non);
            holder.img.setImageResource(R.drawable.non);
            holder.all.setBackgroundResource(R.drawable.non);
            holder.one.setBackgroundResource(R.drawable.non);
        }
        // normal
        else {
            holder.all.setBackgroundColor(context.getResources().getColor(R.color.Main_color_bg));
            holder.one.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.d2.setText("%");
            holder.d1.setImageResource(R.drawable.ic_discount);

            holder.name.setText(list.get(position).getNameProduct());
            if(list.get(position).getOffProduct().toString().equals("null") || list.get(position).getOffProduct().toString().equals("")){
                holder.discount.setText("0");
            }else{
                holder.discount.setText(list.get(position).getOffProduct());
            }

            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list.get(position).getPicProduct();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(holder.img);
            }catch (Exception e){
                new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
            }

            final int id = list.get(position).getId();
            final int uid = list.get(position).getUserId();
            holder.all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product.Pid = id;
                    Product.UID = uid;
                    activity.finish();
                    Product.TabIndex = 2;
                    view.getContext().startActivity(new Intent(view.getContext(), Product.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}