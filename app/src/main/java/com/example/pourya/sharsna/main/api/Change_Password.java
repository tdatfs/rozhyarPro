package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.profile_login2.Tabs_main2;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;

public class Change_Password extends BaseActivity {

    Context _context;
    Activity _activity;
    int Uid;
    String old_pass, new_pass;
    ProgressDialog progressDialog;
    boolean finish;

    public Change_Password(Context context, Activity activity, int id, String o, String n, boolean fin) {

        progressDialog = new ProgressDialog(activity);
        _context = context;
        _activity = activity;
        Uid = id;
        old_pass = o;
        new_pass = n;
        finish = fin;
    }

    public void post() throws JSONException {
        progressDialog.setMessage("لطفا صبر کنید ...");
        progressDialog.show();

        JSONObject _jsonBody=new JSONObject();
        _jsonBody.put("id", Uid);
        _jsonBody.put("oldPass", old_pass);
        _jsonBody.put("newPass", new_pass);

        RequestQueue requestQueue = Volley.newRequestQueue(_context);
        final String requestBody = _jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT,url+"ApiUser/ChangePassword?id="+Uid+"&oldPass="+old_pass+"&newPass="+new_pass,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(response.equals("200")){
                        progressDialog.dismiss();
                        Tabs_main2.force_change = false;
                        Toast.makeText(_context,"تغییرات با موفقیت ثبت شد",Toast.LENGTH_LONG).show();
                        if(finish){
                            _activity.finish();
                        }
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(_context,"خطا در ارسال اطلاعات به سرور",Toast.LENGTH_LONG).show();
                    }

                }catch (Exception e){
                    new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        new ErrorVolley(_context).Error(error, "post");
                    }
                })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}