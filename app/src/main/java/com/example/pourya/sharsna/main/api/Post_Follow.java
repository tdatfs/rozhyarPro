package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.Interface.MVP_Shop;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.shop.Shop;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;

public class Post_Follow extends BaseActivity {

    Context _context;
    Activity _activity;
    boolean mnet;
    int userid, UU, count;
    String memberid, followersuserid;
    ProgressDialog progressDialog;
    MVP_Shop.ProvidedPresenterOps mPresenter;

    public Post_Follow(boolean net, MVP_Shop.ProvidedPresenterOps mP, Context context, Activity activity, int uid, String mid, String fuid, int uu, int cf) {

        progressDialog = new ProgressDialog(activity);
        mnet = net;
        _context = context;
        _activity = activity;
        userid = uid;
        memberid = mid;
        mPresenter = mP;
        UU = uu;
        count = cf;
        followersuserid = fuid;
    }

    public void post() throws JSONException {
        if (memberid.equals("")) {
            memberid = "0";
        }
        if (followersuserid.equals("")) {
            followersuserid = "0";
        }

        progressDialog.setMessage("لطفا صبر کنید ...");
        progressDialog.show();

        JSONObject _jsonBody=new JSONObject();
        _jsonBody.put("UserId", userid);
        _jsonBody.put("MemberId", memberid);
        _jsonBody.put("FollowersUserId", followersuserid);

        RequestQueue requestQueue = Volley.newRequestQueue(_context);
        final String requestBody = _jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,url+"ApiFollower/Follow",
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(response.equals("200")){
                        Shop.f.setText("دنبال شده");
                        Shop.backgrond.setBackgroundResource(R.drawable.ic_button_follow);
                        Shop.background_follower = 1;
                        progressDialog.dismiss();
                        Toast.makeText(_context,"وضعیت به دنبال شده تغییر یافت",Toast.LENGTH_LONG).show();
                        // update one user
                        // change isfollow & countfollow
                        mPresenter.update_follow("true",count+1,UU);
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(_context,"خطا در ارسال اطلاعات به سرور",Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        new ErrorVolley(_context).Error(error, "post");
                    }
                })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}