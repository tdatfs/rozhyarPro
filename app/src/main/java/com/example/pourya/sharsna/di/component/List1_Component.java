package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.List1_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.list1.List1;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = List1_Module.class )
public interface List1_Component {
    List1 inject(List1 activity);
}