package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.profile_login2.Tab_info;
import com.example.pourya.sharsna.main.view.profile_login2.Tab_product;
import com.example.pourya.sharsna.main.viewModel.Users;
import org.json.JSONException;
import org.json.JSONObject;

public class Get_info2 extends BaseActivity {

    Context _context;
    Activity mactivity;
    int id;
    String mid, uid;
    ProgressDialog progressDialog;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Get_info2(Context context, Activity activity, int i, String m, String u) {
        mactivity = activity;
        _context = context;
        id = i;
        mid = m;
        uid = u;
        progressDialog = new ProgressDialog(mactivity);
        get();
    }

    public Get_info2 get() {

        if (true) {
            id = Math.abs(id);
            if (mid.equals("")) {
                mid = "0";
            }
            if (uid.equals("")) {
                uid = "0";
            }
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url + "ApiUser/User?id=" + id + "&row=0x0&memberId="+mid+"&userid="+uid, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        Users app = new Users();
                        app.setId(Integer.parseInt(response.getString("Id")));
                        app.setGulidId(Integer.parseInt(response.getString("GulidId")));
                        app.setGuildName(response.getString("GuildName"));
                        app.setUserTypeId(Integer.parseInt(response.getString("UserTypeId")));
                        app.setNameShop(response.getString("NameShop"));
                        app.setAddress(response.getString("Address"));
                        app.setOperator(response.getString("Operator"));
                        app.setPhone(response.getString("Phone"));
                        app.setPicProfile(response.getString("PicProfile"));
                        app.setCellPhone(response.getString("CellPhone"));
                        app.setEmail(response.getString("Email"));
                        app.setWebSilte(response.getString("WebSilte"));
                        app.setTelegram(response.getString("Telegram"));
                        app.setInstagram(response.getString("Instagram"));
                        app.setCountView(response.getString("CountView"));
                        app.setCountFollowe(response.getString("CountFollowe"));
                        app.setRating(response.getString("Rating"));
                        app.setFollow(response.getString("IsFollow"));
                        app.setGeneral(response.getString("IsGeneral"));
                        app.setCode(response.getString("Code"));
                        app.setConfirm(response.getString("IsConfirm"));
                        app.setRemainingdays(Integer.parseInt(response.getString("Remainingdays")));
                        app.setRowVersion("1");

                        Tab_product.is_confirm = app.isConfirm();

                        // show data
                        // operator
                        if(app.getOperator().toString().equals("") || app.getOperator().toString().equals("null")){
                        }else{
                            Tab_info.name.setText(app.getOperator().toString());
                            Tab_info.et_admin_name.setText(app.getOperator().toString());
                        }
                        // phone
                        if(app.getCellPhone().toString().equals("") || app.getCellPhone().toString().equals("null")){
                        }else{
                            Tab_info.tel.setText(app.getCellPhone().toString());
                        }
                        // day
                        Tab_info.day.setText(app.getRemainingdays()+"");
                        // code
                        Tab_info.code.setText(app.getCode().toString());
                        // image
                        if(app.getPicProfile().equals("") || app.getPicProfile().equals("null")){
                        }else{
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.drawable.place_holder);
                            requestOptions.error(R.drawable.e);
                            String url = url_download + app.getPicProfile();
                            Glide.with(_context)
                                    .setDefaultRequestOptions(requestOptions)
                                    .load(url)
                                    .into(Tab_info.img);
                        }
                        // name
                        if(app.getNameShop().toString().equals("") || app.getNameShop().toString().equals("null")){
                        }else{
                            Tab_info.et_shop_name.setText(app.getNameShop().toString());
                        }
                        // address
                        if(app.getAddress().toString().equals("") || app.getAddress().toString().equals("null")){
                        }else{
                            Tab_info.et_address.setText(app.getAddress().toString());
                        }
                        // phone
                        if(app.getPhone().toString().equals("") || app.getPhone().toString().equals("null")){
                        }else{
                            Tab_info.et_telephone_num.setText(app.getPhone().toString());
                        }
                        // mobile
                        if(app.getCellPhone().toString().equals("") || app.getCellPhone().toString().equals("null")){
                        }else{
                            Tab_info.et_mobile.setText(app.getCellPhone().toString());
                        }
                        // email
                        if(app.getEmail().toString().equals("") || app.getEmail().toString().equals("null")){
                        }else{
                            Tab_info.et_email.setText(app.getEmail().toString());
                        }
                        // web
                        if(app.getWebSilte().toString().equals("") || app.getWebSilte().toString().equals("null")){
                        }else{
                            Tab_info.et_website.setText(app.getWebSilte().toString());
                        }
                        // instagram
                        if(app.getInstagram().toString().equals("") || app.getInstagram().toString().equals("null")){
                        }else{
                            Tab_info.et_instagram.setText(app.getInstagram().toString());
                        }
                        // telegram
                        if(app.getTelegram().toString().equals("") || app.getTelegram().toString().equals("null")){
                        }else{
                            Tab_info.et_telegram.setText(app.getTelegram().toString());
                        }

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError, "get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        } else {}
        return null;
    }
}