package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Main_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.main.Main;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Main_Module.class )
public interface Main_Component {
    Main inject(Main activity);
}