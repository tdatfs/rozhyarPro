package com.example.pourya.sharsna.main.view.profile_login2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Get_info2;
import com.example.pourya.sharsna.main.api.Update_User;
import com.example.pourya.sharsna.main.view.login.Login1;
import com.example.pourya.sharsna.main.view.login.Login2;
import org.json.JSONException;

import de.hdodenhof.circleimageview.CircleImageView;

public class Tab_info extends Fragment {

    public static TextView name, tel, day, code, change;
    public static ImageView img;
    public static EditText et_shop_name, et_address, et_admin_name, et_telephone_num, et_mobile, et_email, et_website, et_instagram, et_telegram;
    Button update;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_info, container, false);

        name = (TextView) view.findViewById(R.id.name) ;
        tel = (TextView) view.findViewById(R.id.tel) ;
        day = (TextView) view.findViewById(R.id.day) ;
        code = (TextView) view.findViewById(R.id.code) ;
        change = (TextView) view.findViewById(R.id.change) ;
        img = (CircleImageView) view.findViewById(R.id.img) ;
        et_shop_name = (EditText) view.findViewById(R.id.et_shop_name) ;
        et_address = (EditText) view.findViewById(R.id.et_address) ;
        et_admin_name = (EditText) view.findViewById(R.id.et_admin_name) ;
        et_telephone_num = (EditText) view.findViewById(R.id.et_telephone_num) ;
        et_mobile = (EditText) view.findViewById(R.id.et_mobile) ;
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_website = (EditText) view.findViewById(R.id.et_website) ;
        et_instagram = (EditText) view.findViewById(R.id.et_instagram) ;
        et_telegram = (EditText) view.findViewById(R.id.et_telegram) ;
        update = (Button) view.findViewById(R.id.save);

        Login1.sp1 = getContext().getSharedPreferences("DB1",0);
        String mid = Login1.sp1.getString("uid1","");
        Login2.sp2 = getContext().getSharedPreferences("DB2",0);
        String uid = Login2.sp2.getString("uid2","");
        new Get_info2(getContext(), getActivity(), Tabs_main2.IdUser, mid, uid);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String a1 = et_shop_name.getText().toString();
                String a2 = et_address.getText().toString();
                String a3 = et_admin_name.getText().toString();
                String a4 = et_telephone_num.getText().toString();
                String a5 = et_mobile.getText().toString();
                String a6 = et_email.getText().toString();
                String a7 = et_website.getText().toString();
                String a8 = et_instagram.getText().toString();
                String a9 = et_telegram.getText().toString();

                try {
                    new Update_User(getContext(), getActivity(), Tabs_main2.IdUser, a1, a2, a3, a4, a5, a6, a7, a8, a9).post();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Change.class));
            }
        });

        return view;
    }
}