package com.example.pourya.sharsna.main.cls;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.pourya.sharsna.main.Interface.MVP_Comment;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.shop.Adapter_Comment;
import com.example.pourya.sharsna.main.viewModel.Scores;
import java.util.List;

public class Set_Comments extends BaseActivity {

    MVP_Comment.ProvidedPresenterOps presenter;
    Context _context;
    Activity mactivity;
    RecyclerView mRecyclerView;
    List<Scores> list_Scores;
    int UID;

    public Set_Comments(MVP_Comment.ProvidedPresenterOps ppo, Context c, Activity a, RecyclerView r, int uid) {
        _context = c;
        mactivity = a;
        presenter = ppo;
        mRecyclerView = r;
        UID = uid;
    }

    public void load() {
        list_Scores = presenter.List_Comments(UID);
        Adapter_Comment adapter = new Adapter_Comment(_context, mactivity, list_Scores);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(_context, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(adapter);
    }
}