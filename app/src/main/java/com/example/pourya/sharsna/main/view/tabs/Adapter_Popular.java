package com.example.pourya.sharsna.main.view.tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.util.List;

public class Adapter_Popular extends RecyclerView.Adapter<Adapter_Popular.ViewHolder> {

    Context context;
    Activity activity;
    List<Products> list;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_Popular(Context c, Activity a, List<Products> l) {
        context = c;
        activity = a;
        list = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView like;
        private ImageView img_like;
        private ImageView image_main;
        private LinearLayout all, one;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            like = (TextView) itemView.findViewById(R.id.like_count) ;
            img_like = (ImageView) itemView.findViewById(R.id.img_like);
            image_main = (ImageView) itemView.findViewById(R.id.img_popular_main);
            all = (LinearLayout) itemView.findViewById(R.id.all);
            one = (LinearLayout) itemView.findViewById(R.id.one);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_popular,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // not show
        if(list.get(position).getNameProduct().equals("")){
            holder.name.setText("");
            holder.like.setText("");
            holder.img_like.setImageResource(R.drawable.non);
            holder.image_main.setImageResource(R.drawable.non);
            holder.all.setBackgroundResource(R.drawable.non);
            holder.one.setBackgroundResource(R.drawable.non);
        }
        // normal
        else {
            holder.all.setBackgroundColor(context.getResources().getColor(R.color.Main_color_bg));
            holder.one.setBackgroundColor(context.getResources().getColor(R.color.white));

            holder.name.setText(list.get(position).getNameProduct());
            if(list.get(position).getCountLike().toString().equals("null") || list.get(position).getCountLike().toString().equals("")){
                holder.like.setText("0");
            }else{
                holder.like.setText(list.get(position).getCountLike());
            }
            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list.get(position).getPicProduct();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(holder.image_main);
            }catch (Exception e){
                new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
            }

            final int id = list.get(position).getId();
            final int uid = list.get(position).getUserId();
            holder.all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product.Pid = id;
                    Product.UID = uid;
                    activity.finish();
                    Product.TabIndex = 1;
                    view.getContext().startActivity(new Intent(view.getContext(), Product.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}