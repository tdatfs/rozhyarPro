package com.example.pourya.sharsna.main.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.view.splash.Splash;

public class Dialog_Retry extends Dialog implements View.OnClickListener {

    Activity c;
    Button btn;

    public Dialog_Retry(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.retry);

        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn:
                dismiss();
                c.startActivity(new Intent(c, Splash.class));
                c.finish();
                break;
        }
    }
}