package com.example.pourya.sharsna.main.view.profile_login2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.Dialog_Delete;
import com.example.pourya.sharsna.main.viewModel.Products;
import java.util.List;

public class Adapter_product extends RecyclerView.Adapter<Adapter_product.ViewHolder> {

    Context context;
    Activity activity;
    List<Products> list;
    String url_download = "http://rozhyar.tdaapp.ir/ImageSave/";

    public Adapter_product(Context c, Activity a, List<Products> l) {
        context = c;
        activity = a;
        list = l;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView like;
        private TextView discount;
        private ImageView img;
        private FloatingActionButton btn_delete;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            like = (TextView) itemView.findViewById(R.id.like_count) ;
            discount = (TextView) itemView.findViewById(R.id.discount_percent) ;
            img = (ImageView) itemView.findViewById(R.id.img_profile_product_main);
            btn_delete = (FloatingActionButton) itemView.findViewById(R.id.btn_delete);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_product2, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getNameProduct());
        if(list.get(position).getCountLike().toString().equals("null") || list.get(position).getCountLike().toString().equals("")){
            holder.like.setText(0+"");
        }else{
            holder.like.setText(Integer.parseInt(list.get(position).getCountLike())+"");
        }
        if(list.get(position).getOffProduct().toString().equals("null") || list.get(position).getOffProduct().toString().equals("")){
            holder.discount.setText(0+"");
        }else{
            holder.discount.setText(Integer.parseInt(list.get(position).getOffProduct())+"");
        }
        if(list.get(position).getPicProduct().toString().equals("null") || list.get(position).getPicProduct().toString().equals("")){
           // nothing
        }else{
            try{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.place_holder);
                requestOptions.error(R.drawable.e);
                String url = url_download + list.get(position).getPicProduct();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(url)
                        .into(holder.img);
            }catch (Exception e){
                new PostError(context, e.getMessage(), Utility.getMethodName()).postError();
            }
        }
        final int id = list.get(position).getId();
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Product2.Pid = id;
                activity.finish();
                view.getContext().startActivity(new Intent(view.getContext(), Product2.class));
            }
        });
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog_Delete cc = new Dialog_Delete(activity, id);
                cc.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}