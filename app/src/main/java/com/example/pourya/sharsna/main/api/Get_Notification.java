package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_Splash;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Retry;
import com.example.pourya.sharsna.main.view.main.Main;
import org.json.JSONException;
import org.json.JSONObject;

public class Get_Notification extends BaseActivity {

    MVP_Splash.ProvidedPresenterOps splash_presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;

    public Get_Notification(boolean net, MVP_Splash.ProvidedPresenterOps ppo, Context context, Activity activity) {
        splash_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        get();
    }

    public Get_Notification get() {

        splash_presenter.Delete_Notification();

        if (mnet) {

            int count = splash_presenter.Count_Notification();

            if(count == 0) {

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                        url + "ApiNotification/Notifications?row=" + splash_presenter.MaxRowV_Notification(), null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String id = response.getString("Id");
                            String text = response.getString("Text");
                            String row = "1";

                            int Id = Integer.parseInt(id);

                            String Q1 = "insert into Notifications (Id,Text,RowVersion) values " +
                                    "('" + Id + "','" + text + "','" + row + "')";
                            splash_presenter.Insert_Notification(Q1);

                            // go main activity
                            Main.city_id = 31;
                            Main.id_selected = 0;
                            Intent intent = new Intent(mactivity, Main.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mactivity.startActivity(intent);
                            mactivity.finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                            Toast.makeText(_context, "خطایی پیش امده است مجددا تلاش کنید.", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        new ErrorVolley(_context).Error(volleyError, "get");
                        if (volleyError.networkResponse == null) {
                            if (volleyError.getClass().equals(TimeoutError.class)) {
                                Dialog_Retry dr = new Dialog_Retry(mactivity);
                                dr.show();
                            }
                        }
                    }
                });

                SampleApp.getInstance().addToRequestQueue(jsonObjReq);
            }

        } else {}

        return null;
    }
}