package com.example.pourya.sharsna.main.view.tabs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Tab_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Tab;
import com.example.pourya.sharsna.main.presenter.Tab_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class Tabs_Main extends BaseActivity implements MVP_Tab.RequiredViewOps,
        View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, TabLayout.OnTabSelectedListener{

    private static final String TAG = Tabs_Main.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), Tabs_Main.class.getName());

    @Inject
    public MVP_Tab.ProvidedPresenterOps mPresenter;

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    MaterialSpinner spinner;
    List<Cities> list_City = new ArrayList<>();
    SpaceNavigationView spaceNavigationView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    LinearLayout navigation_icon;
    public static int city_id=0, id_selected=0;
    public static int TabIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs__main);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(2);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Tabs_Main.this.finish();
                city_id = 0;
                Intent intent = new Intent(Tabs_Main.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 1:
                        Tabs_Main.this.finish();
                        city_id = 0;
                        Intent intenthome = new Intent(Tabs_Main.this, List1.class);
                        startActivity(intenthome);
                        break;

                    case 0:
                        Tabs_Main.this.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent = new Intent(Tabs_Main.this, Main.class);
                        startActivity(intent);
                        break;

                    case 2:
                        // nothing
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();

        after_setup();
    }

    private void setupViews(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.pager);
    }

    private void after_setup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        // first time
        List<String> city_name = new ArrayList<>();
        if(city_id == 0){
            for(int i=0 ; i<list_City.size() ; i++){
                city_name.add(list_City.get(i).getTitle());
                if(list_City.get(i).getSelected().equals("1")){
                    id_selected = i;
                    city_id = list_City.get(i).getId();
                }
            }
        }
        // change city
        else{
            for(int i=0 ; i<list_City.size() ; i++){
                city_name.add(list_City.get(i).getTitle());
                if(list_City.get(i).getId() == city_id){
                    id_selected = i;
                }
            }
        }
        // other setting
        spinner.setItems(city_name);
        spinner.setSelectedIndex(id_selected);
        // set text size
        spinner.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
        spinner.setTextDirection(View.TEXT_DIRECTION_RTL);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Tabs_Main.this.finish();
                city_id = list_City.get(position).getId();
                startActivity(new Intent(Tabs_Main.this, Tabs_Main.class));
            }
        });

        navigation_icon.setOnClickListener(this);

        tabLayout.addTab(tabLayout.newTab().setText("جدیدترین ها"));
        tabLayout.addTab(tabLayout.newTab().setText("محبوبترین ها"));
        tabLayout.addTab(tabLayout.newTab().setText("تخفیفات"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        // BACK TO TAB
        new Handler().postDelayed(
                new Runnable(){
                    @Override
                    public void run() {
                        tabLayout.getTabAt(TabIndex).select();
                    }
                }, 100);

        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        tabLayout.setOnTabSelectedListener((TabLayout.OnTabSelectedListener) Tabs_Main.this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.navigation_icon) {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Tabs_Main.this.finish();
                city_id = 0;
                startActivity(new Intent(Tabs_Main.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Tabs_Main.this.finish();
                startActivity(new Intent( Tabs_Main.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Tab_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Tab_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getTabComponent(new Tab_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //spaceNavigationView.changeCurrentItem(2);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        Tabs_Main.this.finish();
        city_id = 0;
        startActivity(new Intent(this, Main.class));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        viewPager.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}
}