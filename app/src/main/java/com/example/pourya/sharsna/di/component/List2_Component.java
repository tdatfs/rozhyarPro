package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.List2_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.list2.List2;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = List2_Module.class )
public interface List2_Component {
    List2 inject(List2 activity);
}