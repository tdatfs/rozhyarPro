package com.example.pourya.sharsna.main.Interface;

import android.content.Context;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.util.List;

public interface MVP_List3 {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_List3.RequiredViewOps view);

        // get user
        List<Users> List_Users(int id, int cid);
        String MaxRowV_Users();
        void Insert_Users(String Q);
        List<Integer> Id_Users();

        // get city
        List<Cities> getCity();

        String Status_Gulid(int gid);
        void Chanege_Status_Gulid(int gid);
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // get user
        List<Users> List_Users(int id, int cid);
        String MaxRowV_Users();
        void Insert_Users(String Q);
        List<Integer> Id_Users();

        // get city
        List<Cities> getCity();

        String Status_Gulid(int gid);
        void Chanege_Status_Gulid(int gid);
    }
}