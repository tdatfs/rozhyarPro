package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.profile_login2.Tabs_main2;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class Update_Product extends BaseActivity {

    Context _context;
    Activity _activity;
    int Pid, price, off;
    String name, date, des, image;
    ProgressDialog progressDialog;

    public Update_Product(Context context, Activity activity,
                          int id, String na,String da, String off1, String price1, String des1, String img) {

        progressDialog = new ProgressDialog(activity);
        _context = context;
        _activity = activity;
        Pid = id;
        name = na;
        off = Integer.parseInt(off1);
        date = da;
        price = Integer.parseInt(price1);
        des = des1;
        image = img;
    }

    public void post() throws JSONException {
        progressDialog.setMessage("لطفا صبر کنید ...");
        progressDialog.show();

        JSONObject _jsonBody=new JSONObject();
        _jsonBody.put("Id", Pid);
        _jsonBody.put("NameProduct", name);
        _jsonBody.put("OffProduct", off);
        _jsonBody.put("DateExpire", date);
        _jsonBody.put("Price", price);
        _jsonBody.put("Des", des);
        if(image.equals("") || image.equals("null")){
            // image no changed !
        }else{
            // image changed !
            _jsonBody.put("PicProduct", image);
        }

        RequestQueue requestQueue = Volley.newRequestQueue(_context);
        final String requestBody = _jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT,url+"ApiProduct",
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(response.equals("200")){
                        progressDialog.dismiss();
                        Toast.makeText(_context,"اطلاعات با موفقیت ثبت شد",Toast.LENGTH_LONG).show();
                        _activity.finish();
                        Intent intent = new Intent(_activity, Tabs_main2.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        _activity.startActivity(intent);
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(_context,"خطا در ارسال اطلاعات به سرور",Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        new ErrorVolley(_context).Error(error, "post");
                    }
                })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}