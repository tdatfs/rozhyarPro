package com.example.pourya.sharsna.main.view.shop;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Comment_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Comment;
import com.example.pourya.sharsna.main.api.Get_Comments;
import com.example.pourya.sharsna.main.api.Post_Scores;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.presenter.Comment_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.Dialog_Login1;
import com.example.pourya.sharsna.main.viewModel.Scores;
import org.json.JSONException;
import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentPage extends BaseActivity implements MVP_Comment.RequiredViewOps, View.OnClickListener {

    private static final String TAG = CommentPage.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer(getFragmentManager(), CommentPage.class.getName());

    @Inject
    public MVP_Comment.ProvidedPresenterOps mPresenter;

    public static int Uid;
    public static String Mid, SUid;
    public static String _name, _gulid, _img;
    TextView name, gulid;
    ImageView img;
    public static RatingBar rate;
    RecyclerView recyclerView;
    public static EditText et_commetnts;
    Button submit_comments;
    Scores MyComment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment);

        // hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setupViews();
        setupMVP();

        new Get_Comments(haveNetworkConnection(), mPresenter, getApplicationContext(), this, recyclerView, Uid);
        String member_id = get_member_id();
        String user_id = get_user_id();
        if(member_id.equals("") && user_id.equals("")){
            // nothing
        }else{
            if(member_id.equals("")){
                // nothing
            }else{
                MyComment = mPresenter.My_Comment_M(Integer.parseInt(member_id));
            }
            if(user_id.equals("")){
                // nothing
            }else{
                MyComment = mPresenter.My_Comment_U(Integer.parseInt(user_id));
            }
        }

        after_setup();
    }

    private void setupViews() {
        name = (TextView) findViewById(R.id.owner_name);
        gulid = (TextView) findViewById(R.id.categori);
        img = (CircleImageView) findViewById(R.id.rv_img);
        rate = (RatingBar) findViewById(R.id.set_rateing);
        recyclerView = (RecyclerView) findViewById(R.id.rv_shop_comments);
        et_commetnts = (EditText) findViewById(R.id.et_commetnts);
        submit_comments = (Button) findViewById(R.id.submit_comments);
    }

    private void after_setup(){

        name.setText(_name);
        gulid.setText(_gulid);
        try{
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.place_holder);
            requestOptions.error(R.drawable.e);
            String url = url_download + _img ;
            Glide.with(getApplicationContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load(url)
                    .into(img);
        }catch (Exception e){
            new PostError(getApplicationContext(), e.getMessage(), Utility.getMethodName()).postError();
        }
        submit_comments.setOnClickListener(this);

        if(MyComment == null){
            rate.setRating((float) 5);
        }else{
            String my_rate = MyComment.getPoint();
            if(my_rate.equals("") || my_rate.equals("null")){
                rate.setRating((float) 5);
            }else{
                rate.setRating((float) Float.parseFloat(my_rate));
                rate.setIsIndicator(false);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit_comments) {
            String comment = et_commetnts.getText().toString();
            String point = rate.getRating()+"";
            if(comment.equals("") && point.equals("")){
                if(comment.equals("") || comment.equals("null")){
                    Toast.makeText(getActivityContext(), "متن را وارد کنید", Toast.LENGTH_LONG).show();
                }
                if(point.equals("") || point.equals("0.0")){
                    Toast.makeText(getActivityContext(), "ستاره دهید", Toast.LENGTH_LONG).show();
                }
            }else{
                String Mid = get_member_id();
                String SUid = get_user_id();
                if(Mid.equals("") && SUid.equals("")){
                    Dialog_Login1 cdd = new Dialog_Login1(this);
                    cdd.show();
                }else{
                    try {
                        new Post_Scores(haveNetworkConnection(), getApplicationContext(), this, Uid, Mid, SUid, comment, point).post();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Comment_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Comment_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getCommentComponent(new Comment_Module(this))
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        CommentPage.this.finish();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}