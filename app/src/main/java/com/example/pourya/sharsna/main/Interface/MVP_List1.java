package com.example.pourya.sharsna.main.Interface;

import android.content.Context;

import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.util.List;

public interface MVP_List1 {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_List1.RequiredViewOps view);

        // get gulids
        List<Gulids> List_Gulids();
        String MaxRowV_Gulids();
        void Insert_Gulids(String Q);
        List<Integer> Id_Gulids();
        int Count_Gulids();

        // get city
        List<Cities> getCity();
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // get gulids
        List<Gulids> List_Gulids();
        String MaxRowV_Gulids();
        void Insert_Gulids(String Q);
        List<Integer> Id_Gulids();
        int Count_Gulids();

        // get city
        List<Cities> getCity();
    }
}