package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_AddProduct;
import com.example.pourya.sharsna.main.model.AddProduct_Model;
import com.example.pourya.sharsna.main.presenter.AddProduct_Presenter;
import com.example.pourya.sharsna.main.view.profile_login2.AddProduct;
import dagger.Module;
import dagger.Provides;

@Module
public class AddProduct_Module {

    private AddProduct activity;

    public AddProduct_Module(AddProduct  activity) {

        this.activity = activity;
    }

    @Provides
    @ActivityScope
    AddProduct providesAddProductActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_AddProduct.ProvidedPresenterOps providedPresenterOps() {
        AddProduct_Presenter presenter = new AddProduct_Presenter( activity );
        AddProduct_Model model = new AddProduct_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
