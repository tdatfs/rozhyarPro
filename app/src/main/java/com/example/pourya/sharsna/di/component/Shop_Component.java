package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Shop_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.shop.Shop;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Shop_Module.class )
public interface Shop_Component {
    Shop inject(Shop activity);
}