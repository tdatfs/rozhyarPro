package com.example.pourya.sharsna.main.view.profile_login1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Get_Followers;

public class Tab_follower extends Fragment {

    RecyclerView recyclerView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_follower, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_list3);
        new Get_Followers(getContext(), getActivity(), recyclerView, Tabs_main1.IdUser);

        return view;
    }
}