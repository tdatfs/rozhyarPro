package com.example.pourya.sharsna.main.view.profile_login2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Change_Password;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.login.Login2;
import org.json.JSONException;

public class Change extends BaseActivity {

    EditText old, new1, new2;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change);

        Login2.sp2 = getApplicationContext().getSharedPreferences("DB2",0);
        final String pass = Login2.sp2.getString("pass2","");
        final int id = Integer.parseInt(Login2.sp2.getString("uid2",""));

        old = (EditText)findViewById(R.id.old);
        new1 = (EditText)findViewById(R.id.new1);
        new2 = (EditText)findViewById(R.id.new2);
        btn = (Button)findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String o = old.getText().toString();

                // old correct
                if (o.equals(pass)) {
                    // new1 == new2
                    String n1 = new1.getText().toString();
                    String n2 = new2.getText().toString();
                    if(n1.equals(n2)){
                        try {
                            new Change_Password(getApplicationContext(), Change.this, id , o, n1, true).post();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"پسورد های جدید مشابه نیستند",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"پسورد خود را اشتباه وارد کردید",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}