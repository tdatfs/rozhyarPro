package com.example.pourya.sharsna.main.viewModel;

public class SelectedShops {

    private int UserId;
    private String UserNameShop;
    private String UserPicProfile;
    private int CityId;
    private String GetServer;

    public SelectedShops(){}

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUserNameShop() {
        return UserNameShop;
    }

    public void setUserNameShop(String userNameShop) {
        UserNameShop = userNameShop;
    }

    public String getUserPicProfile() {
        return UserPicProfile;
    }

    public void setUserPicProfile(String userPicProfile) {
        UserPicProfile = userPicProfile;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getGetServer() {
        return GetServer;
    }

    public void setGetServer(String getServer) {
        GetServer = getServer;
    }
}
