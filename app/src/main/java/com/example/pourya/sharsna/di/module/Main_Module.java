package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.model.Main_Model;
import com.example.pourya.sharsna.main.presenter.Main_Presenter;
import com.example.pourya.sharsna.main.view.main.Main;
import dagger.Module;
import dagger.Provides;

@Module
public class Main_Module {

    private Main activity;

    public Main_Module(Main activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Main providesMainActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Main.ProvidedPresenterOps providedPresenterOps() {
        Main_Presenter presenter = new Main_Presenter( activity );
        Main_Model model = new Main_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
