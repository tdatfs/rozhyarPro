package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_List3;
import com.example.pourya.sharsna.main.cls.Set_List3;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class Get_List3 extends BaseActivity {

    MVP_List3.ProvidedPresenterOps list3_presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    int id, cid;
    String iemi, mid, uid;

    public Get_List3(boolean net, MVP_List3.ProvidedPresenterOps ppo, Context context, Activity activity, RecyclerView r, int i, String ie, String m, int c, String u) {
        list3_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        progressDialog = new ProgressDialog(mactivity);
        recyclerView = r;
        id = i;
        mid = m;
        iemi = ie;
        cid = c;
        uid = u;
        get();
    }

    public Get_List3 get() {
        if(mnet){
            if (mid.equals("")) {
                mid = "0";
            }
            if (uid.equals("")) {
                uid = "0";
            }
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    url+ "ApiUser/ListForGulid?id="+id+"&iemi="+iemi+"&memberId="+mid+"&CityId="+cid+"&userid="+uid, null, new Response.Listener<JSONArray>() {

                //?row="+list1_presenter.MaxRowV_Gulids()

                @Override
                public void onResponse(JSONArray response) {

                    boolean insert = false;
                    List<Integer> list_users = list3_presenter.Id_Users();

                    try {
                        String Q1 = "insert into Users (Id,GulidId,GuildName,UserTypeId,NameShop,Address,Operator,Phone,PicProfile,CellPhone,Email,WebSilte,Telegram,Instagram,CityId,CountView,CountFollowe,Rating,IsFollow,IsGeneral,RowVersion) values ";
                        for (int i=0; i<response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            String id = jsonObject.getString("Id");
                            String gid = jsonObject.getString("GulidId");
                            String gname = jsonObject.getString("GuildName");
                            String user_type = jsonObject.getString("UserTypeId");
                            String name = jsonObject.getString("NameShop");
                            String address = jsonObject.getString("Address");
                            String operator = jsonObject.getString("Operator");
                            String phone = jsonObject.getString("Phone");
                            String pic = jsonObject.getString("PicProfile");
                            String cell = jsonObject.getString("CellPhone");
                            String mail = jsonObject.getString("Email");
                            String web = jsonObject.getString("WebSilte");
                            String tel = jsonObject.getString("Telegram");
                            String insta = jsonObject.getString("Instagram");
                            String city_id = jsonObject.getString("CityId");
                            String c_view = jsonObject.getString("CountView");
                            String c_follow = jsonObject.getString("CountFollowe");
                            String rate = jsonObject.getString("Rating");
                            String is_follow = jsonObject.getString("IsFollow");
                            String is_general = jsonObject.getString("IsGeneral");
                            String row = "1";

                            int Id = Integer.parseInt(id);
                            int GId = Integer.parseInt(gid);
                            int CId = Integer.parseInt(city_id);
                            int User_Type = Integer.parseInt(user_type);

                            int type = whatdo(list_users, Id);
                            // insert
                            if(type == 1){
                                insert = true;
                                Q1 = Q1.concat("('" + Id + "','" + GId + "','" + gname + "','" + User_Type + "','" + name + "','" + address + "','" + operator + "','" + phone + "','" + pic + "','" + cell + "','" + mail + "','" + web + "','" + tel + "','" + insta + "','" + CId + "','" + c_view + "','" + c_follow + "','" + rate + "','" + is_follow + "','" + is_general + "','" + row + "')," );
                            }
                            // update
                            if(type == 2){
                                String Q2="update Users set GulidId='"+GId+"',GuildName='"+gname+"',UserTypeId='"+User_Type+"',NameShop='"+name+"',Address='"+address+"',Operator='"+operator+"',Phone='"+phone+"',PicProfile='"+pic+"',CellPhone='"+cell+"',Email='"+mail+"',WebSilte='"+web+"',Telegram='"+tel+"',Instagram='"+insta+"',CityId='"+CId+"',CountView='"+c_view+"',CountFollowe='"+c_follow+"',Rating='"+rate+"',IsFollow='"+is_follow+"',IsGeneral='"+is_general+"',RowVersion='"+row+"' where Id="+Id;
                                list3_presenter.Insert_Users(Q2);
                            }
                        }
                        if(insert) {
                            Q1 = Q1.substring(0, Q1.trim().length() - 1).concat(";");
                            list3_presenter.Insert_Users(Q1);
                        }
                        progressDialog.dismiss();
                        list3_presenter.Chanege_Status_Gulid(id);
                        Set_List3 set1 = new Set_List3(list3_presenter,_context,mactivity,recyclerView,id,cid);
                        set1.load();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {
                            Set_List3 set1 = new Set_List3(list3_presenter,_context,mactivity,recyclerView,id,cid);
                            set1.load();
                        }
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        }else{
            Set_List3 set1 = new Set_List3(list3_presenter,_context,mactivity,recyclerView,id,cid);
            set1.load();
        }
        return null;
    }

    public int whatdo(List<Integer> listFunction, int id){
        int result = 1;
        for(int j=0 ; j < listFunction.size() ; j++) {
            if(listFunction.get(j) == id){
                result = 2;
            }
        }
        return result;
    }
}