package com.example.pourya.sharsna.main.cls;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.pourya.sharsna.main.Interface.MVP_List3;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list3.Adapter_List3;
import com.example.pourya.sharsna.main.view.list3.List3;
import com.example.pourya.sharsna.main.viewModel.Users;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Set_List3 extends BaseActivity {

    MVP_List3.ProvidedPresenterOps list3_presenter;
    Context _context;
    Activity mactivity;
    RecyclerView mRecyclerView;
    List<Users> list_User;
    int Gid, Cid;

    public Set_List3(MVP_List3.ProvidedPresenterOps ppo, Context c, Activity a, RecyclerView r, int id, int ci) {
        _context = c;
        mactivity = a;
        list3_presenter = ppo;
        mRecyclerView = r;
        Gid = id;
        Cid = ci;
    }

    public void load() {
        list_User = list3_presenter.List_Users(Gid, Cid);

        if(list_User.size() == 0) {
            List3.no_gulid.setVisibility(View.VISIBLE);
        }else{
            List3.no_gulid.setVisibility(View.GONE);
            // 1 item null
            Users app = new Users();
            app.setNameShop("");
            list_User.add(app);
        }
        Adapter_List3 adapter = new Adapter_List3(_context, mactivity, list_User, false, false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(_context, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(adapter);
    }
}
