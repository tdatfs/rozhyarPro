package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Comment;
import com.example.pourya.sharsna.main.model.Comment_Model;
import com.example.pourya.sharsna.main.presenter.Comment_Presenter;
import com.example.pourya.sharsna.main.view.shop.CommentPage;
import dagger.Module;
import dagger.Provides;

@Module
public class Comment_Module {

    private CommentPage activity;

    public Comment_Module(CommentPage activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    CommentPage providesCommentActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Comment.ProvidedPresenterOps providedPresenterOps() {
        Comment_Presenter presenter = new Comment_Presenter( activity );
        Comment_Model model = new Comment_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
