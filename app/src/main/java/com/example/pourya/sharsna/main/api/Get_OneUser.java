package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.shop.Shop;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class Get_OneUser extends BaseActivity {

    MVP_Main.ProvidedPresenterOps main_presenter;
    Context _context;
    Activity mactivity;
    boolean mnet;
    ProgressDialog progressDialog;
    int id;
    String mid, uid;
    int ID;

    public Get_OneUser(boolean net, MVP_Main.ProvidedPresenterOps ppo, Context context, Activity activity, int i, String m, String u) {
        main_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        progressDialog = new ProgressDialog(mactivity);
        id = i;
        mid = m;
        uid = u;
        get();
    }

    public Get_OneUser get() {
        if (mnet) {
            if (mid.equals("")) {
                mid = "0";
            }
            if (uid.equals("")) {
                uid = "0";
            }
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url + "ApiUser/User?id=" + id + "&row=0x0&memberId="+mid+"&userid="+uid, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    //?row="+list1_presenter.MaxRowV_Gulids()
                    List<Integer> list_users = main_presenter.Id_Users();

                    try {
                        String id = response.getString("Id");
                        String gid = response.getString("GulidId");
                        String gname = response.getString("GuildName");
                        String user_type = response.getString("UserTypeId");
                        String name = response.getString("NameShop");
                        String address = response.getString("Address");
                        String operator = response.getString("Operator");
                        String phone = response.getString("Phone");
                        String pic = response.getString("PicProfile");
                        String cell = response.getString("CellPhone");
                        String mail = response.getString("Email");
                        String web = response.getString("WebSilte");
                        String tel = response.getString("Telegram");
                        String insta = response.getString("Instagram");
                        String c_view = response.getString("CountView");
                        String c_follow = response.getString("CountFollowe");
                        String rate = response.getString("Rating");
                        String is_follow = response.getString("IsFollow");
                        String is_general = response.getString("IsGeneral");
                        String row = "1";

                        ID = Integer.parseInt(id);
                        int GId = Integer.parseInt(gid);
                        int User_Type = Integer.parseInt(user_type);

                        int type = whatdo(list_users, ID);
                        // insert
                        if (type == 1) {
                            String Q1 = "insert into Users (Id,GulidId,GuildName,UserTypeId,NameShop,Address,Operator,Phone,PicProfile,CellPhone,Email,WebSilte,Telegram,Instagram,CountView,CountFollowe,Rating,IsFollow,IsGeneral,RowVersion) values " +
                                    "('" + ID + "','" + GId + "','" + gname + "','" + User_Type + "','" + name + "','" + address + "','" + operator + "','" + phone + "','" + pic + "','" + cell + "','" + mail + "','" + web + "','" + tel + "','" + insta + "','" + c_view + "','" + c_follow + "','" + rate + "','" + is_follow + "','" + is_general + "','" + row + "')";
                            main_presenter.Insert_Users(Q1);
                        }
                        // update
                        if (type == 2) {
                            String Q2 = "update Users set GulidId='" + GId + "',GuildName='" + gname + "',UserTypeId='" + User_Type + "',NameShop='" + name + "',Address='" + address + "',Operator='" + operator + "',Phone='" + phone + "',PicProfile='" + pic + "',CellPhone='" + cell + "',Email='" + mail + "',WebSilte='" + web + "',Telegram='" + tel + "',Instagram='" + insta + "',CountView='" + c_view + "',CountFollowe='" + c_follow + "',Rating='" + rate + "',IsFollow='" + is_follow + "',IsGeneral='" + is_general + "',RowVersion='" + row + "' where Id=" + ID;
                            main_presenter.Insert_Users(Q2);
                        }
                        progressDialog.dismiss();

                        main_presenter.Chanege_Status_Selected(ID);

                        // go shop
                        Shop.Uid = ID;
                        Shop.is_search = false;
                        Shop.selected = true;
                        Shop.bazar = false;
                        Shop.profile = false;
                        Intent intent = new Intent(mactivity, Shop.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mactivity.finish();
                        mactivity.startActivity(intent);


                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError, "get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {
                        }
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        } else {
        }
        return null;
    }

    public int whatdo(List<Integer> listFunction, int id){
        int result = 1;
        for(int j=0 ; j < listFunction.size() ; j++) {
            if(listFunction.get(j) == id){
                result = 2;
            }
        }
        return result;
    }
}