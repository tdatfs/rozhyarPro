package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Product2;
import com.example.pourya.sharsna.main.model.Product2_Model;
import com.example.pourya.sharsna.main.presenter.Product2_Presenter;
import com.example.pourya.sharsna.main.view.profile_login2.Product2;
import dagger.Module;
import dagger.Provides;

@Module
public class Product2_Module {

    private Product2 activity;

    public Product2_Module(Product2 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Product2  providesProduct2Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Product2.ProvidedPresenterOps providedPresenterOps() {
        Product2_Presenter presenter = new Product2_Presenter( activity );
        Product2_Model model = new Product2_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
