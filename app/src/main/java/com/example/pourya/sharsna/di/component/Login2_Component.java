package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Login2_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.login.Login2;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Login2_Module.class )
public interface Login2_Component {
    Login2 inject(Login2 activity);
}