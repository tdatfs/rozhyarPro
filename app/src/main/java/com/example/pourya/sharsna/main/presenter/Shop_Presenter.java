package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Shop;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import com.example.pourya.sharsna.main.viewModel.Products;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.lang.ref.WeakReference;
import java.util.List;

public class Shop_Presenter implements MVP_Shop.ProvidedPresenterOps, MVP_Shop.RequiredPresenterOps{

    private WeakReference<MVP_Shop.RequiredViewOps> mView;
    private MVP_Shop.ProvidedModelOps mModel;

    public Shop_Presenter(MVP_Shop.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Shop.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Shop.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public Users Get_User(int id) {
        return mModel.Get_User(id);
    }

    @Override
    public List<Products> List_Products(int id) {
        return mModel.List_Products(id);
    }

    @Override
    public String MaxRowV_Products() {
        return mModel.MaxRowV_Products();
    }

    @Override
    public void Insert_Products(String Q) {
        mModel.Insert_Products(Q);
    }

    @Override
    public List<Integer> Id_Products() {
        return mModel.Id_Products();
    }

    @Override
    public Gulids Get_Gulid(int id) {
        return mModel.Get_Gulid(id);
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    @Override
    public void update_follow(String follow, int count, int id) {
        mModel.update_follow(follow, count, id);
    }

    public void setModel(MVP_Shop.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}