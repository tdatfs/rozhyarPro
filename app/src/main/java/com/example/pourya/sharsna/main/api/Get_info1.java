package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.profile_login1.Tab_user;
import com.example.pourya.sharsna.main.viewModel.Members;
import org.json.JSONException;
import org.json.JSONObject;

public class Get_info1 extends BaseActivity {

    Context _context;
    Activity mactivity;
    int id;
    ProgressDialog progressDialog;

    public Get_info1(Context context, Activity activity, int i) {
        mactivity = activity;
        _context = context;
        id = i;
        progressDialog = new ProgressDialog(mactivity);
        get();
    }

    public Get_info1 get() {

        if (true) {
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url + "ApiMember/SelectMember?id=" + id, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        Members app = new Members();
                        app.setId(Integer.parseInt(response.getString("Id")));
                        app.setFullName(response.getString("FullName"));
                        app.setPhone(response.getString("Phone"));
                        app.setEmail(response.getString("Email"));
                        app.setIEMI(response.getString("IEMI"));
                        app.setRowVersion("1");

                        // show data
                        // name
                        if(app.getFullName().toString().equals("") || app.getFullName().toString().equals("null")){
                        }else{
                            Tab_user.name.setText(app.getFullName().toString());
                            Tab_user.et_name_family.setText(app.getFullName().toString());
                        }
                        // phone
                        if(app.getPhone().toString().equals("") || app.getPhone().toString().equals("null")){
                        }else{
                            Tab_user.tel.setText(app.getPhone().toString());
                            Tab_user.et_mobile.setText(app.getPhone().toString());
                        }
                        // email
                        if(app.getEmail().toString().equals("") || app.getEmail().toString().equals("null")){
                        }else{
                            Tab_user.mail.setText(app.getEmail().toString());
                            Tab_user.et_email.setText(app.getEmail().toString());
                        }

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError, "get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        } else {}

        return null;
    }
}