package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_List3;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Users;
import java.lang.ref.WeakReference;
import java.util.List;

public class List3_Presenter implements MVP_List3.ProvidedPresenterOps, MVP_List3.RequiredPresenterOps{

    private WeakReference<MVP_List3.RequiredViewOps> mView;
    private MVP_List3.ProvidedModelOps mModel;

    public List3_Presenter(MVP_List3.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_List3.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_List3.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Users> List_Users(int id, int cid) {
        return mModel.List_Users(id, cid);
    }

    @Override
    public String MaxRowV_Users() {
        return mModel.MaxRowV_Users();
    }

    @Override
    public void Insert_Users(String Q) {
        mModel.Insert_Users(Q);
    }

    @Override
    public List<Integer> Id_Users() {
        return mModel.Id_Users();
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    @Override
    public String Status_Gulid(int gid) {
        return mModel.Status_Gulid(gid);
    }

    @Override
    public void Chanege_Status_Gulid(int gid) {
        mModel.Chanege_Status_Gulid(gid);
    }

    public void setModel(MVP_List3.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}