package com.example.pourya.sharsna.main.model;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Login2;
import com.example.pourya.sharsna.main.db.DBAdapter;

public class Login2_Model implements MVP_Login2.ProvidedModelOps {

    private MVP_Login2.RequiredPresenterOps mPresenter;
    DBAdapter dbAdapter;
    Context context;

    public Login2_Model(MVP_Login2.RequiredPresenterOps presenter, Context _conContext) {
        mPresenter = presenter;
        context=_conContext;
        dbAdapter=new DBAdapter(context);
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        if (!isChangingConfiguration) {
            mPresenter = null;
        }
    }
}