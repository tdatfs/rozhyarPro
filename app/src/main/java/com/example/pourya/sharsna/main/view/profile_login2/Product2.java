package com.example.pourya.sharsna.main.view.profile_login2;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Product2_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Product2;
import com.example.pourya.sharsna.main.api.Get_OneProduct;
import com.example.pourya.sharsna.main.api.Update_Product;
import com.example.pourya.sharsna.main.permission.PermissionHandler;
import com.example.pourya.sharsna.main.permission.Permissions;
import com.example.pourya.sharsna.main.presenter.Product2_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.shop.Shop;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.iceteck.silicompressorr.SiliCompressor;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import org.json.JSONException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;

public class Product2 extends BaseActivity implements MVP_Product2.RequiredViewOps, View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Product2.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), Product2.class.getName());

    @Inject
    public MVP_Product2.ProvidedPresenterOps mPresenter;

    public static int Pid;
    Toolbar toolbar;
    LinearLayout navigation_icon;
    DrawerLayout drawerLayout;
    MaterialSpinner spinner;
    List<Cities> list_City = new ArrayList<>();
    SpaceNavigationView spaceNavigationView;
    Button insert;
    public static Button date;
    public static TextView name,off,price,des;
    public static ImageView img_add;
    LinearLayout btn_camera, galery;
    int PICK_IMAGE_REQUEST = 111;
    int CAMERA_REQUEST = 1888;
    public static final int RequestPermissionCode  = 1 ;
    public static String wh = "";
    Bitmap bitmap_camera, bitmap_choose;
    String imageString="";
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product2);

        // hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        wh = "";

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(-1);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Product2.this.finish();
                Intent intent = new Intent(Product2.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        Product2.this.finish();
                        Intent intent1 = new Intent(Product2.this, Main.class);
                        startActivity(intent1);
                        break;

                    case 1:
                        Product2.this.finish();
                        Intent intent2 = new Intent(Product2.this, List1.class);
                        startActivity(intent2);
                        break;

                    case 2:
                        Product2.this.finish();
                        Intent intentshop = new Intent(Product2.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        profile_status();
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();
        setupMVP();

        list_City = mPresenter.getCity();

        String member_id = get_member_id();
        String user_id = get_user_id();
        new Get_OneProduct(getApplicationContext(), this, Pid, member_id, user_id);

        after_setup();
    }

    private void setupViews(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        date = (Button) findViewById(R.id.date);
        insert=(Button)findViewById(R.id.insert);
        name=(TextView)findViewById(R.id.et_product_name);
        price=(TextView)findViewById(R.id.et_price);
        date=(Button)findViewById(R.id.date);
        off=(TextView)findViewById(R.id.et_discount);
        des=(TextView)findViewById(R.id.et_discription);
        img_add=(ImageView)findViewById(R.id.img_add);
        btn_camera = (LinearLayout) findViewById(R.id.btn_camera);
        galery = (LinearLayout) findViewById(R.id.galery);
    }

    private void after_setup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        navigation_icon.setOnClickListener(this);
        date.setOnClickListener(this);
        insert.setOnClickListener(this);
        btn_camera.setOnClickListener(this);
        galery.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.navigation_icon:
                drawerLayout.openDrawer(GravityCompat.END);
                break;

            case R.id.date:

                PersianCalendar P = new PersianCalendar();
                final String now_date = P.getPersianShortDate();

                PersianDatePickerDialog picker = new PersianDatePickerDialog(Product2.this)
                        .setNegativeButton("انصراف")
                        .setPositiveButtonString("تایید")
                        .setActionTextColor(Color.BLACK)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar persianCalendar) {
                                String select_date = persianCalendar.getPersianYear() + "/" + persianCalendar.getPersianMonth() + "/" + persianCalendar.getPersianDay();
                                boolean r = cheak_date(now_date, select_date);
                                if(r){
                                    date.setText(select_date);
                                }else{
                                    date.setText("");
                                    Toast.makeText(getApplicationContext(), "تاریخ معتبر نیست", Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onDisimised() {
                                date.setText("");
                            }
                        });

                picker.show();
                break;

            case R.id.insert:
                //converting image to base64 string
                if(wh.equals("camera")){
                    // compress
                    try {
                        bitmap_camera = SiliCompressor.with(getApplicationContext()).getCompressBitmap(getRealPathFromURI(imageUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap_camera.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    byte[] imageBytes = baos.toByteArray();
                    imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                }
                if(wh.equals("choose")){
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap_choose.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    byte[] imageBytes = baos.toByteArray();
                    imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                }
                String name_product = name.getText().toString();
                String date_expire = date.getText().toString();
                String off_price = faToEn(off.getText().toString());
                String price_product = faToEn(price.getText().toString());
                String des_product = des.getText().toString();
                try {
                    // img
                   // if(imageString.equals("")){
                        //Toast.makeText(getApplicationContext(), "یک عکس انتخاب شود", Toast.LENGTH_LONG).show();
                   // }else{
                        // name
                        if(name_product.equals("")){
                            Toast.makeText(getApplicationContext(), "نام محصول را وارد کنید", Toast.LENGTH_LONG).show();
                        }else{
                            // price
                            if(price_product.equals("")){
                                Toast.makeText(getApplicationContext(), "قیمت را وارد کنید", Toast.LENGTH_LONG).show();
                            }else{
                                // off & date
                                if (off_price.equals("")) {
                                    if(date_expire.equals("")){
                                        // off(0) & date(0)
                                        new Update_Product(getApplicationContext(), this, Pid,
                                                name_product, date_expire, off_price, price_product, des_product, imageString).post();
                                    }else{
                                        // off(0) & date(1)
                                        Toast.makeText(getApplicationContext(), "درصد تخفیف را وارد کنید", Toast.LENGTH_LONG).show();
                                    }
                                }else{
                                    int offp = Integer.parseInt(off_price);
                                    if(offp>0 && offp<100){
                                        // off(1) & date(0)
                                        if(date_expire.equals("")){
                                            Toast.makeText(getApplicationContext(), "تاریخ اعتبار تخفیف را وارد کنید", Toast.LENGTH_LONG).show();
                                        }else{
                                            // off(1) & date(1)
                                            new Update_Product(getApplicationContext(), this, Pid,
                                                    name_product, date_expire, off_price, price_product, des_product, imageString).post();
                                        }
                                    }else{
                                        Toast.makeText(getApplicationContext(), "درصد تخفیف عددی بین 0 تا 100 است", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        }
                    //}

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.btn_camera:
                Permissions.check(getApplicationContext(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        "camera", new Permissions.Options().setRationaleDialogTitle("Info"),

                        new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                wh = "camera";
                                ContentValues values = new ContentValues();
                                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                                imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(intent, CAMERA_REQUEST);
                            }

                            @Override
                            public void onDenied(Context context, ArrayList<String> deniedPermissions) {}

                            @Override
                            public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                                return false;
                            }

                            @Override
                            public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {}
                        });
                break;

            case R.id.galery:
                Permissions.check(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE, null,
                        new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                wh = "choose";
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_PICK);
                                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
                            }
                        });
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                bitmap_choose = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                img_add.setImageBitmap(bitmap_choose);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (requestCode == CAMERA_REQUEST){
            if (resultCode == Activity.RESULT_OK) {
                try {
                    bitmap_camera = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    img_add.setImageBitmap(bitmap_camera);
                    String imageurl = getRealPathFromURI(imageUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    // Requesting runtime permission to access camera.
    public void EnableRuntimePermissionToAccessCamera(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(Product2.this, Manifest.permission.CAMERA)) {
            Toast.makeText(Product2.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(Product2.this,new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Product2.this.finish();
                startActivity(new Intent(Product2.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Product2_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Product2_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getProduct2Component(new Product2_Module(this))
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(-1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        Product2.this.finish();
        startActivity(new Intent(this, Tabs_main2.class));
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}