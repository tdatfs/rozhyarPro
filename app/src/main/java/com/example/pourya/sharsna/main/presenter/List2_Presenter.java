package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_List2;
import com.example.pourya.sharsna.main.viewModel.Cities;
import com.example.pourya.sharsna.main.viewModel.Gulids;
import java.lang.ref.WeakReference;
import java.util.List;

public class List2_Presenter implements MVP_List2.ProvidedPresenterOps, MVP_List2.RequiredPresenterOps{

    private WeakReference<MVP_List2.RequiredViewOps> mView;
    private MVP_List2.ProvidedModelOps mModel;

    public List2_Presenter(MVP_List2.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_List2.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_List2.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Gulids> List_Gulids(int id) {
        return mModel.List_Gulids(id);
    }

    @Override
    public List<Cities> getCity() {
        return mModel.getCity();
    }

    public void setModel(MVP_List2.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}