package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.List3_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.list3.List3;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = List3_Module.class )
public interface List3_Component {
    List3 inject(List3 activity);
}