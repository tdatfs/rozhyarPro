package com.example.pourya.sharsna.main.viewModel;

public class Notifications {

    private int Id;
    private String Text;
    private String RowVersion;

    public Notifications(){}

    public int get_id() {
        return Id;
    }

    public void set_id(int _id) {
        this.Id = _id;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String rowVersion) {
        RowVersion = rowVersion;
    }
}
