package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Comment_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.shop.CommentPage;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Comment_Module.class )
public interface Comment_Component {
    CommentPage inject(CommentPage activity);
}