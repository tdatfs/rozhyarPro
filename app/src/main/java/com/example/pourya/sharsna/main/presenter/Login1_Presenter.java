package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Login1;
import java.lang.ref.WeakReference;

public class Login1_Presenter implements MVP_Login1.ProvidedPresenterOps, MVP_Login1.RequiredPresenterOps {

    private WeakReference<MVP_Login1.RequiredViewOps> mView;
    private MVP_Login1.ProvidedModelOps mModel;

    public Login1_Presenter(MVP_Login1.RequiredViewOps view)
    {
        mView = new WeakReference<>(view);
    }

    private MVP_Login1.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Login1.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(MVP_Login1.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}