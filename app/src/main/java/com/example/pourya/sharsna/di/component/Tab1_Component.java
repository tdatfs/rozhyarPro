package com.example.pourya.sharsna.di.component;

import com.example.pourya.sharsna.di.module.Tab1_Module;
import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.view.profile_login1.Tabs_main1;
import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = Tab1_Module.class )
public interface Tab1_Component {
    Tabs_main1 inject(Tabs_main1 activity);
}