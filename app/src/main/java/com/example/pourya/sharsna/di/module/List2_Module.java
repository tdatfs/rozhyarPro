package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_List2;
import com.example.pourya.sharsna.main.model.List2_Model;
import com.example.pourya.sharsna.main.presenter.List2_Presenter;
import com.example.pourya.sharsna.main.view.list2.List2;
import dagger.Module;
import dagger.Provides;

@Module
public class List2_Module {

    private List2 activity;

    public List2_Module(List2 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    List2 providesList2Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_List2.ProvidedPresenterOps providedPresenterOps() {
        List2_Presenter presenter = new List2_Presenter( activity );
        List2_Model model = new List2_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
