package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Comment;
import com.example.pourya.sharsna.main.viewModel.Scores;
import java.lang.ref.WeakReference;
import java.util.List;

public class Comment_Presenter implements MVP_Comment.ProvidedPresenterOps, MVP_Comment.RequiredPresenterOps{

    private WeakReference<MVP_Comment.RequiredViewOps> mView;
    private MVP_Comment.ProvidedModelOps mModel;

    public Comment_Presenter(MVP_Comment.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Comment.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Comment.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public List<Integer> Id_Comments() {
        return mModel.Id_Comments();
    }

    @Override
    public void Insert_Comments(String Q) {
        mModel.Insert_Comments(Q);
    }

    @Override
    public String MaxRowV_Comments() {
        return mModel.MaxRowV_Comments();
    }

    @Override
    public List<Scores> List_Comments(int uid) {
        return mModel.List_Comments(uid);
    }

    @Override
    public Scores My_Comment_M(int mid) {
        return mModel.My_Comment_M(mid);
    }

    @Override
    public Scores My_Comment_U(int uid) {
        return mModel.My_Comment_U(uid);
    }

    public void setModel(MVP_Comment.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}