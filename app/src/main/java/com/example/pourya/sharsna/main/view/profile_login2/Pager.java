package com.example.pourya.sharsna.main.view.profile_login2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class Pager extends FragmentStatePagerAdapter {

    int tabCount;

    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Tab_product tb1 = new Tab_product();
                return tb1;
            case 1:
                Tab_info tb2 = new Tab_info();
                return tb2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}