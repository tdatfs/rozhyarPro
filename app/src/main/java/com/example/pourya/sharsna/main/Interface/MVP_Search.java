package com.example.pourya.sharsna.main.Interface;

import android.content.Context;
import com.example.pourya.sharsna.main.viewModel.Cities;
import java.util.List;

public interface MVP_Search {

    interface RequiredViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedPresenterOps {
        void onDestroy(boolean isChangingConfiguration);
        void setView(MVP_Search.RequiredViewOps view);

        // get city
        List<Cities> getCity();
    }

    interface RequiredPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    interface ProvidedModelOps {
        void onDestroy(boolean isChangingConfiguration);

        // get city
        List<Cities> getCity();
    }
}