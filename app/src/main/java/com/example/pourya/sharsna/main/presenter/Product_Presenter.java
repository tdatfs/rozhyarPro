package com.example.pourya.sharsna.main.presenter;

import android.content.Context;
import com.example.pourya.sharsna.main.Interface.MVP_Product;
import java.lang.ref.WeakReference;
import java.util.List;

public class Product_Presenter implements MVP_Product.ProvidedPresenterOps, MVP_Product.RequiredPresenterOps{

    private WeakReference<MVP_Product.RequiredViewOps> mView;
    private MVP_Product.ProvidedModelOps mModel;

    public Product_Presenter(MVP_Product.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    private MVP_Product.RequiredViewOps getView() throws NullPointerException {
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void onDestroy(boolean isChangingConfiguration) {
        mView = null;
        mModel.onDestroy(isChangingConfiguration);
        if ( !isChangingConfiguration ) {
            mModel = null;
        }
    }

    @Override
    public void setView(MVP_Product.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public String MaxRowV_Users() {
        return mModel.MaxRowV_Users();
    }

    @Override
    public void Insert_Users(String Q) {
        mModel.Insert_Users(Q);
    }

    @Override
    public List<Integer> Id_Users() {
        return mModel.Id_Users();
    }

    @Override
    public void update_like(String like, int count, int id) {
        mModel.update_like(like,count,id);
    }

    public void setModel(MVP_Product.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }
}