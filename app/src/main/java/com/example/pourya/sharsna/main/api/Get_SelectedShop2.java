package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.Interface.MVP_Main;
import com.example.pourya.sharsna.main.cls.Set_SelectedShop;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.main.Main;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Get_SelectedShop2 extends BaseActivity {

    MVP_Main.ProvidedPresenterOps main_presenter;
    Context _context;
    Activity mactivity;
    RecyclerView recyclerView;
    boolean mnet;
    boolean insert;
    int cid;

    public Get_SelectedShop2(boolean net, MVP_Main.ProvidedPresenterOps ppo, Context context, Activity activity, RecyclerView r, int c) {
        main_presenter = ppo;
        mactivity = activity;
        _context = context;
        mnet = net;
        recyclerView = r;
        cid = c;
        get();
    }

    public Get_SelectedShop2 get() {
        if(Main.select == 0){
            main_presenter.Delete_SelectedShop();
            insert = false;

            if(mnet){
                int count = main_presenter.Count_SelectedShop();

                if(count == 0){

                    JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                            url+ "ApiShop/Shop?cityid="+cid, null, new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                String Q1 = "insert into SelectedShops (UserId,UserNameShop,UserPicProfile,CityId,GetServer) values ";
                                for (int i=0; i<response.length(); i++) {
                                    insert = true;
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    String id = jsonObject.getString("UserId");
                                    String name = jsonObject.getString("UserNameShop");
                                    String pic = jsonObject.getString("UserPicProfile");
                                    String status = "false";

                                    int Id = Integer.parseInt(id);

                                    Q1 = Q1.concat("('" + Id + "','" + name + "','" + pic + "','" + cid + "','" + status + "')," );
                                }
                                if(insert) {
                                    Q1 = Q1.substring(0, Q1.trim().length() - 1).concat(";");
                                    main_presenter.Insert_SelectedShop(Q1);
                                }

                                // set selected shop
                                Main.select = 1;
                                Set_SelectedShop set = new Set_SelectedShop(mnet, main_presenter,_context,mactivity,recyclerView,cid);
                                set.load();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                                Toast.makeText(_context, "خطایی پیش امده است مجددا تلاش کنید.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            new ErrorVolley(_context).Error(volleyError,"get");
                            if (volleyError.networkResponse == null) {
                                if (volleyError.getClass().equals(TimeoutError.class)) {
                                    Toast.makeText(_context, "کیفیت اینترنت شما مناسب نیست.", Toast.LENGTH_LONG).show();
                                    Set_SelectedShop set = new Set_SelectedShop(mnet, main_presenter,_context,mactivity,recyclerView,cid);
                                    set.load();
                                }
                            }
                        }
                    });
                    SampleApp.getInstance().addToRequestQueue(jsonObjReq);
                }

            }else{
                Set_SelectedShop set = new Set_SelectedShop(mnet, main_presenter,_context,mactivity,recyclerView,cid);
                set.load();
            }
        }
        // not repeat again
        else{
            Set_SelectedShop set = new Set_SelectedShop(mnet, main_presenter,_context,mactivity,recyclerView,cid);
            set.load();
        }
        return null;
    }
}