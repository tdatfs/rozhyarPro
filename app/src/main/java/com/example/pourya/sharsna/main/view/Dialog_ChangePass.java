package com.example.pourya.sharsna.main.view;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.main.api.Change_Password;
import com.example.pourya.sharsna.main.view.login.Login2;
import org.json.JSONException;

public class Dialog_ChangePass extends Dialog implements View.OnClickListener {

    Activity c;
    Dialog d;
    EditText old, new1, new2;
    Button btn;
    String pass;
    int id;

    public Dialog_ChangePass(Activity a) {
        super(a);
        this.c = a;
        this.setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.change);

        Login2.sp2 = c.getSharedPreferences("DB2",0);
        pass = Login2.sp2.getString("pass2","");
        id = Integer.parseInt(Login2.sp2.getString("uid2",""));

        btn = (Button) findViewById(R.id.btn);
        old = (EditText) findViewById(R.id.old);
        new1 = (EditText) findViewById(R.id.new1);
        new2 = (EditText) findViewById(R.id.new2);

        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn:
                String old_p = faToEn(old.getText().toString());

                // old correct
                if (old_p.equals(pass)) {

                    // new1 == new2
                    String new1_p = faToEn(new1.getText().toString());
                    String new2_p = faToEn(new2.getText().toString());

                    if(new1_p.equals(new2_p)){
                        try {
                            new Change_Password(c, c, id , old_p, new1_p,false).post();
                            dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(c,"پسورد های جدید مشابه نیستند",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(c,"پسورد خود را اشتباه وارد کردید",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public String faToEn(String num) {
        return num
                .replace("۰", "0")
                .replace("۱", "1")
                .replace("۲", "2")
                .replace("۳", "3")
                .replace("۴", "4")
                .replace("۵", "5")
                .replace("۶", "6")
                .replace("۷", "7")
                .replace("۸", "8")
                .replace("۹", "9");
    }
}