package com.example.pourya.sharsna.main.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.main.db.ErrorVolley;
import com.example.pourya.sharsna.main.db.PostError;
import com.example.pourya.sharsna.main.db.Utility;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.profile_login2.Adapter_product;
import com.example.pourya.sharsna.main.view.profile_login2.Tab_product;
import com.example.pourya.sharsna.main.viewModel.Products;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class Get_Product2 extends BaseActivity {

    Context _context;
    Activity mactivity;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    int id;
    String mid, uid;

    public Get_Product2(Context context, Activity activity, RecyclerView r, int i, String m, String u) {
        mactivity = activity;
        _context = context;
        progressDialog = new ProgressDialog(mactivity);
        recyclerView = r;
        id = i;
        mid = m;
        uid = u;
        get();
    }

    public Get_Product2 get() {

        final List<Products> PList = new ArrayList<>();

        if(true){
            if (mid.equals("")) {
                mid = "0";
            }
            if (uid.equals("")) {
                uid = "0";
            }
            progressDialog.setMessage("در حال دریافت اطلاعات از سرور ...");
            progressDialog.show();
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    url+ "ApiProduct/ListForUser?id="+id+"&memberId="+mid+"&userid="+uid, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    try {
                        for (int i=0; i<response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);

                            Products app = new Products();
                            app.setId(Integer.parseInt(jsonObject.getString("Id")));
                            app.setUserId(Integer.parseInt(jsonObject.getString("UserId")));
                            app.setNameProduct(jsonObject.getString("NameProduct"));
                            app.setPicProduct(jsonObject.getString("PicProduct"));
                            app.setPrice(jsonObject.getString("Price"));
                            app.setInsertDate(jsonObject.getString("InsertDate"));
                            app.setDes(jsonObject.getString("Des"));
                            app.setOffProduct(jsonObject.getString("OffProduct"));
                            app.setDateExpire(jsonObject.getString("DateExpire"));
                            app.setDateExpireshamsi(jsonObject.getString("DateExpireshamsi"));
                            app.setIsActive(jsonObject.getString("IsActive"));
                            app.setCountView(jsonObject.getString("CountView"));
                            app.setCountLike(jsonObject.getString("CountLike"));
                            app.setIsConfirm(jsonObject.getString("IsConfirm"));
                            app.setIsLike(jsonObject.getString("IsLike"));
                            app.setRowVersion("1");

                            PList.add(app);
                        }

                        // set data to count
                        Tab_product.pnumber.setText(PList.size()+"");

                        // set data to recycler view
                        Adapter_product adapter = new Adapter_product(_context, mactivity, PList);
                        recyclerView.setLayoutManager(new GridLayoutManager(_context,2));
                        recyclerView.setAdapter(adapter);

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        new PostError(_context, e.getMessage(), Utility.getMethodName()).postError();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    new ErrorVolley(_context).Error(volleyError,"get");
                    if (volleyError.networkResponse == null) {
                        if (volleyError.getClass().equals(TimeoutError.class)) {}
                    }
                }
            });

            SampleApp.getInstance().addToRequestQueue(jsonObjReq);

        }else{}

        return null;
    }
}