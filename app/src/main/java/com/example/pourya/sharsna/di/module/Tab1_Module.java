package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_Tab1;
import com.example.pourya.sharsna.main.model.Tab1_Model;
import com.example.pourya.sharsna.main.presenter.Tab1_Presenter;
import com.example.pourya.sharsna.main.view.profile_login1.Tabs_main1;
import dagger.Module;
import dagger.Provides;

@Module
public class Tab1_Module {

    private Tabs_main1 activity;

    public Tab1_Module(Tabs_main1 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Tabs_main1 providesTab1Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_Tab1.ProvidedPresenterOps providedPresenterOps() {
        Tab1_Presenter presenter = new Tab1_Presenter( activity );
        Tab1_Model model = new Tab1_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}