package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_List3;
import com.example.pourya.sharsna.main.model.List3_Model;
import com.example.pourya.sharsna.main.presenter.List3_Presenter;
import com.example.pourya.sharsna.main.view.list3.List3;
import dagger.Module;
import dagger.Provides;

@Module
public class List3_Module {

    private List3 activity;

    public List3_Module(List3 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    List3 providesList3Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_List3.ProvidedPresenterOps providedPresenterOps() {
        List3_Presenter presenter = new List3_Presenter( activity );
        List3_Model model = new List3_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
