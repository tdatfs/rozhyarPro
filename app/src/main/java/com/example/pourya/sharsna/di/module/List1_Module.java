package com.example.pourya.sharsna.di.module;

import com.example.pourya.sharsna.di.scope.ActivityScope;
import com.example.pourya.sharsna.main.Interface.MVP_List1;
import com.example.pourya.sharsna.main.model.List1_Model;
import com.example.pourya.sharsna.main.presenter.List1_Presenter;
import com.example.pourya.sharsna.main.view.list1.List1;
import dagger.Module;
import dagger.Provides;

@Module
public class List1_Module {

    private List1 activity;

    public List1_Module(List1 activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    List1 providesList1Activity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MVP_List1.ProvidedPresenterOps providedPresenterOps() {
        List1_Presenter presenter = new List1_Presenter( activity );
        List1_Model model = new List1_Model( presenter , activity );
        presenter.setModel( model );
        return presenter;
    }
}
