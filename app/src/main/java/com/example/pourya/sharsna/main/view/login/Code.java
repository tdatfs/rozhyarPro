package com.example.pourya.sharsna.main.view.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pourya.sharsna.R;
import com.example.pourya.sharsna.common.SampleApp;
import com.example.pourya.sharsna.common.StateMaintainer;
import com.example.pourya.sharsna.di.module.Code_Module;
import com.example.pourya.sharsna.main.Interface.MVP_Login1;
import com.example.pourya.sharsna.main.api.Get_StepTwo;
import com.example.pourya.sharsna.main.presenter.Login1_Presenter;
import com.example.pourya.sharsna.main.view.BaseActivity;
import com.example.pourya.sharsna.main.view.list1.List1;
import com.example.pourya.sharsna.main.view.main.Main;
import com.example.pourya.sharsna.main.view.profile_login1.Tabs_main1;
import com.example.pourya.sharsna.main.view.search.Search;
import com.example.pourya.sharsna.main.view.tabs.Tabs_Main;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

public class Code extends BaseActivity implements MVP_Login1.RequiredViewOps, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = Code.class.getSimpleName();
    private final StateMaintainer mStateMaintainer = new StateMaintainer( getFragmentManager(), Code.class.getName());

    @Inject
    public MVP_Login1.ProvidedPresenterOps mPresenter;

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    LinearLayout navigation_icon;
    SpaceNavigationView spaceNavigationView;
    MaterialSpinner spinner;
    EditText code;
    LinearLayout btn_login;
    public static SharedPreferences sp1;
    String username_shp, uid_shp;
    public static String enter_mobile;
    TextView txt_t;
    int wait_time = 80;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.code);

        setNavigationViewListener();

        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_list_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_shopping_cart_24px));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_person_24px));
        spaceNavigationView.changeCurrentItem(3);
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                Code.this.finish();
                Intent intent = new Intent(Code.this, Search.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        Code.this.finish();
                        Main.city_id = 31;
                        Main.id_selected = 0;
                        Intent intent = new Intent(Code.this, Main.class);
                        startActivity(intent);
                        break;

                    case 1:
                        Code.this.finish();
                        Intent intentmenu = new Intent(Code.this, List1.class);
                        startActivity(intentmenu);
                        break;

                    case 2:
                        Code.this.finish();
                        Intent intentshop = new Intent(Code.this, Tabs_Main.class);
                        startActivity(intentshop);
                        break;

                    case 3:
                        // nothing
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {}
        });

        setupViews();

        sp1 = getApplicationContext().getSharedPreferences("DB1",0);
        username_shp = sp1.getString("user1","");
        uid_shp = sp1.getString("uid1","");

        // no login
        if(uid_shp.equals("")){
            if(username_shp.equals("")){
                // nothing
            }else{
                // show it and login
                String _code = faToEn(code.getText().toString());
                if(_code.equals("")){
                    Toast.makeText(getApplicationContext(), "کد فعالسازی را وارد کنید", Toast.LENGTH_LONG).show();
                }else{
                    new Get_StepTwo(haveNetworkConnection(),getApplicationContext(),this,enter_mobile,_code,false);
                }
            }
        }
        // is login
        else{
            Tabs_main1.IdUser = Integer.parseInt(uid_shp);
            this.finish();
            startActivity(new Intent(this, Tabs_main1.class));
        }

        setupMVP();
        afterSetup();
    }

    public void setupViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        navigation_icon = (LinearLayout) findViewById(R.id.navigation_icon);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        code =(EditText) findViewById(R.id.code);
        btn_login =(LinearLayout) findViewById(R.id.btn_login);
        txt_t =(TextView) findViewById(R.id.txt_t);
    }

    private void afterSetup(){

        setSupportActionBar(toolbar);
        toolbar.setTitleMarginStart(60);
        toolbar.setTitleTextColor(Color.WHITE);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        spinner.setVisibility(View.INVISIBLE);

        navigation_icon.setOnClickListener(this);
        btn_login.setOnClickListener(this);

        final Timer T = new Timer();
        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(wait_time>=0) {
                            txt_t.setText(wait_time + " ثانیه");
                            wait_time = wait_time - 1;
                        }else{
                            T.cancel();
                        }
                    }
                });
            }
        }, 1000, 1000);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.navigation_icon:
                drawerLayout.openDrawer(GravityCompat.END);
                break;

            case R.id.btn_login:
                //AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
                //v.startAnimation(buttonClick);
                String _code = code.getText().toString();
                if(_code.equals("")){
                    Toast.makeText(getApplicationContext(), "شماره موبایل خود را وارد کنید", Toast.LENGTH_LONG).show();
                }else{
                    new Get_StepTwo(haveNetworkConnection(),getApplicationContext(),this,enter_mobile,_code,false);
                }
                break;
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem1:
                drawerLayout.closeDrawer(GravityCompat.END);
                Code.this.finish();
                startActivity(new Intent(Code.this, Main.class));
                break;

            case R.id.menuitem2:
                clear_login1();
                clear_login2();
                drawerLayout.closeDrawer(GravityCompat.END);
                Code.this.finish();
                startActivity(new Intent( Code.this, Main.class));
                break;
        }
        return true;
    }

    private void setupMVP(){
        if ( mStateMaintainer.firstTimeIn() ) {
            initialize();
        } else {
            reinitialize();
        }
    }

    private void initialize(){
        Log.d(TAG, "initialize");
        setupComponent();
        mStateMaintainer.put(Login1_Presenter.class.getSimpleName(), mPresenter);
    }

    private void reinitialize() {
        Log.d(TAG, "reinitialize");
        mPresenter = mStateMaintainer.get(Login1_Presenter.class.getSimpleName());
        mPresenter.setView(this);
        if ( mPresenter == null )
            setupComponent();
    }

    private void setupComponent(){
        Log.d(TAG, "setupComponent");
        SampleApp.get(this)
                .getAppComponent()
                .getCodeComponent(new Code_Module(this))
                .inject(this);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceNavigationView.changeCurrentItem(3);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    public void back(){
        Code.this.finish();
        startActivity(new Intent(this, Main.class));
    }
}